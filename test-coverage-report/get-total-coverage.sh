#!/usr/bin/env bash

set -ue

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

DELIM=','
INFILE="$DIR/target/site/jacoco-aggregate/jacoco.csv"

declare -A totals=()

i=0
for COL_NAME in $(head -n1 "$INFILE" | tr "$DELIM" '\n')
do
    i=$((i+1))
    if [[ "$COL_NAME" == 'LINE_MISSED' ]] || [[ "$COL_NAME" == 'LINE_COVERED' ]]
    then
        totals[$COL_NAME]=$(sed 1,1d "$INFILE" | awk -F "\"*,\"*" "{sum += \$$i} END {print sum}")
    fi
done

awk "BEGIN{ printf \"%0.2f\n\", (${totals['LINE_COVERED']}*100/(${totals['LINE_COVERED']}+${totals['LINE_MISSED']})) }"
