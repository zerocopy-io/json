package io.zerocopy.json.core.format;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RFC3339Converter extends AbstractDateTimeConverter {

    public enum Format {
        DATE("([0-9]+)-([0-9]{2})-([0-9]{2})"),
        TIME("([0-9]{2}):([0-9]{2})(?::([0-9]{2})(?:.([0-9]{1,9}))?)?(?:[Zz]|([+-])([0-9]{2}):([0-9]{2}))"),
        DATE_TIME(DATE.expr + "[Tt]" + TIME.expr);

        public final String expr;
        public final Pattern pattern;

        Format(String expr) {
            this.expr = expr;
            this.pattern = Pattern.compile("^" + expr + "$");
        }
    }

    protected final Format format;

    public RFC3339Converter(Format format) {
        this.format = format;
    }

    public Format getFormat() {
        return format;
    }

    @Override
    public Date parse(String source) {
        return parseRFC3339Date(source, format);
    }

    @Override
    public void format(Appendable out, Date date, int offset) throws IOException {
        printRFC3339Date(out, date, format, offset);
    }

    public static Date parseRFC3339Date(String source, Format format) {
        if (format == null)
            throw new NullPointerException();

        Matcher matcher = format.pattern.matcher(source);
        if (!matcher.matches())
            throw new IllegalArgumentException("not a " + format.name() + " string");

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setLenient(false);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        if (format == Format.DATE || format == Format.DATE_TIME) {
            calendar.set(Calendar.YEAR, intGroup(matcher, 1));
            calendar.set(Calendar.MONTH, intGroup(matcher, 2) - 1);
            calendar.set(Calendar.DAY_OF_MONTH, intGroup(matcher, 3));
        } else {
            //leave the current date
        }

        if (format == Format.DATE_TIME) {
            calendar.set(Calendar.HOUR_OF_DAY, intGroup(matcher, 4));
            calendar.set(Calendar.MINUTE, intGroup(matcher, 5));
            calendar.set(Calendar.SECOND, intGroup(matcher, 6));
            calendar.set(Calendar.MILLISECOND, fractGroup(matcher, 7, 3));

            int zoneOffset = 0;
            String offsetSign = matcher.group(8);
            if (offsetSign != null) {
                zoneOffset += 3600000L * intGroup(matcher, 9); //offset minutes
                zoneOffset += 60000L * intGroup(matcher, 10); //offset seconds

                if (offsetSign.equals("-"))
                    zoneOffset = -zoneOffset;
            }

            calendar.set(Calendar.ZONE_OFFSET, zoneOffset);
        }
        if (format == Format.TIME) {
            calendar.set(Calendar.HOUR_OF_DAY, intGroup(matcher, 1));
            calendar.set(Calendar.MINUTE, intGroup(matcher, 2));
            calendar.set(Calendar.SECOND, intGroup(matcher, 3));
            calendar.set(Calendar.MILLISECOND, fractGroup(matcher, 4, 3));

            int zoneOffset = 0;
            String offsetSign = matcher.group(5);
            if (offsetSign != null) {
                zoneOffset += 3600000L * intGroup(matcher, 6); //offset minutes
                zoneOffset += 60000L * intGroup(matcher, 7); //offset seconds

                if (offsetSign.equals("-"))
                    zoneOffset = -zoneOffset;
            }

            calendar.set(Calendar.ZONE_OFFSET, zoneOffset);
        }
        return calendar.getTime();
    }

    /**
     * @param out    print destination
     * @param date   the date to print
     * @param format the date/time format
     * @param offset the timezone offset [ms]
     * @throws IOException when a print operation fails
     */
    public static void printRFC3339Date(Appendable out, Date date, Format format, int offset) throws IOException {
        if (offset % 60000 != 0)
            throw new IllegalArgumentException("zoneOffset must be a multiple of 60000 ms (minutes)");

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(new Date(date.getTime() + offset));
        calendar.set(Calendar.ZONE_OFFSET, offset);

        if (format == Format.DATE || format == Format.DATE_TIME) {
            padInt(out, calendar.get(Calendar.YEAR), 4);
            out.append('-');
            padInt(out, calendar.get(Calendar.MONTH) + 1, 2);
            out.append('-');
            padInt(out, calendar.get(Calendar.DAY_OF_MONTH), 2);
        }

        if (format == Format.DATE_TIME)
            out.append('T');

        if (format == Format.TIME || format == Format.DATE_TIME) {
            padInt(out, calendar.get(Calendar.HOUR_OF_DAY), 2);
            out.append(':');
            padInt(out, calendar.get(Calendar.MINUTE), 2);
            out.append(':');
            padInt(out, calendar.get(Calendar.SECOND), 2);
            out.append('.');
            padInt(out, calendar.get(Calendar.MILLISECOND), 3);

            if (offset != 0) {
                out.append(offset >= 0 ? '+' : '-');
                padInt(out, offset / 3600000, 2);
                out.append(':');
                padInt(out, (offset % 3600000) / 60000, 2);
            } else
                out.append('Z');
        }
    }

    private static int intGroup(Matcher matcher, int group) {
        String groupValue = matcher.group(group);
        if (groupValue != null)
            return Integer.parseInt(groupValue, 10);
        else
            return 0;
    }

    private static int fractGroup(Matcher matcher, int group, int negPow10) {
        String groupValue = matcher.group(group);
        if (groupValue != null) {
            if (groupValue.length() > negPow10)
                groupValue = groupValue.substring(0, negPow10);
            return Integer.parseInt(groupValue, 10);
        } else
            return 0;
    }

    private static void padInt(Appendable out, int value, int places) throws IOException {
        String str = Integer.toString(value);
        while (places-- > str.length())
            out.append('0');
        out.append(str);
    }
}
