package io.zerocopy.json.core;

public class ObjectUtils {
    //TODO replace with Objects.hashCode() when Java >= 7
    public static int hashCode(Object... a) {
        if (a == null) {
            return 0;
        } else {
            int result = 1;
            Object[] var2 = a;
            int var3 = a.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                Object element = var2[var4];
                result = 31 * result + (element == null ? 0 : element.hashCode());
            }

            return result;
        }
    }
}
