package io.zerocopy.json.core.format;

import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;

public abstract class AbstractDateTimeConverter implements DateTimeConverter {

    @Override
    public String format(Date date) {
        return format(date, 0);
    }

    @Override
    public String format(Date date, TimeZone timeZone) {
        return format(date, timeZone.getRawOffset());
    }

    @Override
    public String format(Date date, int offset) {
        StringBuilder sb = new StringBuilder();
        try {
            format(sb, date, offset);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    @Override
    public void format(Appendable out, Date date) throws IOException {
        format(out, date, 0);
    }

    @Override
    public void format(Appendable out, Date date, TimeZone timeZone) throws IOException {
        format(out, date, timeZone.getRawOffset());
    }
}
