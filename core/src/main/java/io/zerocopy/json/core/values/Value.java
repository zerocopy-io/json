package io.zerocopy.json.core.values;

import java.math.BigDecimal;
import java.util.Arrays;

public class Value {
    public static boolean equals(Object a, Object b) {
        return a == b || a != null && a.equals(b);
    }

    public static int hash(Object... values) {
        return Arrays.hashCode(values);
    }

    public static BigDecimal scalarToBigInteger(Object value) {
        if (value == null)
            return null;
        if (value instanceof BigDecimal)
            return (BigDecimal) value;
        else if (value instanceof Number)
            return new BigDecimal(value.toString());
        else
            return new BigDecimal(value.toString());
    }

    public static BigDecimal max(BigDecimal a, BigDecimal b) {
        if (b != null)
            if (a != null)
                a = a.max(b);
            else
                a = b;
        return a;
    }

    public static BigDecimal min(BigDecimal a, BigDecimal b) {
        if (b != null)
            if (a != null)
                a = a.min(b);
            else
                a = b;
        return a;
    }

    public static BigDecimal gcd(BigDecimal a, BigDecimal b) {
        while (b.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal temp = b;
            b = a.divideAndRemainder(b)[1];
            a = temp;
        }
        return a;
    }

    public static BigDecimal lcm(BigDecimal a, BigDecimal b) {
        if (b != null)
            if (a != null)
                return a.multiply(b.divide(gcd(a, b), BigDecimal.ROUND_UNNECESSARY));
            else
                a = b;
        return a;
    }

    public static Integer max(Integer a, Integer b) {
        if (b != null)
            if (a != null)
                a = Math.max(a, b);
            else
                a = b;
        return a;
    }

    public static Integer min(Integer a, Integer b) {
        if (b != null)
            if (a != null)
                a = Math.min(a, b);
            else
                a = b;
        return a;
    }
}
