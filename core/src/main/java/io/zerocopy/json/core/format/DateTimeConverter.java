package io.zerocopy.json.core.format;

import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;

public interface DateTimeConverter {
    Date parse(String source);

    /**
     * @param date the date to print
     * @return string representation of the date in UTC
     */
    String format(Date date);

    /**
     * @param date     the date to print
     * @param timeZone the timezone in which the date shall be printed
     * @return string representation of the date in the specified tine zone
     */
    String format(Date date, TimeZone timeZone);

    /**
     * @param date   the date to print
     * @param offset the timezone offset in [ms], with which the date shall be printed.
     *               Must be a multiple of 60000
     * @return string representation of the date with the specified offset
     */
    String format(Date date, int offset);

    /**
     * Prints a date in UTC
     *
     * @param out  print destination
     * @param date the date to print
     * @throws IOException when a print operation fails
     */
    void format(Appendable out, Date date) throws IOException;

    /**
     * Prints a date in the specified time zone
     *
     * @param out      print destination
     * @param date     the date to print
     * @param timeZone the timezone in which the date shall be printed
     * @throws IOException when a print operation fails
     */
    void format(Appendable out, Date date, TimeZone timeZone) throws IOException;

    /**
     * Prints a date with a specified offset
     *
     * @param out    print destination
     * @param date   the date to print
     * @param offset the timezone offset in [ms], with which the date shall be printed.
     *               Must be a multiple of 60000
     * @throws IOException when a print operation fails
     */
    void format(Appendable out, Date date, int offset) throws IOException;
}
