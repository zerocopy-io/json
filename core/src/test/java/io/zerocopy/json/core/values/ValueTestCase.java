package io.zerocopy.json.core.values;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ValueTestCase {
    @Test
    public void scalarToBigDecimal() {
        {
            BigDecimal bigDecimal = Value.scalarToBigInteger(null);
            assertEquals(null, bigDecimal);
        }

        {
            BigDecimal bigDecimal = Value.scalarToBigInteger("42.7");
            assertEquals(new BigDecimal("42.7"), bigDecimal);
        }
    }

    @Test
    public void bigDecimalToBigDecimal() {
        {
            BigDecimal value = new BigDecimal("42.0");
            BigDecimal bigDecimal = Value.scalarToBigInteger(value);
            assertEquals(value, bigDecimal);
            assertSame(value, bigDecimal);
        }
    }

    @Test
    public void max_BigDecimal() {
        assertNull(Value.max((BigDecimal) null, null));

        {
            BigDecimal a = BigDecimal.valueOf(42);
            BigDecimal b = BigDecimal.valueOf(39.9);
            assertSame(a, Value.max(a, b));
            assertSame(a, Value.max(b, a));
            assertSame(a, Value.max(a, null));
            assertSame(a, Value.max(null, a));
        }
    }

    @Test
    public void min_BigDecimal() {
        assertNull(Value.min((BigDecimal) null, null));

        {
            BigDecimal a = BigDecimal.valueOf(42);
            BigDecimal b = BigDecimal.valueOf(42.1);
            assertSame(a, Value.min(a, b));
            assertSame(a, Value.min(b, a));
            assertSame(a, Value.min(a, null));
            assertSame(a, Value.min(null, a));
        }
    }

    @Test
    public void max_Integer() {
        assertNull(Value.max((Integer) null, null));

        {
            int a = 42;
            int b = 39;
            assertSame(a, Value.max(a, b));
            assertSame(a, Value.max(b, a));
            assertSame(a, Value.max(a, null));
            assertSame(a, Value.max(null, a));
        }
    }

    @Test
    public void min_Integer() {
        assertNull(Value.min((Integer) null, null));

        {
            int a = 42;
            int b = 43;
            assertSame(a, Value.min(a, b));
            assertSame(a, Value.min(b, a));
            assertSame(a, Value.min(a, null));
            assertSame(a, Value.min(null, a));
        }
    }

}
