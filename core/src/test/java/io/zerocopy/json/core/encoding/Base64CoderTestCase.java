package io.zerocopy.json.core.encoding;

import org.junit.Test;

import java.nio.charset.Charset;

import static org.junit.Assert.*;

public class Base64CoderTestCase {

    private final Charset charset = Charset.forName("UTF-8");

    @Test
    public void testEncodeDecode() {
        String testString = "sca09fna'0f9v$4\taaa\r\nsadf98a";

        String base64 = Base64Coder.encodeString(testString);

        assertTrue(base64.matches("^([a-zA-Z0-9+/]*={0,2})*$"));

        String decodedString = Base64Coder.decodeString(base64);

        assertEquals(testString, decodedString);
    }

    @Test
    public void testEncodeDecodeBytes() {
        byte[] bytes = "sca09fna'0f9v$4\taaa\r\nsadf98a".getBytes(charset);

        char[] base64 = Base64Coder.encode(bytes);

        assertTrue(new String(base64).matches("^([a-zA-Z0-9+/]*={0,2})*$"));

        byte[] decodedBytes = Base64Coder.decode(base64);

        assertArrayEquals(bytes, decodedBytes);
    }

    @Test
    public void testEncodeDecodeBytesWithOffsetAndLength() {
        byte[] bytes = "sca09fna'0f9v$4\taaa\r\nsadf98a".getBytes(charset);

        char[] base64 = Base64Coder.encode(bytes, bytes.length);

        assertTrue(new String(base64).matches("^([a-zA-Z0-9+/]*={0,2})*$"));

        byte[] decodedBytes = Base64Coder.decode(base64, 0, base64.length);

        assertArrayEquals(bytes, decodedBytes);
    }

    @Test
    public void testEncodeDecodeLines() {
        String testString = "sca0\r9fna'0f9v$4\taaa\r\nsadf\n98a";

        String base64 = Base64Coder.encodeLines(testString.getBytes(), "\n");

        assertTrue(base64.matches("^([a-zA-Z0-9+/\n]{0,76}={0,2})*$"));

        byte[] decodedLines = Base64Coder.decodeLines(base64);

        assertEquals(testString, new String(decodedLines));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecodeInvalidLength() {
        String testString = "sca09fna'0f9v$4\taaa\r\nsadf98a";

        String base64 = Base64Coder.encodeLines(testString.getBytes(), "\n");

        base64 = base64.substring(0, base64.length() / 2) + "a" + base64.substring(base64.length() / 2);

        byte[] decodedLines = Base64Coder.decodeLines(base64);

        assertEquals(testString, new String(decodedLines));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecodeInvalidChar0x01() {
        String testString = "sca09fna'0f9v$4\taaa\r\nsadf98a";

        String base64 = Base64Coder.encodeLines(testString.getBytes(), "\n");

        base64 = base64.substring(0, base64.length() / 2) + "\u0001" + base64.substring(base64.length() / 2 + 1);

        byte[] decodedLines = Base64Coder.decodeLines(base64);

        assertEquals(testString, new String(decodedLines));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecodeInvalidString0x80() {
        String testString = "sca09fna'0f9v$4\taaa\r\nsadf98a";

        String base64 = Base64Coder.encodeLines(testString.getBytes(), "\n");

        base64 = base64.substring(0, base64.length() / 2) + "\u0080" + base64.substring(base64.length() / 2 + 1);

        byte[] decodedLines = Base64Coder.decodeLines(base64);

        assertEquals(testString, new String(decodedLines));
    }
}
