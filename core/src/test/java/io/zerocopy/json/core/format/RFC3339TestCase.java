package io.zerocopy.json.core.format;

import org.junit.Test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class RFC3339TestCase {
    @Test
    public void testDateConversion() {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.DATE);

        Date date = new Date();
        String dateString = converter.format(date);
        Date parsedDate = converter.parse(dateString);

        assertEquals(zeroTimeOfDay(date), zeroTimeOfDay(parsedDate));
    }

    @Test
    public void testTimeConversion() {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.TIME);

        Date date = new Date();
        String dateString = converter.format(date);
        Date parsedDate = converter.parse(dateString);

        assertEquals(date, parsedDate);
    }

    @Test
    public void testDateTimeConversion() {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.DATE_TIME);

        Date date = new Date();
        String dateString = converter.format(date);
        Date parsedDate = converter.parse(dateString);

        assertEquals(date, parsedDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDateFormattingWithInvalidOffset() {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.DATE_TIME);

        converter.format(new Date(), 1234567);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTimeFormattingWithInvalidOffset() {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.TIME);

        converter.format(new Date(), 1234567);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDateTimeFormattingWithInvalidOffset() {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.DATE_TIME);

        converter.format(new Date(), 1234567);
    }

    @Test
    public void testDateParsingWithOffset() throws IOException {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.DATE);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 8 - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 4);

        calendar.set(Calendar.HOUR_OF_DAY, 2);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        calendar.set(Calendar.ZONE_OFFSET, 2 * 3600000);

        testWithCalendar(converter, calendar, "2018-08-04", "2018-08-04");
    }

    @Test
    public void testTimeParsingWithOffset() throws IOException {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.TIME);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 8 - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 4);

        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 13);
        calendar.set(Calendar.SECOND, 29);
        calendar.set(Calendar.MILLISECOND, 920);
        calendar.set(Calendar.ZONE_OFFSET, 2 * 3600000);

        testWithCalendar(converter, calendar, "15:13:29.920+02:00", "13:13:29.920Z");
    }

    @Test
    public void testDateTimeParsingWithOffset() throws IOException {
        RFC3339Converter converter = new RFC3339Converter(RFC3339Converter.Format.DATE_TIME);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 8 - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 4);

        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 13);
        calendar.set(Calendar.SECOND, 29);
        calendar.set(Calendar.MILLISECOND, 920);
        calendar.set(Calendar.ZONE_OFFSET, 2 * 3600000);

        testWithCalendar(converter, calendar, "2018-08-04T15:13:29.920+02:00", "2018-08-04T13:13:29.920Z");
    }

    private static void testWithCalendar(RFC3339Converter converter, Calendar calendar, String dateTimeString, String utcDateTimeString) throws IOException {
        final int offset = calendar.get(Calendar.ZONE_OFFSET);
        final TimeZone timeZoneForOffset = timeZoneForOffset(offset);

        Date expectedDate = calendar.getTime();
        Date parsedDate = converter.parse(dateTimeString);

        if (converter.getFormat() == RFC3339Converter.Format.TIME) {
            expectedDate = setCurrentDate(expectedDate);
            parsedDate = setCurrentDate(parsedDate);
        }
        assertEquals(expectedDate, parsedDate);

        assertEquals(utcDateTimeString, converter.format(parsedDate));
        assertEquals(dateTimeString, converter.format(parsedDate, offset));
        assertEquals(dateTimeString, converter.format(parsedDate, timeZoneForOffset));

        {
            StringBuilder buf = new StringBuilder();
            converter.format(buf, parsedDate);
            assertEquals(utcDateTimeString, buf.toString());
        }
        {
            StringBuilder buf = new StringBuilder();
            converter.format(buf, parsedDate, offset);
            assertEquals(dateTimeString, buf.toString());
        }
        {
            StringBuilder buf = new StringBuilder();
            converter.format(buf, parsedDate, timeZoneForOffset);
            assertEquals(dateTimeString, buf.toString());
        }

        // with exceptions

        {
            boolean notFailed = false;
            try {
                Appendable buf = new FailingAppendable();
                converter.format(buf, parsedDate);
                notFailed = true;
            } catch (IOException ignored) {
            } finally {
                assertFalse(notFailed);
            }
        }
        {
            boolean notFailed = false;
            try {
                Appendable buf = new FailingAppendable();
                converter.format(buf, parsedDate, offset);
                notFailed = true;
            } catch (IOException ignored) {
            } finally {
                assertFalse(notFailed);
            }
        }
        {
            boolean notFailed = false;
            try {
                Appendable buf = new FailingAppendable();
                converter.format(buf, parsedDate, timeZoneForOffset);
                notFailed = true;
            } catch (IOException ignored) {
            } finally {
                assertFalse(notFailed);
            }
        }
    }

    private static TimeZone timeZoneForOffset(int offset) {
        for (String tzId : TimeZone.getAvailableIDs()) {
            TimeZone ailableTimeZone = TimeZone.getTimeZone(tzId);
            if (ailableTimeZone.getRawOffset() == offset) {
                return ailableTimeZone;
            }
        }
        return null;
    }

    private static Date setCurrentDate(Date date) {
        Calendar currentCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);

        calendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, currentCalendar.get(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, currentCalendar.get(Calendar.DAY_OF_MONTH));

        return calendar.getTime();
    }

    private static Date zeroTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    private static class FailingAppendable implements Appendable {
        @Override
        public Appendable append(CharSequence charSequence) throws IOException {
            throw new IOException("test error");
        }

        @Override
        public Appendable append(CharSequence charSequence, int i, int i1) throws IOException {
            throw new IOException("test error");
        }

        @Override
        public Appendable append(char c) throws IOException {
            throw new IOException("test error");
        }
    }
}
