# JSON Validation Based on JSON Schema

## Validator Compliance

* draft-04

* draft-06

* draft-07

    without:
     * format: idn-hostname
     * format: iri
     * format: iri-reference

Tested with: https://github.com/json-schema-org/JSON-Schema-Test-Suite


## Compatibility

Language level: Java 7

Platforms:
* JRE / JDK 
* Android


## Dependency Update

    mvn versions:use-latest-versions
    #verify changes and the build, then
    mvn versions:commit



## Verify the Build

    mvn build-helper:remove-project-artifact
    mvn clean verify
