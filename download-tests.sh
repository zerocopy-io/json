#!/usr/bin/env bash

set -ue

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

cd "$DIR"

TEST_RESOURCE_DIR="validator/src/test/resources/org/json-schema/"

TEST_GIT_DIR="validator/target/tests.git"
TMP_TEST_GIT_DIR="$TEST_GIT_DIR.tmp"

if [[ ! -d "$TEST_GIT_DIR" ]]
then
  rm -rf "$TMP_TEST_GIT_DIR"
  git clone https://github.com/json-schema-org/JSON-Schema-Test-Suite.git "$TMP_TEST_GIT_DIR"

  rm -rf "$TEST_GIT_DIR"
  mv --no-clobber --no-target-directory "$TMP_TEST_GIT_DIR" "$TEST_GIT_DIR"
else
  git -C "$TEST_GIT_DIR" fetch --prune
  git -C "$TEST_GIT_DIR" clean -dxf
  git -C "$TEST_GIT_DIR" checkout --force origin/master
fi

rm -rf "$TEST_RESOURCE_DIR"
git checkout --force -- "$TEST_RESOURCE_DIR"
rm -rf "$TEST_RESOURCE_DIR/tests"
rm -rf "$TEST_RESOURCE_DIR/remotes"
cp --recursive --no-clobber --no-target-directory --verbose "$TEST_GIT_DIR/remotes" "$TEST_RESOURCE_DIR/remotes"
cp --recursive --no-clobber --no-target-directory --verbose "$TEST_GIT_DIR/tests" "$TEST_RESOURCE_DIR/tests"

git reset
git add "$TEST_RESOURCE_DIR"

echo "OK"
