#!/usr/bin/env bash

set -ue

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

RESOURCE_DIR="$DIR/schema/src/main/resources/io/zerocopy/json/schema/"
TMP_RESOURCE_DIR="$DIR/schema/target/schemas.tmp"

rm -rf "$TMP_RESOURCE_DIR"
mkdir -p "$TMP_RESOURCE_DIR"

cd "$TMP_RESOURCE_DIR"

meta_files=(
  "core"
  "applicator"
  "validation"
  "meta-data"
  "format"
  "content"
)

for version in 'draft-00' 'draft-01' 'draft-02' 'draft-03' 'draft-04' 'draft-06' 'draft-07' 'draft/2019-09'; do
  mkdir -p "${version}" \
    "${version}/meta"

  wget "http://json-schema.org/${version}/schema" -O "${version}/schema"
  wget "http://json-schema.org/${version}/hyper-schema" -O "${version}/hyper-schema"

  for meta_file in "${meta_files[@]}"; do
    wget "http://json-schema.org/${version}/meta/${meta_file}" -O "${version}/meta/${meta_file}" || rm -f "${version}/meta/${meta_file}"
  done
done

rm -rf "$RESOURCE_DIR"
mv --no-clobber --no-target-directory "$TMP_RESOURCE_DIR" "$RESOURCE_DIR"

echo "OK"
