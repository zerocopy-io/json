package io.zerocopy.json.schema.merger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.*;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ResourceList;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.SchemaException;
import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.merger.testparams.SchemaDocumentTestParameters;
import io.zerocopy.json.schema.merger.testparams.SchemaTestParameters;
import io.zerocopy.json.schema.merger.testparams.TestParameters;
import io.zerocopy.json.schema.merger.testparams.file.SchemaDocumentTestCase;
import io.zerocopy.json.schema.merger.testparams.file.SchemaTestCase;
import io.zerocopy.json.schema.merger.testparams.file.TestCase;
import io.zerocopy.json.schema.merger.utils.Combinations;
import io.zerocopy.json.schema.merger.utils.PermutationCallbackWithProgress;
import io.zerocopy.json.schema.merger.utils.Permutations;
import io.zerocopy.json.schema.merger.utils.SchemaTestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SchemaMergerTestCase {

    public static final int PERMUTATION_ELEMENT_REPETITIONS = 2;
    public static final int MAX_ELEMENTS_TO_REPEAT = 4;

    private static Collection<URL> listTestResources(final String... relativePaths) {
        ResourceList resourceList = new ClassGraph()
                .acceptPaths(relativePaths)
                .scan()
                .getResourcesWithExtension("json");
        return new HashSet<URL>(resourceList.getURLs());
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> parameters() throws Exception {
        List<Object[]> argumentsList = new ArrayList<Object[]>();

        Map<String, JsonSchema.Version> schemaVersionMapping = new LinkedHashMap<String, JsonSchema.Version>();
        schemaVersionMapping.put("/schema_merge_tests", JsonSchema.Version.DRAFT_7);

        URL resourceBase = SchemaMergerTestCase.class.getResource("/");

        for (Map.Entry<String, JsonSchema.Version> schemaVersionEntry : schemaVersionMapping.entrySet()) {
            String testResourcePath = schemaVersionEntry.getKey();

            for (final URL resourceUrl : listTestResources(testResourcePath)) {
                final JsonNode testJson = objectMapper.readTree(resourceUrl);

                for (JsonNode testCaseTree : testJson) {
                    try {
                        TestCase testCase = objectMapper.treeToValue(testCaseTree, TestCase.class);

                        if (testCase instanceof SchemaTestCase) {
                            for (SchemaTestCase.Test test : ((SchemaTestCase) testCase).tests) {
                                SchemaTestParameters testParameters = new SchemaTestParameters();
                                testParameters.options = test.options;
                                testParameters.description = testCase.description;
                                testParameters.expectedException = test.expectedException;
                                testParameters.expectedProblems = test.expectedProblems;

                                testParameters.schemas = SchemaTestUtils.nodesToReferences(resourceUrl, ((SchemaTestCase) testCase).schemas);
                                testParameters.expectedSchema = test.expectedSchema;

                                appendTestParameters(argumentsList, resourceBase, resourceUrl, testParameters);
                            }
                        } else if (testCase instanceof SchemaDocumentTestCase) {
                            for (SchemaDocumentTestCase.Test test : ((SchemaDocumentTestCase) testCase).tests) {
                                SchemaDocumentTestParameters testParameters = new SchemaDocumentTestParameters();
                                testParameters.options = test.options;
                                testParameters.description = testCase.description;
                                testParameters.expectedException = test.expectedException;
                                testParameters.expectedProblems = test.expectedProblems;

                                testParameters.schemaDocuments = ((SchemaDocumentTestCase) testCase).schemaDocuments;
                                testParameters.expectedSchemaDocument = test.expectedSchemaDocument;

                                appendTestParameters(argumentsList, resourceBase, resourceUrl, testParameters);
                            }
                        } else
                            throw new IllegalArgumentException();

                    } catch (Exception e) {
                        //appendTestParameters(argumentsList, resourceBase, resourceUrl, null);
                        throw new RuntimeException("failed to load test parameters from " + resourceUrl, e);
                    }
                }
            }
        }
        return argumentsList;
    }

    private static void appendTestParameters(List<Object[]> argumentsList, URL resourceBase, URL resourceUrl, TestParameters testParameters) {
        String[] testTrace = {
                testParameters.description,
                getRelativePath(resourceBase, resourceUrl)
        };
        Object[] args = {
                testTrace[1] + " : " + testTrace[0],
                testTrace,
                testParameters,

        };
        argumentsList.add(args);
    }

    private static String getRelativePath(URL resourceBase, URL resourceUrl) {
        String relativePath;
        if (resourceUrl.getPath().startsWith(resourceBase.getPath())) {
            relativePath = resourceUrl.getPath().substring(resourceBase.getPath().length());
        } else
            relativePath = resourceUrl.toString();
        return relativePath;
    }

    private final String[] testTrace;
    private final TestParameters testParameters;

    private static final ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;

    public SchemaMergerTestCase(@SuppressWarnings("unused") String testName, String[] testTrace, TestParameters testParameters) {
        if (testTrace == null || testParameters == null)
            throw new IllegalArgumentException();
        this.testTrace = testTrace;
        this.testParameters = testParameters;
    }

    @Test
    public void merge() throws IOException {
        final SchemaMerger schemaMerger = new SchemaMerger(testParameters.options);

        System.out.println("executing test for \"" + testTrace[0] + "\" \n    at " + testTrace[1] + "(" + (testTrace[1]) + ":1)");

        if (testParameters instanceof SchemaTestParameters) {
            final SchemaTestParameters schemaTestParameters = (SchemaTestParameters) this.testParameters;


            List<SchemaReference> input = Permutations.repeatElements(schemaTestParameters.schemas.size() <= MAX_ELEMENTS_TO_REPEAT ? PERMUTATION_ELEMENT_REPETITIONS : 1, schemaTestParameters.schemas);

            executeTest(BigInteger.ZERO, schemaMerger, schemaTestParameters);
            Permutations.quickPerm(SchemaReference.class, input, new PermutationCallbackWithProgress<SchemaReference>(schemaTestParameters.schemas) {
                @Override
                protected void permutation(BigInteger i, List<SchemaReference> elements) {
                    try {
                        executeTest(i, schemaMerger, schemaTestParameters);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        } else if (testParameters instanceof SchemaDocumentTestParameters) {
            final SchemaDocumentTestParameters schemaDocumentTestParameters = (SchemaDocumentTestParameters) this.testParameters;

            List<SchemaDocument> input = Permutations.repeatElements(((SchemaDocumentTestParameters) testParameters).schemaDocuments.size() <= MAX_ELEMENTS_TO_REPEAT ? PERMUTATION_ELEMENT_REPETITIONS : 1, schemaDocumentTestParameters.schemaDocuments);

            final BigInteger permutationCount = Permutations.factorial(input.size());

            System.out.printf("permutations of %d elements: %d\n", input.size(), permutationCount);

            executeTest(BigInteger.ZERO, schemaMerger, schemaDocumentTestParameters);
            Permutations.quickPerm(SchemaDocument.class, input, new PermutationCallbackWithProgress<SchemaDocument>(schemaDocumentTestParameters.schemaDocuments) {
                @Override
                protected void permutation(BigInteger i, List<SchemaDocument> elements) {
                    try {
                        executeTest(i, schemaMerger, schemaDocumentTestParameters);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        } else
            throw new IllegalArgumentException();
    }

    private static void executeTest(BigInteger i, SchemaMerger schemaMerger, SchemaTestParameters schemaTestParameters) throws IOException {
        List<SchemaReference> schemaReferences = new ArrayList<SchemaReference>();

        SchemaDocument expectedSchema = new SchemaDocument(null, schemaTestParameters.expectedSchema);
        Set<?> expectedProblems = schemaTestParameters.expectedProblems != null ? new TreeSet<Problem>(schemaTestParameters.expectedProblems) : Collections.emptySet();
        SchemaProblem expectedException = schemaTestParameters.expectedException;

        Result<SchemaDocument> result = null;
        SchemaException actualException = null;

        {
            for (SchemaReference schema : schemaTestParameters.schemas)
                schemaReferences.add(schema);

            try {
                result = schemaMerger.merge(schemaReferences);
            } catch (SchemaException e) {
                actualException = e;
            }
        }

        SchemaDocument actualSchema = result != null ? result.getDocument() : null;
        Set<?> actualProblems = result != null && result.getProblems() != null ? new TreeSet<Problem>(result.getProblems()) : Collections.emptySet();


        assertEquals(expectedException, actualException != null ? actualException.getError() : null);
        assertDocumentEqual(true/*i.compareTo(BigInteger.ZERO) != 0*/, expectedSchema, actualSchema);
        assertEquals(expectedProblems, actualProblems);

        if ((expectedSchema != null && !expectedSchema.getRoot().isNull()) || (actualSchema != null && !actualSchema.getRoot().isNull())) {
            SchemaMerger minimizingMerger = new SchemaMerger(SchemaMerger.OPTION_SET_MINIMIZE);
            assertDocumentEqual(
                    true,
                    minimizingMerger.merge(Collections.singletonList(new SchemaReference(expectedSchema))).getDocument(),
                    minimizingMerger.merge(Collections.singletonList(new SchemaReference(actualSchema))).getDocument()
            );
        }
    }

    private static void executeTest(BigInteger i, SchemaMerger schemaMerger, SchemaDocumentTestParameters schemaDocumentTestParameters) throws IOException {
        SchemaDocument expectedDocument = schemaDocumentTestParameters.expectedSchemaDocument;
        Set<?> expectedProblems = schemaDocumentTestParameters.expectedProblems != null ? new TreeSet<Problem>(schemaDocumentTestParameters.expectedProblems) : Collections.emptySet();
        SchemaProblem expectedException = schemaDocumentTestParameters.expectedException;


        Result<SchemaDocument> result = null;
        SchemaProblem actualException = null;
        try {
            Collection<SchemaDocument> schemaDocuments = schemaDocumentTestParameters.schemaDocuments;
            result = schemaMerger.mergeDocuments(schemaDocuments);
        } catch (SchemaException e) {
            actualException = e.getError();
        }
        SchemaDocument actualDocument = result != null ? result.getDocument() : null;
        Set<?> actualProblems = result != null && result.getProblems() != null ? new TreeSet<Problem>(result.getProblems()) : Collections.emptySet();


        assertEquals(expectedException, actualException);
        assertEquals(expectedProblems, actualProblems);
        assertDocumentEqual(i.compareTo(BigInteger.ZERO) != 0, expectedDocument, actualDocument);
        assertEquals(expectedDocument.getSchemaParents(), actualDocument.getSchemaParents());

        if ((expectedDocument.getRoot() != null && !expectedDocument.getRoot().isNull()) || (actualDocument.getRoot() != null && !actualDocument.getRoot().isNull())) {
            for (SchemaMerger.Option[] optionsArray : Combinations.allOf(SchemaMerger.Option.class, SchemaMerger.Option.values())) {
                EnumSet<SchemaMerger.Option> options = EnumSet.noneOf(SchemaMerger.Option.class);
                options.addAll(Arrays.asList(optionsArray));

                SchemaMerger minimizingMerger = new SchemaMerger(options);
                assertNodeEqual(
                        true,
                        minimizingMerger.mergeDocuments(Collections.singletonList(expectedDocument)).getDocument().getRoot(),
                        minimizingMerger.mergeDocuments(Collections.singletonList(actualDocument)).getDocument().getRoot()
                );
            }
        }
    }

    public static void assertNodeEqual(boolean normalize, JsonNode a, JsonNode b) throws IOException {
        if (a instanceof NullNode)
            a = null;
        if (b instanceof NullNode)
            b = null;

        if (normalize) {
            assertEquals(nodeToNormalizedSchemaString(a), nodeToNormalizedSchemaString(b));
        } else
            assertEquals(objectMapper.writeValueAsString(a), objectMapper.writeValueAsString(b));
    }

    public static void assertDocumentEqual(boolean normalize, SchemaDocument a, SchemaDocument b) throws IOException {
        assertNodeEqual(normalize, a != null ? a.getRoot() : null, b != null ? b.getRoot() : null);
    }

    private static String nodeToNormalizedSchemaString(JsonNode node) throws IOException {
        if (node == null)
            return null;

        node = node.deepCopy();
        SchemaUtils.visitAllNodes(node, new SchemaUtils.NodeVisitor() {
            @Override
            public void push(LinkedList<String> path, JsonNode node) {
                if (node instanceof FloatNode || node instanceof DoubleNode) {
                    throw new IllegalArgumentException();
                }
            }

            private void sortObjectProperties(ObjectNode object) {
                if (object == null || object.size() == 0)
                    return;

                TreeMap<String, JsonNode> sortedFields = new TreeMap<String, JsonNode>();
                for (Iterator<Map.Entry<String, JsonNode>> it = object.fields(); it.hasNext(); ) {
                    Map.Entry<String, JsonNode> field = it.next();
                    sortedFields.put(field.getKey(), field.getValue());
                }
                object.removeAll();
                object.setAll(sortedFields);
            }

            private void sortUniqueArray(ArrayNode array) {
                if (array == null || array.size() == 0)
                    return;

                TreeSet<JsonNode> sortedUniqueValues = new TreeSet<JsonNode>(new Comparator<JsonNode>() {
                    @Override
                    public int compare(JsonNode a, JsonNode b) {
                        if (a == b)
                            return 0;
                        if (a == null || b == null)
                            return a == null ? -1 : 1;
                        return a.toString().compareTo(b.toString());
                    }
                });
                for (JsonNode arrayElem : array)
                    if (!sortedUniqueValues.add(arrayElem))
                        throw new IllegalArgumentException("duplicate array element: " + arrayElem);

                array.removeAll();
                array.addAll(sortedUniqueValues);
            }

            @Override
            public void pop(LinkedList<String> path, JsonNode node) {
                if (node instanceof ObjectNode) {
                    sortUniqueArray((ArrayNode) node.get("required"));
                    sortUniqueArray((ArrayNode) node.get("enum"));
                    sortUniqueArray((ArrayNode) node.get("allOf"));
                    sortUniqueArray((ArrayNode) node.get("anyOf"));
                    sortUniqueArray((ArrayNode) node.get("oneOf"));

                    sortObjectProperties((ObjectNode) node);
                }
            }
        });

        return objectMapper.writeValueAsString(node);
    }

}
