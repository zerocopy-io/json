package io.zerocopy.json.schema.merger.utils;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Permutations {

    public interface PermutationCallback<E> {
        void startPermutations(int size);

        void start(int size);

        void elem(int index, E elem);

        void end();

        void endPermutations();
    }

    public static <E> List<E> repeatElements(int n, List<E> input) {
        if (n == 0)
            return Collections.emptyList();
        else if (n == 1)
            return input;

        List<E> result = new ArrayList<E>(n * input.size());

        for (int i = 0; i < input.size() * n; ++i) {
            result.add(input.get(i / n));
        }

        return result;
    }

    private static BigInteger recfact(long start, long n) {
        long i;
        if (n <= 16) {
            BigInteger r = BigInteger.valueOf(start);
            for (i = start + 1; i < start + n; i++) r = r.multiply(BigInteger.valueOf(i));
            return r;
        }
        i = n / 2;
        return recfact(start, i).multiply(recfact(start + i, n - i));
    }

    public static BigInteger factorial(long n) {
        return recfact(1, n);
    }

    public static <E> void quickPerm(Class<E> elementType, Collection<E> input, PermutationCallback<E> out) {
        //noinspection unchecked
        quickPerm(elementType, input.toArray((E[]) Array.newInstance(elementType, 0)), out);
    }

    /**
     * Generates permutations iteratively, using a state array.
     * Yields all <code>input.length</code>! permutations.
     * <p>
     * See http://www.quickperm.org/
     */
    public static <E> void quickPerm(Class<E> elementType, E[] input, PermutationCallback<E> out) {
        /*
         * Unrolling the stirling formula (n! ~ sqrt(2 * PI * n) * (n / e)^n)
         * for the required precision looked too tricky at the first glance and
         * would probably not be faster than this state array based approach,
         * even when integer/fixed point arithmetic is used.
         *
         * Furthermore, the state array would never exceed a few dozen elements
         * in practice anyway, thus, it may fit into the lowest level caches,
         * (e.g. n <= 64 integers in 4 * 64B cache lines).
         *
         * The array could thus also be squashed to 8bit or 16bit elements, what
         * on the other hand could change machine specific extraction &
         * insertion costs when loading/storing elements to/from registers.
         */
        @SuppressWarnings("unchecked")
        E[] output = (E[]) Array.newInstance(elementType, input.length);
        System.arraycopy(input, 0, output, 0, input.length);

        int n = output.length;
        int state[] = new int[n];
        int i = 1;

        out.startPermutations(output.length);

        {
            // emit
            out.start(output.length);
            for (int k = 0; k < output.length; ++k)
                out.elem(k, output[k]);
            out.end();
        }

        while (i < n) {
            if (state[i] < i) {
                int j = i % 2 != 0 ? state[i] : 0;
                {
                    E tmp = output[i];
                    output[i] = output[j];
                    output[j] = tmp;
                }
                ++state[i];
                i = 1;

                {
                    // emit
                    out.start(output.length);
                    for (int k = 0; k < output.length; ++k)
                        out.elem(k, output[k]);
                    out.end();
                }
            } else {
                assert state[i] == i;
                state[i] = 0;
                ++i;
            }
        }

        out.endPermutations();
    }
}
