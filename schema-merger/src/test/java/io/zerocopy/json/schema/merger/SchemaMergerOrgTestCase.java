package io.zerocopy.json.schema.merger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ResourceList;
import io.zerocopy.json.schema.SchemaException;
import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.merger.testparams.OrgSchemaTestParameters;
import io.zerocopy.json.schema.merger.testparams.TestParameters;
import io.zerocopy.json.schema.merger.utils.PermutationCallbackWithProgress;
import io.zerocopy.json.schema.merger.utils.Permutations;
import io.zerocopy.json.validator.impl.OfflineTestDocumentLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SchemaMergerOrgTestCase {

    public static final int PERMUTATION_ELEMENT_REPETITIONS = 2;
    public static final int MAX_ELEMENTS_TO_REPEAT = 4;

    private static Collection<URL> listTestResources(final String... relativePaths) {
        ResourceList resourceList = new ClassGraph()
                .acceptPaths(relativePaths)
                .scan()
                .getResourcesWithExtension("json");
        return new HashSet<URL>(resourceList.getURLs());
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> parameters() throws Exception {
        List<Object[]> argumentsList = new ArrayList<Object[]>();

        URL resourceBase = SchemaMergerOrgTestCase.class.getResource("/");
        String testResourceBaseDir = "org/json-schema/tests/draft7";

        for (final URL resourceUrl : listTestResources(testResourceBaseDir)) {
            JsonNode testJson = objectMapper.readTree(resourceUrl);

            ArrayNode tests = (ArrayNode) testJson;

            for (JsonNode test : tests) {
                String description = test.get("description").asText();
                JsonNode schema = test.get("schema");

                OrgSchemaTestParameters testParameters = new OrgSchemaTestParameters();
                testParameters.options = null;
                testParameters.description = description;
                testParameters.expectedException = null;
                testParameters.expectedProblems = null;

                testParameters.schemas = new ArrayList<SchemaDocument>();
                testParameters.schemas.add(new SchemaDocument(resourceUrl.toURI(), schema));

                appendTestParameters(argumentsList, resourceBase, resourceUrl, testParameters);
            }
        }

        if (argumentsList.isEmpty())
            throw new IllegalStateException("missing test data");

        return argumentsList;
    }

    private static void appendTestParameters(List<Object[]> argumentsList, URL resourceBase, URL resourceUrl, TestParameters testParameters) {
        String[] testTrace = {
                testParameters.description,
                getRelativePath(resourceBase, resourceUrl)
        };
        Object[] args = {
                testTrace[1] + " : " + testTrace[0],
                testTrace,
                testParameters,

        };
        argumentsList.add(args);
    }

    private static String getRelativePath(URL resourceBase, URL resourceUrl) {
        String relativePath;
        if (resourceUrl.getPath().startsWith(resourceBase.getPath())) {
            relativePath = resourceUrl.getPath().substring(resourceBase.getPath().length());
        } else
            relativePath = resourceUrl.toString();
        return relativePath;
    }

    private final String[] testTrace;
    private final OrgSchemaTestParameters testParameters;

    private static final ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;

    public SchemaMergerOrgTestCase(@SuppressWarnings("unused") String testName, String[] testTrace, OrgSchemaTestParameters testParameters) {
        this.testTrace = testTrace;
        this.testParameters = testParameters;
    }

    @Test
    public void merge() throws IOException {
        final SchemaMerger schemaMerger = new SchemaMerger(testParameters.options);
        schemaMerger.setDocumentLoader(new OfflineTestDocumentLoader());

        System.out.println("executing test for \"" + testTrace[0] + "\" \n    at " + testTrace[1] + "(" + (testTrace[1]) + ":1)");

        List<SchemaDocument> input = Permutations.repeatElements(testParameters.schemas.size() <= MAX_ELEMENTS_TO_REPEAT ? PERMUTATION_ELEMENT_REPETITIONS : 1, testParameters.schemas);

        executeTest(BigInteger.ZERO, schemaMerger, testParameters);
        Permutations.quickPerm(SchemaDocument.class, input, new PermutationCallbackWithProgress<SchemaDocument>(testParameters.schemas) {
            @Override
            protected void permutation(BigInteger i, List<SchemaDocument> elements) {
                try {
                    executeTest(i, schemaMerger, testParameters);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private void executeTest(BigInteger i, SchemaMerger schemaMerger, OrgSchemaTestParameters schemaTestParameters) throws IOException {
        SchemaDocument expectedSchema;
        Set<?> expectedProblems;

        {
            Result<SchemaDocument> mergerResult = schemaMerger.merge(SchemaReference.fromDocuments(schemaTestParameters.schemas));

            expectedSchema = mergerResult.getDocument();
            expectedProblems = schemaTestParameters.expectedProblems != null ? new TreeSet<Problem>(schemaTestParameters.expectedProblems) : Collections.emptySet();
        }


        List<SchemaReference> schemaReferences = new ArrayList<SchemaReference>();

        SchemaProblem expectedException = schemaTestParameters.expectedException;

        Result<SchemaDocument> result = null;
        SchemaException actualException = null;

        {
            for (SchemaDocument schemaDocument : schemaTestParameters.schemas)
                schemaReferences.add(new SchemaReference(schemaDocument));

            try {
                result = schemaMerger.merge(schemaReferences);
            } catch (SchemaException e) {
                actualException = e;
            }
        }


        SchemaDocument actualSchema = result != null ? result.getDocument() : null;

        assertEquals(expectedException, actualException != null ? actualException.getError() : null);
        SchemaMergerTestCase.assertDocumentEqual(i.compareTo(BigInteger.ZERO) != 0, expectedSchema, actualSchema);

        //Set<?> actualProblems = result != null && result.getProblems() != null ? new TreeSet<Problem>(result.getProblems()) : Collections.emptySet();
        //assertEquals(expectedProblems, actualProblems);

        if ((expectedSchema != null && expectedSchema.getRoot() != null && !expectedSchema.getRoot().isNull()) || (actualSchema != null && actualSchema.getRoot() != null && !actualSchema.getRoot().isNull())) {
            EnumSet<SchemaMerger.Option> options = EnumSet.noneOf(SchemaMerger.Option.class);
            if (testParameters.options != null)
                options.addAll(testParameters.options);
            options.addAll(SchemaMerger.OPTION_SET_MINIMIZE);

            SchemaMerger minimizingMerger = new SchemaMerger(options);
            minimizingMerger.setDocumentLoader(schemaMerger.getDocumentLoader());
            SchemaMergerTestCase.assertDocumentEqual(
                    true,
                    minimizingMerger.merge(Collections.singletonList(new SchemaReference(expectedSchema))).getDocument(),
                    minimizingMerger.merge(Collections.singletonList(new SchemaReference(actualSchema))).getDocument()
            );
        }
    }

}
