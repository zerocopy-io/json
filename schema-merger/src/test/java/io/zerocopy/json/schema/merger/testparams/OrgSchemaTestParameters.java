package io.zerocopy.json.schema.merger.testparams;

import io.zerocopy.json.schema.document.SchemaDocument;

import java.util.List;

public class OrgSchemaTestParameters extends TestParameters {
    public List<SchemaDocument> schemas;
}
