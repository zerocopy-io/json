package io.zerocopy.json.schema.merger.testparams;

import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.merger.Problem;
import io.zerocopy.json.schema.merger.SchemaMerger;

import java.util.Set;
import java.util.TreeSet;

public class TestParameters {
    public Set<SchemaMerger.Option> options;
    public String description;
    public SchemaProblem expectedException;
    public TreeSet<Problem> expectedProblems;
}