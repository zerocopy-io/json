package io.zerocopy.json.schema.merger.utils;

import java.math.BigInteger;
import java.util.Iterator;

public interface BigListIterator<E> extends Iterator<E> {
    @Override
    public boolean hasNext();

    @Override
    public E next();

    public boolean hasPrevious();

    public E previous();

    public BigInteger nextIndex();

    public BigInteger previousIndex();

    @Override
    public void remove();

    public void set(E e);

    public void add(E e);
}
