package io.zerocopy.json.schema.merger.testparams.file;

import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.merger.Problem;
import io.zerocopy.json.schema.merger.SchemaMerger;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SchemaDocumentTestCase extends TestCase {
    public List<SchemaDocument> schemaDocuments;
    public List<Test> tests;

    public static class Test {
        public Set<SchemaMerger.Option> options;

        public SchemaDocument expectedSchemaDocument;
        public SchemaProblem expectedException;
        public TreeSet<Problem> expectedProblems;
    }
}
