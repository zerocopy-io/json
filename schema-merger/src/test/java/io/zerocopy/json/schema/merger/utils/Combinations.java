package io.zerocopy.json.schema.merger.utils;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Iterator;

public class Combinations {
    public static <T> BigList<T[]> allOf(final Class<T> elementType, final T[] in) {
        return new BigList<T[]>() {
            private BigInteger size = BigInteger.valueOf(2).pow(in.length);

            @Override
            public BigListIterator<T[]> listIterator() {
                return new BigListIterator<T[]>() {
                    private BigInteger i = BigInteger.ZERO;

                    @Override
                    public boolean hasNext() {
                        return i.compareTo(size) < 0;
                    }

                    @Override
                    public T[] next() {
                        T[] next = createCombination(elementType, in, i);
                        i = i.add(BigInteger.ONE);
                        return next;
                    }

                    @Override
                    public boolean hasPrevious() {
                        return i.compareTo(BigInteger.ZERO) >= 0;
                    }

                    @Override
                    public T[] previous() {
                        i = i.subtract(BigInteger.ONE);
                        T[] previous = createCombination(elementType, in, i);
                        return previous;
                    }

                    @Override
                    public BigInteger nextIndex() {
                        return i;
                    }

                    @Override
                    public BigInteger previousIndex() {
                        return i.subtract(BigInteger.ONE);
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public void set(T[] ts) {
                        throw new UnsupportedOperationException();
                    }

                    @Override
                    public void add(T[] ts) {
                        throw new UnsupportedOperationException();
                    }
                };
            }

            @Override
            public Iterator<T[]> iterator() {
                return listIterator();
            }

            @Override
            public BigInteger size() {
                return size;
            }
        };
    }

    private static <T> T[] createCombination(final Class<T> elementType, T[] in, BigInteger mask) {
        @SuppressWarnings("unchecked")
        T[] next = (T[]) Array.newInstance(elementType, mask.bitCount());
        int outIndex = 0;
        for (int inIndex = 0; inIndex < mask.bitLength(); ++inIndex) {
            boolean bitValue = mask.testBit(inIndex);
            if (bitValue) {
                next[outIndex] = in[inIndex];
                ++outIndex;
            }
        }
        if (outIndex != next.length)
            throw new AssertionError();
        return next;
    }
}
