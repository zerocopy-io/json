package io.zerocopy.json.schema.merger.testparams;

import com.fasterxml.jackson.databind.JsonNode;
import io.zerocopy.json.schema.document.SchemaReference;

import java.util.List;

public class SchemaTestParameters extends TestParameters {
    public List<SchemaReference> schemas;

    public JsonNode expectedSchema;
}
