package io.zerocopy.json.schema.merger;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import io.zerocopy.json.schema.document.SchemaDocument;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedHashSet;

import static org.junit.Assert.*;

public class SchemaMergerUtilsTestCase {

    @Test
    public void eq() {
        assertTrue(SchemaMergerUtils.eq(null, null));
        assertTrue(SchemaMergerUtils.eq(JsonNodeFactory.instance.objectNode(), JsonNodeFactory.instance.objectNode()));
        assertFalse(SchemaMergerUtils.eq(JsonNodeFactory.instance.objectNode(), null));
        assertFalse(SchemaMergerUtils.eq(null, JsonNodeFactory.instance.objectNode()));

        assertTrue(SchemaMergerUtils.eq(
                JsonNodeFactory.instance.objectNode()
                        .put("a", 42),
                JsonNodeFactory.instance.objectNode()
                        .put("a", 42)
        ));
        assertFalse(SchemaMergerUtils.eq(
                JsonNodeFactory.instance.objectNode(),
                JsonNodeFactory.instance.objectNode()
                        .put("a", 42)
        ));
    }

    @Test
    public void arrayIntersect_Distinct() {
        Integer[] a = {42};
        Integer[] b = {43};
        Integer[] r = {};
        assertArrayEquals(r, SchemaMergerUtils.arrayIntersect(a, b));
        assertArrayEquals(r, SchemaMergerUtils.arrayIntersect(b, a));
        assertArrayEquals(a, SchemaMergerUtils.arrayIntersect(a, null));
        assertArrayEquals(a, SchemaMergerUtils.arrayIntersect(null, a));
        assertArrayEquals(b, SchemaMergerUtils.arrayIntersect(b, null));
        assertArrayEquals(b, SchemaMergerUtils.arrayIntersect(null, b));
    }

    @Test
    public void arrayIntersect_Intersecting() {
        Integer[] a = {41, 42, 42};
        Integer[] b = {40, 41, 42};
        Integer[] r = {41, 42};
        assertArrayEquals(r, SchemaMergerUtils.arrayIntersect(a, b));
        assertArrayEquals(r, SchemaMergerUtils.arrayIntersect(b, a));
        assertArrayEquals(a, SchemaMergerUtils.arrayIntersect(a, null));
        assertArrayEquals(a, SchemaMergerUtils.arrayIntersect(null, a));
        assertArrayEquals(b, SchemaMergerUtils.arrayIntersect(b, null));
        assertArrayEquals(b, SchemaMergerUtils.arrayIntersect(null, b));
    }

    @Test
    public void collectionIntersect_Distinct() {
        LinkedHashSet<Integer> a = new LinkedHashSet<Integer>(Arrays.asList(42));
        LinkedHashSet<Integer> b = new LinkedHashSet<Integer>(Arrays.asList(43));
        LinkedHashSet<Integer> r = new LinkedHashSet<Integer>();
        assertEquals(r, SchemaMergerUtils.collectionIntersect(a, b));
        assertEquals(r, SchemaMergerUtils.collectionIntersect(b, a));
        assertEquals(a, SchemaMergerUtils.collectionIntersect(a, null));
        assertEquals(a, SchemaMergerUtils.collectionIntersect(null, a));
        assertEquals(b, SchemaMergerUtils.collectionIntersect(b, null));
        assertEquals(b, SchemaMergerUtils.collectionIntersect(null, b));
    }

    @Test
    public void collectionIntersect_Intersecting() {
        LinkedHashSet<Integer> a = new LinkedHashSet<Integer>(Arrays.asList(41, 42, 42));
        LinkedHashSet<Integer> b = new LinkedHashSet<Integer>(Arrays.asList(40, 41, 42));
        LinkedHashSet<Integer> r = new LinkedHashSet<Integer>(Arrays.asList(41, 42));
        assertEquals(r, SchemaMergerUtils.collectionIntersect(a, b));
        assertEquals(r, SchemaMergerUtils.collectionIntersect(b, a));
        assertEquals(a, SchemaMergerUtils.collectionIntersect(a, null));
        assertEquals(a, SchemaMergerUtils.collectionIntersect(null, a));
        assertEquals(b, SchemaMergerUtils.collectionIntersect(b, null));
        assertEquals(b, SchemaMergerUtils.collectionIntersect(null, b));
    }

    @Test
    public void putNode_newRoot() {
        SchemaDocument schemaDocument = new SchemaDocument();

        assertNull(schemaDocument.getRoot());

        JsonNode newRoot = JsonNodeFactory.instance.objectNode();

        JsonPointer pointer = JsonPointer.compile("");

        SchemaMergerUtils.putNode(schemaDocument, pointer, newRoot);

        assertSame(newRoot, schemaDocument.getRoot());
        assertSame(newRoot, schemaDocument.getRoot().at(pointer));
    }

    @Test
    public void putNode_newChild() {
        SchemaDocument schemaDocument = new SchemaDocument();

        assertNull(schemaDocument.getRoot());

        JsonNode newChild = JsonNodeFactory.instance.objectNode();

        JsonPointer pointer = JsonPointer.compile("/path/to/child");

        SchemaMergerUtils.putNode(schemaDocument, pointer, newChild);

        assertNotNull(schemaDocument.getRoot());
        assertSame(newChild, schemaDocument.getRoot().at(pointer));
    }
}
