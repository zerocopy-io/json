package io.zerocopy.json.schema.merger.utils;


public interface BigList<E> extends BigCollection<E> {
    BigListIterator<E> listIterator();
}
