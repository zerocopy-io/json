package io.zerocopy.json.schema.merger.utils;

import com.fasterxml.jackson.databind.JsonNode;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SchemaTestUtils {
    public static List<SchemaReference> nodesToReferences(final URL baseUrl, Collection<JsonNode> nodes) {
        ArrayList<SchemaReference> list = new ArrayList<SchemaReference>();
        for (JsonNode node : nodes)
            try {
                list.add(SchemaReference.create(new SchemaDocument(baseUrl.toURI(), node), "", node));
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        return list;
    }

    public static List<SchemaReference> documentsToReferences(Collection<SchemaDocument> documents) {
        ArrayList<SchemaReference> list = new ArrayList<SchemaReference>();
        for (SchemaDocument document : documents)
            list.add(SchemaReference.create(document, "", document.getRoot()));
        return list;
    }

    public static List<SchemaDocument> referencesToDocumnts(Collection<SchemaReference> references) {
        ArrayList<SchemaDocument> list = new ArrayList<SchemaDocument>();
        for (SchemaReference reference : references) {
            SchemaDocument document = new SchemaDocument(null, reference.getSchema());
            list.add(document);
        }
        return list;
    }
}
