package io.zerocopy.json.schema.merger.testparams.file;

import com.fasterxml.jackson.databind.JsonNode;
import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.merger.Problem;
import io.zerocopy.json.schema.merger.SchemaMerger;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SchemaTestCase extends TestCase {
    public List<JsonNode> schemas;
    public List<Test> tests;

    public static class Test {
        public Set<SchemaMerger.Option> options;

        public JsonNode expectedSchema;
        public SchemaProblem expectedException;
        public TreeSet<Problem> expectedProblems;
    }
}
