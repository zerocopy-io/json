package io.zerocopy.json.schema.merger.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public abstract class PermutationCallbackWithProgress<T> implements Permutations.PermutationCallback<T> {
    private BigInteger i;
    private BigInteger permutationCount;
    private List<T> elements;
    private long lastProgressOutput = 0;

    public PermutationCallbackWithProgress(List<T> elements) {
        this.elements = elements;
    }

    @Override
    public void startPermutations(int size) {
        i = BigInteger.ZERO;
        permutationCount = Permutations.factorial(size);
        printProgress(i, permutationCount);
    }

    @Override
    public void start(int size) {
        elements.clear();
    }

    @Override
    public void elem(int index, T elem) {
        elements.add(elem);
    }

    @Override
    public void end() {
        i = i.add(BigInteger.ONE);
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastProgressOutput > 1000) {
            lastProgressOutput = currentTime;
            printProgress(i, permutationCount);
        }
        permutation(i, elements);
    }

    @Override
    public void endPermutations() {
        printProgress(i, permutationCount);
    }

    private static void printProgress(BigInteger i, BigInteger n) {
        BigDecimal percentage = new BigDecimal(i).multiply(BigDecimal.valueOf(100)).divide(new BigDecimal(n), 3, BigDecimal.ROUND_DOWN);
        System.out.println(i + " / " + n + " = " + (percentage + "%"));
    }

    protected abstract void permutation(BigInteger i, List<T> elements);
}
