package io.zerocopy.json.schema.merger.utils;


import java.math.BigInteger;
import java.util.Iterator;

public interface BigCollection<E> extends Iterable<E> {
    Iterator<E> iterator();

    BigInteger size();
}
