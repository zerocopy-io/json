package io.zerocopy.json.schema.merger.testparams.file;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonSubTypes({
        @JsonSubTypes.Type(value = SchemaTestCase.class, name = "schemaTest"),
        @JsonSubTypes.Type(value = SchemaDocumentTestCase.class, name = "schemaDocumentTest")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
public class TestCase {
    public String type;
    public String description;
}