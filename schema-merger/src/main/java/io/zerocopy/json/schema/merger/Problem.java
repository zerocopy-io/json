package io.zerocopy.json.schema.merger;

import io.zerocopy.json.core.values.Value;
import io.zerocopy.json.schema.SchemaProblem;

public class Problem implements Comparable<Problem> {
    public enum Severity {
        ERROR,
        WARNING,
        INFO
    }

    private SchemaProblem type;
    private Severity severity;
    private String message;

    public Problem() {
    }

    public Problem(SchemaProblem type, Severity severity, String message) {
        this.type = type;
        this.severity = severity;
        this.message = message;
    }

    public Problem(SchemaProblem type, Severity severity) {
        this(type, severity, null);
    }

    public SchemaProblem getType() {
        return type;
    }

    public void setType(SchemaProblem type) {
        this.type = type;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int compareTo(Problem problem) {
        int d;
        d = this.type.compareTo(problem.type);
        if (d != 0) return d;
        d = this.severity.compareTo(problem.severity);
        if (d != 0) return d;
        d = this.message == null || problem.message == null ? (this.message == problem.message ? 0 : (this.message == null ? -1 : 1)) : this.message.compareTo(problem.message);
        return d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Problem problem = (Problem) o;
        return type == problem.type &&
                severity == problem.severity &&
                Value.equals(message, problem.message);
    }

    @Override
    public int hashCode() {
        return Value.hash(type, severity, message);
    }

    @Override
    public String toString() {
        return "Problem{" +
                "type=" + type +
                ", severity=" + severity +
                ", message='" + message + '\'' +
                '}';
    }
}
