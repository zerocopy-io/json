package io.zerocopy.json.schema.merger;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.core.values.Value;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;

import java.lang.reflect.Array;
import java.util.*;


public class SchemaMergerUtils {

    public static boolean eq(JsonNode a, JsonNode b) {
        return Value.equals(a, b);
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] arrayIntersect(T[] a, T[] b) {
        if (b != null) {
            if (a != null) {
                Set<T> intersection = collectionIntersect(Arrays.asList(a), Arrays.asList(b));
                return intersection
                        .toArray((T[]) Array.newInstance(a.getClass().getComponentType(), intersection.size()));
            }
            return b;
        }
        return a;
    }

    public static <T> Set<T> collectionIntersect(Collection<T>... collections) {
        Set<T> intersection = new LinkedHashSet<T>();

        int count = 0;

        for (Collection<T> collection : collections) {
            if (collection != null)
                if (count++ == 0)
                    intersection.addAll(collection);
                else
                    intersection.retainAll(collection);
        }

        return intersection;
    }

    public static void putNode(SchemaDocument outputSchema, JsonPointer pointer, JsonNode value) {
        ObjectNode node = (ObjectNode) outputSchema.getRoot();
        if (node == null) {
            node = JsonNodeFactory.instance.objectNode();
            outputSchema.setRoot(node);
        }
        List<String> pathElems = JsonPointerUtils.split(pointer.toString());
        if (pathElems.size() > 0) {
            for (int i = 0; i < pathElems.size() - 1; ++i) {
                String elem = pathElems.get(i);
                ObjectNode childNode = (ObjectNode) node.get(elem);
                if (childNode == null) {
                    childNode = JsonNodeFactory.instance.objectNode();
                    JsonNode oldNode = node.replace(elem, childNode);
                    if (oldNode != null && oldNode != childNode)
                        throw new IllegalArgumentException("duplicate node at " + pointer);
                }
                node = childNode;
            }
            node.set(pathElems.get(pathElems.size() - 1), value);
        } else {
            outputSchema.setRoot(value);
        }
    }

    public static ArrayNode arrayAddUnique(ArrayNode arrayNode, JsonNode element) {
        if (arrayNode != null)
            for (JsonNode existingElement : arrayNode) {
                if (SchemaMergerUtils.eq(element, existingElement)) {
                    return arrayNode;
                }
            }
        else
            arrayNode = JsonNodeFactory.instance.arrayNode(1);

        arrayNode.add(element);

        return arrayNode;
    }

    public static ArrayNode arrayMergeUnique(ArrayNode a, ArrayNode b) {
        ArrayNode result = a != null || b != null ? JsonNodeFactory.instance.arrayNode() : null;

        if (a != null)
            for (JsonNode element : a)
                result = arrayAddUnique(result, element);
        if (b != null)
            for (JsonNode element : b)
                result = arrayAddUnique(result, element);

        return result;
    }

    public static ArrayNode arrayUnique(ArrayNode arrayNode) {
        return arrayMergeUnique(null, arrayNode);
    }

    public static ArrayNode arrayIntersectUnique(ArrayNode a, ArrayNode b) {
        if (a == null && b == null)
            return null;

        ArrayNode result = JsonNodeFactory.instance.arrayNode();

        if (a != null && b != null) {
            for (JsonNode elemA : a) {
                for (JsonNode elemB : b) {
                    if (SchemaMergerUtils.eq(elemA, elemB)) {
                        SchemaMergerUtils.arrayAddUnique(result, elemA);
                        break;
                    }
                }
            }
        } else
            result = arrayMergeUnique(result, a != null ? a : b);

        return result;
    }
}
