package io.zerocopy.json.schema.merger;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.core.ObjectUtils;
import io.zerocopy.json.core.values.Value;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.SchemaException;
import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.context.SchemaResolutionContext;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.loader.DocumentLoader;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class SchemaMerger {
    private SchemaResolutionContext context = new SchemaResolutionContext();

    public enum Option {
        /**
         * Determines, whether contradicting schemas will be converted to a constant mismatch or a {@link SchemaException} shall be thrown.
         */
        MERGE_CONST_INTO_ENUM,
        MERGE_SINGLE_ANY_OF_TO_ALL_OF,
        MERGE_SINGLE_ONE_OF_TO_ALL_OF,
        REMOVE_PROPERTIES_THAT_MATCH_ADDITIONAL_PROPERTIES
    }

    public static final Set<Option> OPTION_SET_MERGING_ACROSS_RULES;

    static {
        OPTION_SET_MERGING_ACROSS_RULES = Collections.unmodifiableSet(EnumSet.of(
                Option.MERGE_CONST_INTO_ENUM,
                Option.MERGE_SINGLE_ANY_OF_TO_ALL_OF,
                Option.MERGE_SINGLE_ONE_OF_TO_ALL_OF
        ));
    }

    public static final Set<Option> OPTION_SET_MINIMIZE;

    static {
        EnumSet<Option> minimizeOptions = EnumSet.copyOf(OPTION_SET_MERGING_ACROSS_RULES);
        minimizeOptions.add(Option.REMOVE_PROPERTIES_THAT_MATCH_ADDITIONAL_PROPERTIES);
        OPTION_SET_MINIMIZE = Collections.unmodifiableSet(minimizeOptions);
    }

    public static final EnumSet<Option> DEFAULT_OPTIONS = EnumSet.noneOf(Option.class);

    private final Set<Option> options;

    public SchemaMerger(Set<Option> options) {
        this.options = options != null ? options : Collections.<Option>emptySet();
    }

    public SchemaMerger() {
        this(DEFAULT_OPTIONS);
    }

    public DocumentLoader getDocumentLoader() {
        return context.getDocumentLoader();
    }

    public void setDocumentLoader(DocumentLoader documentLoader) {
        this.context.setDocumentLoader(documentLoader);
    }

    public Result<SchemaDocument> merge(Collection<SchemaReference> schemas) throws IOException {
        return merge(false, schemas);
    }

    private Result<SchemaDocument> merge(boolean firstIsMerged, Collection<SchemaReference> schemas) throws IOException {
        if (schemas.size() == 0)
            return null;

        Result<SchemaDocument> mergeResult = new Result<SchemaDocument>();

        List<SchemaReference> schemaReferences = new LinkedList<SchemaReference>(schemas);

        JsonSchemaV7 result = null;

        while (!schemaReferences.isEmpty()) {
            ListIterator<SchemaReference> iterator = schemaReferences.listIterator();
            SchemaReference schemaReference = iterator.next();
            iterator.remove();

            if (firstIsMerged) {
                result = context.mapSchema(schemaReference);
                firstIsMerged = false;
            } else
                result = mergeV7(result, schemaReference, schemaReferences, mergeResult);
        }

        JsonNode mergedSchemaNode;
        if (result.booleanValidity == null) {
            mergedSchemaNode = SchemaUtils.OBJECT_MAPPER.valueToTree(result);

            if (mergedSchemaNode.isObject())
                ((ObjectNode) mergedSchemaNode).remove("$schema");
        } else {
            mergedSchemaNode = JsonNodeFactory.instance.booleanNode(result.booleanValidity);
        }

        if (mergedSchemaNode.getNodeType() != JsonNodeType.OBJECT || mergedSchemaNode.size() > 0)
            mergeResult.setSchema(new SchemaDocument(null, mergedSchemaNode));
        return mergeResult;
    }

    public Result<SchemaDocument> mergeDocuments(Collection<SchemaDocument> schemaDocuments) throws IOException {
        Result<SchemaDocument> mergeResult = new Result<SchemaDocument>();

        SchemaDocument resultDocument = new SchemaDocument();

        Map<JsonPointer, List<SchemaReference>> schemas = new LinkedHashMap<JsonPointer, List<SchemaReference>>();
        Set<JsonPointer> schemaParents = new HashSet<JsonPointer>();

        for (SchemaDocument schemaDocument : schemaDocuments) {
            if (!schemaDocument.getSchemaParentMap().isEmpty()) {
                schemaParents.addAll(schemaDocument.getSchemaParents());
                for (Map.Entry<JsonPointer, JsonNode> schemaRootEntry : schemaDocument.getSchemaParentMap().entrySet()) {
                    for (Iterator<Map.Entry<String, JsonNode>> it = schemaRootEntry.getValue().fields(); it.hasNext(); ) {
                        Map.Entry<String, JsonNode> schemaEntry = it.next();

                        JsonPointer schemaPointer = schemaRootEntry.getKey().append(JsonPointer.compile(JsonPointerUtils.create(schemaEntry.getKey())));
                        JsonNode schema = schemaEntry.getValue();

                        addSchema(mergeResult, schemas, schemaDocument, schemaPointer, schema);
                    }
                }
            } else {
                JsonPointer schemaPointer = JsonPointer.compile("");

                addSchema(mergeResult, schemas, schemaDocument, schemaPointer, schemaDocument.getRoot());
            }
        }

        for (Map.Entry<JsonPointer, List<SchemaReference>> schemaReferencesEntry : schemas.entrySet()) {
            Result<SchemaDocument> mergedSchema = merge(schemaReferencesEntry.getValue());
            mergeResult.addProblems(mergedSchema.getProblems());
            SchemaMergerUtils.putNode(resultDocument, schemaReferencesEntry.getKey(), mergedSchema.getDocument() != null ? mergedSchema.getDocument().getRoot() : null);
        }

        resultDocument.setSchemaParents(schemaParents);

        mergeResult.setSchema(resultDocument);

        return mergeResult;
    }

    private static void addSchema(Result<SchemaDocument> mergeResult, Map<JsonPointer, List<SchemaReference>> schemas, SchemaDocument document, JsonPointer schemaPointer, JsonNode schema) {
        if (schema != null) {
            List<SchemaReference> schemaReferences = schemas.get(schemaPointer);
            if (schemaReferences == null) {
                schemaReferences = new ArrayList<SchemaReference>();
                schemas.put(schemaPointer, schemaReferences);
            }

            schemaReferences.add(new SchemaReference(document, schemaPointer, schema));
        } else
            mergeResult.addProblem(new Problem(SchemaProblem.INVALID_REF, Problem.Severity.WARNING, "no data at: " + schemaPointer));
    }

    private JsonNode mergeNode(boolean firstIsMerged, Collection<SchemaReference> schemaReferences) throws IOException {
        Result<SchemaDocument> result = merge(firstIsMerged, schemaReferences);
        return result != null && result.getDocument() != null ? result.getDocument().getRoot() : null;
    }

    private JsonNode mergeNode(boolean firstIsMerged, SchemaReference... schemaReferences) throws IOException {
        return mergeNode(firstIsMerged, Arrays.asList(schemaReferences));
    }

    private boolean containsRecursions(SchemaReference schemaReferenceB) throws IOException {
        if (schemaReferenceB.getSchema() instanceof ObjectNode) {
            final Set<SchemaReference> visitedSchemaLocations = new TreeSet<SchemaReference>(new SchemaReferenceComparator());
            final Stack<SchemaReference> pendingNodes = new Stack<SchemaReference>();
            final AtomicReference<SchemaReference> lastReferencedSchema = new AtomicReference<SchemaReference>();

            try {
                pendingNodes.add(schemaReferenceB);

                while (!pendingNodes.isEmpty()) {
                    final SchemaReference nextSchemaReference = pendingNodes.pop();

                    SchemaUtils.visitAllNodes(nextSchemaReference.getSchema(), new SchemaUtils.NodeVisitor() {
                        final LinkedList<JsonNode> nodes = new LinkedList<JsonNode>();

                        @Override
                        public void push(LinkedList<String> path, JsonNode node) throws IOException {
                            nodes.addLast(node);

                            SchemaReference resolvedSchema;

                            String ref = getCanonicalRef(nextSchemaReference, node);
                            if (ref != null) {
                                resolvedSchema = context.resolveSchema(nextSchemaReference.findChild(node), ref);
                                if (resolvedSchema == null)
                                    throw new SchemaException(null, SchemaProblem.INVALID_REF, "failed to resolve $ref: " + ref);

                                if (!visitedSchemaLocations.add(resolvedSchema)) {
                                    throw new SchemaException(resolvedSchema, SchemaProblem.RECURSION, null);
                                } else {
                                    pendingNodes.add(resolvedSchema);
                                    lastReferencedSchema.set(resolvedSchema);
                                }
                            }
                        }

                        @Override
                        public void pop(LinkedList<String> path, JsonNode node) {
                            nodes.removeLast();
                        }
                    });
                }

//                if (lastReferencedSchema.get() != null)
//                    return lastReferencedSchema.get();
            } catch (SchemaException e) {
                if (e.getError() == SchemaProblem.RECURSION)
                    return true;
                else
                    throw e;
            }
        }
        return false;
    }

    private SchemaReference resolveNonRecursive(final SchemaReference location) throws IOException {
        if (location.getSchema() instanceof ObjectNode) {
            final Set<SchemaReference> visitedSchemaLocations = new TreeSet<SchemaReference>(new SchemaReferenceComparator());
            SchemaReference nextSchemaNode;
            SchemaReference lastResolvedSchema;

            nextSchemaNode = location;
            do {
                SchemaReference schemaReference = nextSchemaNode;
                nextSchemaNode = null;

                if (visitedSchemaLocations.add(schemaReference))
                    lastResolvedSchema = schemaReference;
                else {
                    lastResolvedSchema = null;
                    break;
                }

                final String ref = getCanonicalRef(schemaReference, schemaReference.getSchema());
                if (ref != null) {
                    nextSchemaNode = context.resolveSchema(schemaReference, ref);
                    if (nextSchemaNode == null)
                        throw new SchemaException(null, SchemaProblem.INVALID_REF, "failed to resolve $ref: " + ref);
                }
            } while (nextSchemaNode != null);

            if (lastResolvedSchema != null && !containsRecursions(lastResolvedSchema))
                return lastResolvedSchema;
        }
        return location;
    }

    private static String getRef(final JsonNode referringNode) {
        JsonNode refNode = referringNode.get("$ref");
        if (refNode != null && refNode.isTextual()) {
            return refNode.asText();
        }
        return null;
    }

    private static String getCanonicalRef(SchemaReference schemaReference, final JsonNode referringNode) throws IOException {
        String ref = getRef(referringNode);
        if (ref != null && !ref.startsWith("#")) {
            final Stack<JsonNode> nodes = new Stack<JsonNode>();

            SchemaUtils.visitAllNodes(schemaReference.getRoot(), new SchemaUtils.NodeVisitor() {
                final Stack<JsonNode> currentNodes = new Stack<JsonNode>();

                @Override
                public void push(LinkedList<String> path, JsonNode node) throws IOException {
                    currentNodes.push(node);
                    if (node == referringNode) {
                        nodes.clear();
                        nodes.addAll(currentNodes);
                    }
                }

                @Override
                public void pop(LinkedList<String> path, JsonNode node) throws IOException {
                    currentNodes.pop();
                }
            });

            URI rootUri = schemaReference.getDocument().getRootUri();
            URI nodeUri = SchemaResolutionContext.getUri(nodes, false);
            URI refUri = URI.create(ref);

            if (rootUri != null && nodeUri != null)
                rootUri = rootUri.resolve(nodeUri);

            if (rootUri != null)
                refUri = rootUri.resolve(refUri);

            ref = refUri.toASCIIString();
        }
        return ref;
    }

    private JsonSchemaV7 mergeV7(JsonSchemaV7 schemaA, SchemaReference schemaReferenceB, Collection<SchemaReference> schemasOut, Result<?> result) throws IOException {
        boolean schemaPushed = false;
        boolean logging = false;

        try {
            if (logging) {
                System.err.println(SchemaUtils.OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(schemaA));

                System.err.println("<<<");
                System.err.println(schemaReferenceB.getSchema().toString());
            }

            schemaReferenceB = resolveNonRecursive(schemaReferenceB);

            if (logging) {
                System.err.println("<<<===");
                System.err.println(schemaReferenceB.getSchema().toString());
                System.err.println("----");
                System.err.println();
            }

            if (schemaA == null)
                schemaA = new JsonSchemaV7();

            JsonSchemaV7 schemaB = context.pushSchemaReference(schemaReferenceB);
            schemaPushed = true;

            if (schemaB.ref != null) {
                //schemaB.ref = getCanonicalRef(schemaReferenceB, schemaReferenceB.getSchema());

                int nRefs = 0;
                boolean isNewRef = true;

                if (schemaA.ref != null) {
                    ++nRefs;
                    if (schemaA.ref.equals(schemaB.ref))
                        isNewRef = false;
                }

                if (isNewRef)
                    if (schemaA.allOf != null) {
                        for (JsonNode jsonNode : schemaA.allOf) {
                            if (jsonNode.isObject()) {
                                JsonNode refNode = jsonNode.get("$ref");
                                if (refNode != null) {
                                    ++nRefs;
                                    if (refNode.asText().equals(schemaB.ref))
                                        isNewRef = false;
                                }
                            }
                        }
                    }

                if (isNewRef)
                    if (nRefs == 0
                            && SchemaUtils.OBJECT_MAPPER.valueToTree(schemaA).size() == 0
                    ) {
                        schemaA.ref = schemaB.ref;
                    } else {
                        if (schemaA.allOf == null)
                            schemaA.allOf = JsonNodeFactory.instance.arrayNode();
                        if (schemaA.ref != null) {
                            schemaA.allOf.add(JsonNodeFactory.instance.objectNode()
                                    .put("$ref", schemaA.ref));
                            schemaA.ref = null;
                        }
                        schemaA.allOf.add(JsonNodeFactory.instance.objectNode()
                                .put("$ref", schemaB.ref));
                    }

                schemaB.ref = null;

                return schemaA;
            }

            Set<String> required;
            if (schemaB.required != null && schemaB.required.size() > 0) {
                required = new LinkedHashSet<String>(schemaB.required.size() + (schemaA.required != null ? schemaA.required.size() : 0));

                if (schemaA.required != null)
                    for (JsonNode jsonNode : schemaA.required)
                        required.add(jsonNode.textValue());
                for (JsonNode jsonNode : schemaB.required)
                    required.add(jsonNode.textValue());

                if (schemaA.required == null)
                    schemaA.required = JsonNodeFactory.instance.arrayNode(required.size());
                else
                    schemaA.required.removeAll();

                for (String requiredProperty : required)
                    schemaA.required.add(requiredProperty);

                schemaB.required = null;
            } else {
                if (schemaA.required != null) {
                    required = new HashSet<String>(schemaA.required.size());
                    for (JsonNode requiredProperty : schemaA.required)
                        required.add(requiredProperty.asText());
                } else
                    required = Collections.emptySet();
            }

            JsonNode additionalProperties = schemaA.additionalProperties;

            if (schemaB.additionalProperties != null) {
                if (schemaB.additionalProperties.isBoolean()) {
                    if (!schemaB.additionalProperties.asBoolean())
                        additionalProperties = schemaB.additionalProperties;
                } else {
                    if (schemaA.additionalProperties != null)
                        additionalProperties = mergeNode(true, new SchemaReference(new SchemaDocument(null, schemaA.additionalProperties)), schemaReferenceB.createRelative("/additionalProperties", schemaB.additionalProperties));
                    else
                        additionalProperties = mergeNode(false, schemaReferenceB.createRelative("/additionalProperties", schemaB.additionalProperties));
                }
            }

            if (schemaB.properties != null) {
                Set<String> allKeys = new LinkedHashSet<String>();
                ObjectNode mergedProperties = JsonNodeFactory.instance.objectNode();

                //collect all keys while retaining the order
                if (schemaA.properties != null) {
                    if (!schemaA.properties.isObject())
                        throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "/properties is not an object");
                    for (Iterator<String> it = schemaA.properties.fieldNames(); it.hasNext(); )
                        allKeys.add(it.next());
                }
                for (Iterator<String> it = schemaB.properties.fieldNames(); it.hasNext(); )
                    allKeys.add(it.next());
                allKeys.addAll(required);

                //gather all properties from both schemas and merge them
                for (String key : allKeys) {
                    JsonNode valueA = schemaA.properties != null ? schemaA.properties.get(key) : null;
                    JsonNode valueB = schemaB.properties.get(key);

                    List<SchemaReference> propertySchemas = new ArrayList<SchemaReference>();
                    if (valueA != null || valueB != null) {
                        if (valueA != null)
                            propertySchemas.add(SchemaReference.create(schemaA, JsonPointerUtils.create("properties", key), valueA));
                        if (valueB != null)
                            propertySchemas.add(schemaReferenceB.createRelative(JsonPointerUtils.create("properties", key), valueB));
                        if (valueB == null && schemaB.additionalProperties != null)
                            propertySchemas.add(schemaReferenceB.createRelative("/additionalProperties", schemaB.additionalProperties));
                        if (valueA == null && schemaA.additionalProperties != null)
                            propertySchemas.add(SchemaReference.create(schemaA, "/additionalProperties", schemaA.additionalProperties));

                        JsonNode mergedPropertySchema = mergeNode(valueA != null, propertySchemas);

                        if (mergedPropertySchema != null) {
                            boolean redundantToAdditionalProperties = options.contains(Option.REMOVE_PROPERTIES_THAT_MATCH_ADDITIONAL_PROPERTIES) && SchemaMergerUtils.eq(mergedPropertySchema, additionalProperties);
                            if (!redundantToAdditionalProperties)
                                mergedProperties.set(key, mergedPropertySchema);

                            if (mergedPropertySchema.isBoolean() && !mergedPropertySchema.asBoolean() && required.contains(key))
                                result.addProblem(new Problem(SchemaProblem.VALIDATION_ALWAYS_FAILS, Problem.Severity.WARNING, "required property will never pass validation: " + key));
                        }
                    }
                }

                schemaA.properties = mergedProperties;
                schemaB.properties = null;
            }

            schemaA.additionalProperties = additionalProperties;
            schemaB.additionalProperties = null;


            if (schemaB.pattern != null) {
                boolean newPattern = true;
                int nPatterns = 0;

                if (schemaA.pattern != null) {
                    ++nPatterns;
                    newPattern = !Value.equals(schemaA.pattern, schemaB.pattern);
                }

                if (newPattern) {
                    if (schemaA.allOf != null)
                        for (JsonNode schemaNode : schemaA.allOf) {
                            if (schemaNode.has("pattern")) {
                                ++nPatterns;
                                if (schemaNode.get("pattern").asText().equals(schemaB.pattern)) {
                                    newPattern = false;
                                    break;
                                }
                            }
                        }
                }

                if (newPattern) {
                    if (nPatterns > 0) {
                        if (schemaA.allOf == null)
                            schemaA.allOf = JsonNodeFactory.instance.arrayNode(1);

                        if (schemaA.pattern != null) {
                            schemaA.allOf.add(JsonNodeFactory.instance.objectNode()
                                    .put("pattern", schemaA.pattern));
                            schemaA.pattern = null;
                        }

                        schemaA.allOf.add(JsonNodeFactory.instance.objectNode()
                                .put("pattern", schemaB.pattern));
                    } else
                        schemaA.pattern = schemaB.pattern;
                }

                schemaB.pattern = null;
            }

            schemaA.minLength = Value.max(schemaA.minLength, schemaB.minLength);
            schemaA.maxLength = Value.min(schemaA.maxLength, schemaB.maxLength);
            schemaB.minLength = null;
            schemaB.maxLength = null;

            if (schemaB.patternProperties != null) {
                for (Map.Entry<String, JsonNode> nodeEntry : schemaB.patternProperties.entrySet()) {
                    if (schemaA.patternProperties == null)
                        schemaA.patternProperties = new TreeMap<String, JsonNode>();

                    Map<String, JsonNode> mergedProperties = schemaA.patternProperties;

                    JsonNode mergedPropertyNode = nodeEntry.getValue();

                    JsonNode existing = mergedProperties.get(nodeEntry.getKey());
                    if (existing != null) {
                        mergedProperties.put(nodeEntry.getKey(), mergeNode(true, new SchemaReference(new SchemaDocument(null, existing)), schemaReferenceB.createRelative(JsonPointerUtils.create("patternProperties", nodeEntry.getKey()), mergedPropertyNode)));
                    } else
                        mergedProperties.put(nodeEntry.getKey(), mergeNode(false, schemaReferenceB.createRelative(JsonPointerUtils.create("patternProperties", nodeEntry.getKey()), mergedPropertyNode)));
                }
                schemaB.patternProperties = null;
            }

            if (schemaB.booleanValidity != null) {
                if (schemaA.booleanValidity != null) {
                    if (!schemaA.booleanValidity.equals(schemaB.booleanValidity)) {
                        result.addProblem(new Problem(SchemaProblem.VALIDATION_ALWAYS_FAILS, Problem.Severity.WARNING));
                        schemaA = new JsonSchemaV7();
                        schemaA.booleanValidity = false;//mismatch has precedence over match
                        return schemaA;
                    }
                } else {
                    schemaA.booleanValidity = schemaB.booleanValidity;
                }
                schemaB.booleanValidity = null;
            }

            if (schemaB.nonSchemaProperties != null) {
                if (schemaA.nonSchemaProperties != null)
                    schemaA.nonSchemaProperties.setAll(schemaB.nonSchemaProperties);
                else
                    schemaA.nonSchemaProperties = schemaB.nonSchemaProperties;
                schemaB.nonSchemaProperties = null;
            }

            schemaA.type = SchemaMergerUtils.arrayIntersect(schemaA.type, schemaB.type);
            schemaB.type = null;


            schemaA.minProperties = Value.max(schemaA.minProperties, schemaB.minProperties);
            schemaA.maxProperties = Value.min(schemaA.maxProperties, schemaB.maxProperties);
            schemaB.minProperties = null;
            schemaB.maxProperties = null;

            if (schemaB.propertyNames != null) {
                if (schemaA.propertyNames != null)
                    schemaA.propertyNames = mergeNode(true, new SchemaReference(new SchemaDocument(null, schemaA.propertyNames)), schemaReferenceB.createRelative("/propertyNames", schemaB.propertyNames));
                else
                    schemaA.propertyNames = mergeNode(false, schemaReferenceB.createRelative("/propertyNames", schemaB.propertyNames));
                schemaB.propertyNames = null;
            }

            if (schemaB.items != null) {
                if (schemaB.additionalItems != null && schemaB.additionalItems.size() == 0)
                    schemaB.additionalItems = null;

                // In case the items rule type changes to an array, merge the former schema into the items schema array and the additionalItems schema.
                // Otherwise, just merge the schemas.
                if (schemaB.items.isArray() || (schemaA.items != null && schemaA.items.isArray())) {
                    int totalSchemaCount = 0;
                    if (schemaA.items != null && schemaA.items.isArray())
                        totalSchemaCount = Math.max(totalSchemaCount, schemaA.items.size());
                    if (schemaB.items.isArray())
                        totalSchemaCount = Math.max(totalSchemaCount, schemaB.items.size());

                    ArrayNode newItems = JsonNodeFactory.instance.arrayNode(totalSchemaCount);

                    List<SchemaReference> schemasAtIndex = new ArrayList<SchemaReference>(2);
                    for (int index = 0; index < totalSchemaCount; ++index) {
                        JsonNode existingItem;
                        JsonNode item;

                        if (schemaA.items != null) {
                            if (schemaA.items.isArray())
                                if (index < schemaA.items.size())
                                    existingItem = schemaA.items.get(index);
                                else
                                    existingItem = schemaA.additionalItems;
                            else
                                existingItem = schemaA.items;
                        } else
                            existingItem = null;

                        JsonPointer itemSchemaPointer;
                        if (schemaB.items.isArray())
                            if (index < schemaB.items.size()) {
                                item = schemaB.items.get(index);
                                itemSchemaPointer = schemaReferenceB.getSchemaPointer().append(JsonPointer.compile("/items/" + index));
                            } else {
                                item = schemaB.additionalItems;
                                itemSchemaPointer = schemaReferenceB.getSchemaPointer().append(JsonPointer.compile("/additionalItems"));
                            }
                        else {
                            item = schemaB.items;
                            itemSchemaPointer = schemaReferenceB.getSchemaPointer().append(JsonPointer.compile("/items"));
                        }

                        schemasAtIndex.clear();
                        if (existingItem != null)
                            schemasAtIndex.add(new SchemaReference(new SchemaDocument(null, existingItem)));
                        if (item != null)
                            schemasAtIndex.add(new SchemaReference(schemaReferenceB.getDocument(), itemSchemaPointer, item));

                        JsonNode mergedSchemaAtIndex = mergeNode(existingItem != null, schemasAtIndex);
                        newItems.add(mergedSchemaAtIndex);
                    }

                    List<SchemaReference> additionalItemsSchemas = new ArrayList<SchemaReference>(2);

                    if (schemaA.additionalItems != null)
                        additionalItemsSchemas.add(new SchemaReference(new SchemaDocument(null, schemaA.additionalItems)));
                    if (schemaB.additionalItems != null)
                        additionalItemsSchemas.add(schemaReferenceB.createRelative("/additionalItems", schemaB.additionalItems));

                    schemaA.additionalItems = mergeNode(schemaA.additionalItems != null, additionalItemsSchemas);
                    schemaA.items = newItems;
                } else {
                    //single schema case
                    List<SchemaReference> schemas = new ArrayList<SchemaReference>(2);

                    if (schemaA.items != null)
                        schemas.add(new SchemaReference(new SchemaDocument(null, schemaA.items)));
                    schemas.add(schemaReferenceB.createRelative("/items", schemaB.items));

                    schemaA.additionalItems = null;
                    schemaA.items = mergeNode(schemaA.items != null, schemas);
                }
            }
            schemaB.items = null;
            schemaB.additionalItems = null;


            schemaA.minItems = Value.max(schemaA.minItems, schemaB.minItems);
            schemaA.maxItems = Value.min(schemaA.maxItems, schemaB.maxItems);
            schemaA.minItems = null;
            schemaA.maxItems = null;


            schemaA.minimum = Value.max(schemaA.minimum, schemaB.minimum);
            schemaA.exclusiveMinimum = Value.max(schemaA.exclusiveMinimum, schemaB.exclusiveMinimum);
            schemaA.maximum = Value.min(schemaA.maximum, schemaB.maximum);
            schemaA.exclusiveMaximum = Value.min(schemaA.exclusiveMaximum, schemaB.exclusiveMaximum);
            schemaA.multipleOf = Value.lcm(schemaA.multipleOf, schemaB.multipleOf);
            schemaB.minimum = null;
            schemaB.exclusiveMinimum = null;
            schemaB.maximum = null;
            schemaB.exclusiveMaximum = null;
            schemaB.multipleOf = null;

            if (schemaB._enum != null) {
                schemaA._enum = SchemaMergerUtils.arrayIntersectUnique(schemaA._enum, schemaB._enum);
                schemaB._enum = null;
            }

            if (schemaB._const != null) {
                if (schemaA._const != null && !SchemaMergerUtils.eq(schemaA._const, schemaB._const)) {
                    result.addProblem(new Problem(SchemaProblem.VALIDATION_ALWAYS_FAILS, Problem.Severity.WARNING));
                    schemaA = new JsonSchemaV7();
                    schemaA.booleanValidity = false;//mismatch has precedence over match
                    return schemaA;
                }
                schemaA._const = schemaB._const;
                schemaB._const = null;
            }

            if (schemaA._enum != null && schemaA._const != null) {
                boolean constInEnum = false;
                for (JsonNode enumValue : schemaA._enum) {
                    if (schemaA._const.equals(enumValue)) {
                        constInEnum = true;
                        break;
                    }
                }

                if (constInEnum) {
                    if (options.contains(Option.MERGE_CONST_INTO_ENUM)) {
                        schemaA._enum.removeAll();
                        schemaA._enum.add(schemaA._const);
                        schemaA._const = null;
                    } else {
                        schemaA._enum = null;
                    }
                } else
                    result.addProblem(new Problem(SchemaProblem.VALIDATION_ALWAYS_FAILS, Problem.Severity.WARNING, null));
            }

            if (schemaB.allOf != null) {
                int i = 0;
                for (JsonNode subSchema : schemaB.allOf) {
                    schemasOut.add(schemaReferenceB.createRelative("/allOf/" + i, subSchema));
                    ++i;
                }

                schemaB.allOf = null;
            }

            if (schemaB.anyOf != null) {
                if (options.contains(Option.MERGE_SINGLE_ANY_OF_TO_ALL_OF) && schemaB.anyOf.size() == 1) {
                    schemasOut.add(schemaReferenceB.createRelative("/anyOf/0", schemaB.anyOf.get(0)));
                    schemaB.anyOf = null;
                } else {
                    ArrayNode newAnyOf = null;

                    int i = 0;
                    for (JsonNode anyOfSchema : schemaB.anyOf) {
                        anyOfSchema = mergeNode(false, schemaReferenceB.createRelative("/anyOf/" + i, anyOfSchema));
                        newAnyOf = SchemaMergerUtils.arrayAddUnique(newAnyOf, anyOfSchema);
                        ++i;
                    }

                    schemaB.anyOf = newAnyOf;//defer to generic aggregation in allOf
                }
            }

            if (schemaB.oneOf != null) {
                if (options.contains(Option.MERGE_SINGLE_ONE_OF_TO_ALL_OF) && schemaB.oneOf.size() == 1) {
                    schemasOut.add(schemaReferenceB.createRelative("/oneOf/0", schemaB.oneOf.get(0)));
                    schemaB.oneOf = null;
                } else {
                    ArrayNode newOneOf = null;

                    int i = 0;
                    for (JsonNode oneOfSchema : schemaB.oneOf) {
                        oneOfSchema = mergeNode(false, schemaReferenceB.createRelative("/oneOf/" + i, oneOfSchema));
                        newOneOf = SchemaMergerUtils.arrayAddUnique(newOneOf, oneOfSchema);
                        ++i;
                    }

                    schemaB.oneOf = newOneOf;//defer to generic aggregation in allOf
                }
            }

            if (schemaB.definitions != null) {
                if (schemaA.definitions != null && !schemaA.definitions.equals(schemaB.definitions))
                    throw new SchemaException(null, SchemaProblem.NON_MERGEABLE_NON_SCHEMA_PROPERTIES, null);
                else
                    schemaA.definitions = schemaB.definitions;
                schemaB.definitions = null;
            }

            if (schemaB.description != null) {
                //TODO concatenate description?
                //if (schemaA.description != null && !schemaA.description.equals(schemaB.description))
                //   throw new SchemaException(null, SchemaProblem.NON_MERGEABLE_NON_SCHEMA_PROPERTIES, null);
                if (schemaA.description == null)
                    schemaA.description = schemaB.description;
                schemaB.description = null;
            }

            if (schemaB.id != null) {
                //TODO handle cases where the absorbed id must remain resolvable
                //if (schemaA.id != null && !schemaA.id.equals(schemaB.id))
                //    throw new SchemaException(null, SchemaProblem.NON_MERGEABLE_NON_SCHEMA_PROPERTIES, null);
                if (schemaA.id == null)
                    schemaA.id = schemaB.id;
                schemaB.id = null;
            }

            //add schemas with unmerged properties to allOf
            {
                schemaB.description = null;
                schemaB.schema = null;
                schemaB.nonSchemaProperties = null;

                JsonNode remainderSchemaNode = SchemaUtils.OBJECT_MAPPER.valueToTree(schemaB);

                if (!remainderSchemaNode.isObject() || remainderSchemaNode.size() > 0)
                    schemaA.allOf = SchemaMergerUtils.arrayAddUnique(schemaA.allOf, remainderSchemaNode);
            }

            return schemaA;
        } finally {
            if (schemaPushed)
                context.popSchemaReference(schemaReferenceB);
        }

    }


    private static class SchemaReferenceComparator implements Comparator<SchemaReference> {
        @Override
        public int compare(SchemaReference a, SchemaReference b) {
            if (a == b)
                return 0;

            if (a.getRoot() == b.getRoot()) {
                return a.getSchemaPointer().toString().compareTo(b.getSchemaPointer().toString());
            }

            return ObjectUtils.hashCode(a.getRoot()) - ObjectUtils.hashCode(b.getRoot());
        }
    }
}
