package io.zerocopy.json.schema.merger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Result<Type> {
    private Type schema;
    private List<Problem> problems;


    public Type getDocument() {
        return schema;
    }

    public List<Problem> getProblems() {
        return problems != null ? Collections.unmodifiableList(problems) : Collections.<Problem>emptyList();
    }

    void setSchema(Type schema) {
        this.schema = schema;
    }

    void addProblem(Problem problem) {
        if (problem == null)
            throw new NullPointerException();

        if (this.problems == null)
            this.problems = new ArrayList<Problem>();
        this.problems.add(problem);
    }

    void addProblems(List<Problem> problems) {
        for (Problem problem : problems)
            addProblem(problem);
    }
}
