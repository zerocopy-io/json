package io.zerocopy.json.schema;

import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StaticInitTestCase {
    @Test
    public void accessAllRules() {
        Set<String> uniqueRuleNames = new HashSet<String>();

        for (JsonSchemaV7.Rule rule : JsonSchemaV7.Rule.values()) {
            assertTrue(uniqueRuleNames.add(rule.value));
            assertEquals(rule.name().toLowerCase(), rule.value);
        }
    }

    @Test
    public void accessAllFormats() {
        for (JsonSchemaV7.Format format : JsonSchemaV7.Format.values()) {
            if (format.matcher != null)
                format.matcher.matches("");
            if (format.dateTimeConverter != null)
                format.dateTimeConverter.format(new Date());
        }
    }
}
