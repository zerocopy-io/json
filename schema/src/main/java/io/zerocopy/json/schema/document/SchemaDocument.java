package io.zerocopy.json.schema.document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;

import java.net.URI;
import java.util.*;

/**
 * Describes a document with one or more named schemas
 */
public class SchemaDocument {
    private URI rootUri;
    private JsonNode root;
    /**
     * A set of pointers relative to {@link #root}, identifying document nodes that contain schemas (arrays or objects)
     */
    private Set<JsonPointer> schemaParents = new LinkedHashSet<JsonPointer>();

    public SchemaDocument(URI rootUri, JsonNode root) {
        if (root == null)
            throw new IllegalArgumentException();
        this.rootUri = rootUri;
        this.root = root;
    }

    public SchemaDocument() {
        this.rootUri = null;
        this.root = null;
    }

    public URI getRootUri() {
        return rootUri;
    }

    public void setRootUri(URI rootUri) {
        this.rootUri = rootUri;
    }

    public JsonNode getRoot() {
        return root;
    }

    public void setRoot(JsonNode root) {
        this.root = root;
    }

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    public Set<JsonPointer> getSchemaParents() {
        return schemaParents;
    }

    public void setSchemaParents(Set<JsonPointer> schemaParents) {
        this.schemaParents = schemaParents;
    }

    @JsonIgnore
    public Map<JsonPointer, JsonNode> getSchemaParentMap() {
        final Set<JsonPointer> schemaParents = !this.schemaParents.isEmpty() ? this.schemaParents : Collections.<JsonPointer>emptySet();

        return new AbstractMap<JsonPointer, JsonNode>() {
            @Override
            public Set<Entry<JsonPointer, JsonNode>> entrySet() {
                return new AbstractSet<Entry<JsonPointer, JsonNode>>() {
                    @Override
                    public Iterator<Entry<JsonPointer, JsonNode>> iterator() {
                        return new Iterator<Entry<JsonPointer, JsonNode>>() {
                            Iterator<JsonPointer> iterator = schemaParents.iterator();

                            @Override
                            public boolean hasNext() {
                                return iterator.hasNext();
                            }

                            @Override
                            public Entry<JsonPointer, JsonNode> next() {
                                final JsonPointer jsonPointer = iterator.next();
                                final JsonNode jsonNode = root.at(jsonPointer);

                                return new Entry<JsonPointer, JsonNode>() {
                                    @Override
                                    public JsonPointer getKey() {
                                        return jsonPointer;
                                    }

                                    @Override
                                    public JsonNode getValue() {
                                        return jsonNode;
                                    }

                                    @Override
                                    public JsonNode setValue(JsonNode jsonNode) {
                                        throw new UnsupportedOperationException();
                                    }
                                };
                            }

                            @Override
                            public void remove() {
                                throw new UnsupportedOperationException();
                            }
                        };
                    }

                    @Override
                    public int size() {
                        return SchemaDocument.this.schemaParents.size();
                    }
                };
            }
        };
    }
}