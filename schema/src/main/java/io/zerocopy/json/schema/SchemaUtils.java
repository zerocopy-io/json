package io.zerocopy.json.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class SchemaUtils {
    public static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper()
                .setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)
                .configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true)
                .configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
        ;
    }

    /**
     * Resolves a node according to a ref.
     * In case the ref starts with '#', the search is started at the root node.
     * An all other cases, the search starts at the referring node.
     *
     * @param rootNode      the root node of the referring node
     * @param referringNode the node which contains the reference
     * @param ref           an URL-encoded reference
     * @return the resolved JSON node or null, if not found
     */
    public static JsonNode resolveRelativeRef(JsonNode rootNode, JsonNode referringNode, String ref) {
        try {
            ref = URLDecoder.decode(ref, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        if (ref.length() > 0 && ref.charAt(0) == '#')
            return JsonPointerUtils.resolvePointer(rootNode, ref.substring(1));
        else
            return JsonPointerUtils.resolvePointer(referringNode, ref);
    }

    /**
     * Resolves a node according to a ref.
     * In case the ref starts with '#', the search is started at the root node.
     * An all other cases, the search starts at the referring node.
     *
     * @param document      the document that contains the referring node
     * @param referringNode the node which contains the reference
     * @param ref           an URL-encoded reference
     * @return the resolved JSON node or null, if not found
     * @throws java.io.IOException on resolution errors
     */
    public static SchemaReference resolveRelativeRefPath(SchemaDocument document, JsonNode referringNode, String ref) throws IOException {
//        ref = URLDecoder.decode(ref, "UTF-8");

        JsonNode resolvedNode;
        JsonPointer schemaPointer;

        if (ref.length() > 0 && ref.charAt(0) == '#') {
            schemaPointer = null;
            resolvedNode = null;

            if (resolvedNode == null) {
                final List<JsonNode> anchoredNodes = new ArrayList<JsonNode>();
                final String refAnchor = ref.substring(1);
                final LinkedList<String> nodePath = new LinkedList<String>();
                final LinkedList<String> path = new LinkedList<String>();
                SchemaUtils.visitAllNodes(path, document.getRoot(), new NodeVisitor() {
                    @Override
                    public void push(LinkedList<String> path, JsonNode node) {
                        if (anchoredNodes.isEmpty()) {
                            JsonNode anchor = node.get("$anchor");
                            if (anchor != null) {
                                if (anchor.textValue().equals(refAnchor)) {
                                    anchoredNodes.add(node);
                                    nodePath.addAll(path);
                                }
                            }
                        }
                    }

                    @Override
                    public void pop(LinkedList<String> path, JsonNode node) {
                    }
                });

                if (!anchoredNodes.isEmpty()) {
                    schemaPointer = JsonPointer.compile(JsonPointerUtils.implode(nodePath));
                    resolvedNode = anchoredNodes.get(0);
                }
            }

            if (resolvedNode == null) {
                LinkedList<String> path = new LinkedList<String>();

                schemaPointer = JsonPointer.compile(ref.substring(1));
                resolvedNode = JsonPointerUtils.resolvePointerPath(path, document.getRoot(), ref.substring(1));
            }

            if (resolvedNode == null) {
                ObjectNode definitions = (ObjectNode) document.getRoot().get("definitions");
                if (definitions != null) {
                    for (Iterator<String> fieldNames = definitions.fieldNames(); fieldNames.hasNext(); ) {
                        String name = fieldNames.next();
                        JsonNode definition = definitions.get(name);

                        JsonNode idNode = definition.get("$id");
                        if (idNode == null)
                            idNode = definition.get("id");
                        if (idNode != null && idNode.asText().equals(ref)) {
                            resolvedNode = definition;
                            schemaPointer = JsonPointer.compile("/definitions/" + JsonPointerUtils.escapePointerSegment(name));
                            break;
                        }
                    }
                }
            }
        } else {
            final JsonPointer pointerToReferring = findNode(document.getRoot(), referringNode);
            if (pointerToReferring == null)
                throw new IllegalArgumentException("referring node not a child of root node");
            JsonPointer referringPointer = JsonPointer.compile(ref);
            schemaPointer = pointerToReferring.append(referringPointer);
            resolvedNode = referringNode.at(referringPointer);
        }

        if (resolvedNode != null) {
            return new SchemaReference(document, schemaPointer, resolvedNode);
        } else
            return null;
    }

    public interface NodeVisitor {
        void push(LinkedList<String> path, JsonNode node) throws IOException;

        void pop(LinkedList<String> path, JsonNode node) throws IOException;
    }

    public static void visitAllNodes(JsonNode node, NodeVisitor visitor) throws IOException {
        LinkedList<String> path = new LinkedList<String>();
        visitAllNodes(path, node, visitor);
    }

    /**
     * @param root the node to traverse
     * @param node the node to find in root
     * @return the pointer to node in root when found
     * WARNING: Really slow, scans the DOM with (O(N))
     */
    public static JsonPointer findNode(JsonNode root, final JsonNode node) {
        final AtomicReference<JsonPointer> expectedPointer = new AtomicReference<JsonPointer>();
        try {
            SchemaUtils.visitAllNodes(root, new SchemaUtils.NodeVisitor() {
                final JsonNode nodeToFind = node;

                @Override
                public void push(LinkedList<String> path, JsonNode node) throws IOException {
                    if (node == nodeToFind)
                        expectedPointer.set(JsonPointer.valueOf(JsonPointerUtils.implode(path)));
                }

                @Override
                public void pop(LinkedList<String> path, JsonNode node) throws IOException {
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return expectedPointer.get();
    }

    /**
     * @param path    the base path
     * @param node    the node to start at (corresponding to the specified path)
     * @param visitor the visitor
     */
    private static void visitAllNodes(LinkedList<String> path, JsonNode node, NodeVisitor visitor) throws IOException {
        visitor.push(path, node);
        if (node.isObject()) {
            for (Iterator<Map.Entry<String, JsonNode>> fieldIterator = node.fields(); fieldIterator.hasNext(); ) {
                Map.Entry<String, JsonNode> field = fieldIterator.next();
                JsonNode childNode = field.getValue();

                if (node.isObject() || node.isArray()) {
                    path.addLast(field.getKey());
                    visitAllNodes(path, childNode, visitor);
                    path.removeLast();
                }
            }
        } else if (node.isArray()) {
            int index = 0;
            for (Iterator<JsonNode> fieldIterator = node.elements(); fieldIterator.hasNext(); ) {
                JsonNode childNode = fieldIterator.next();

                if (node.isObject() || node.isArray()) {
                    path.addLast(Integer.toString(index));
                    visitAllNodes(path, childNode, visitor);
                    path.removeLast();
                }
                ++index;
            }
        }
        visitor.pop(path, node);
    }

    public static void findAllRefsInSchema(final JsonNode rootNode, final JsonNode schemaNode, final Set<String> refsOut) throws IOException {
        SchemaUtils.visitAllNodes(schemaNode, new SchemaUtils.NodeVisitor() {
            @Override
            public void push(LinkedList<String> path, JsonNode node) throws IOException {
                JsonNode ref = node.get("$ref");
                if (ref != null) {
                    boolean newRef = refsOut.add(ref.asText());
                    if (newRef) {
                        JsonNode referencedNode = SchemaUtils.resolveRelativeRef(rootNode, schemaNode, ref.asText());
                        if (referencedNode == null)
                            throw new SchemaException(null, SchemaProblem.INVALID_REF, ref.asText());
                        findAllRefsInSchema(rootNode, referencedNode, refsOut);
                    }
                }
            }

            @Override
            public void pop(LinkedList<String> path, JsonNode node) {
            }
        });
    }
}
