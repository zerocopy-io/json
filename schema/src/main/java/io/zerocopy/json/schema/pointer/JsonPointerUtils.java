package io.zerocopy.json.schema.pointer;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonPointerUtils {
    /**
     * @param node        the node from which to resolve the specified pointer
     * @param jsonPointer a JSON pointer according to https://tools.ietf.org/html/rfc6901
     * @return the resolved node, or <code>null</code>, if not found.
     */
    public static JsonNode resolvePointer(JsonNode node, String jsonPointer) {
        JsonNode current = node;
        if (!jsonPointer.isEmpty()) {
            int start = 0;
            int end;
            do {
                end = jsonPointer.indexOf('/', start);
                if (end == start) {
                    start = ++end;
                    continue;
                } else if (end == -1)
                    end = jsonPointer.length();
                String refSegment = unescapePointerSegment(jsonPointer.substring(start, end));
                start = end + 1;

                if (current == null)
                    return null;
                current = resolvePointerSegment(current, refSegment);
            } while (start < jsonPointer.length());
        }
        //TODO builtin resolution may be used instead, but introduces a rather specific dependency to the DOM implementation
//        assert current == node.at(jsonPointer);
        return current;
    }

    /**
     * @param pathOut     path output list
     * @param node        the node from which to resolve the specified pointer
     * @param jsonPointer a JSON pointer according to https://tools.ietf.org/html/rfc6901
     * @return the resolved node, or <code>null</code>, if not found.
     */
    public static JsonNode resolvePointerPath(List<String> pathOut, JsonNode node, String jsonPointer) {
        JsonNode current = node;
        pathOut.clear();
        if (!jsonPointer.isEmpty()) {
            int start = 0;
            int end;
            do {
                end = jsonPointer.indexOf('/', start);
                if (end == start) {
                    start = ++end;
                    continue;
                } else if (end == -1)
                    end = jsonPointer.length();
                String refSegment = unescapePointerSegment(jsonPointer.substring(start, end));
                start = end + 1;

                if (current == null)
                    return null;
                current = resolvePointerSegment(current, refSegment);
                pathOut.add(refSegment);
            } while (start < jsonPointer.length());
        }
        return current;
    }

    /**
     * @param pathOut     resulting decoded path elements
     * @param jsonPointer a JSON pointer according to https://tools.ietf.org/html/rfc6901
     */
    public static void split(List<String> pathOut, String jsonPointer) {
        pathOut.clear();
        if (!jsonPointer.isEmpty()) {
            int start = 0;
            int end;
            do {
                end = jsonPointer.indexOf('/', start);
                if (end == start) {
                    start = ++end;
                    continue;
                } else if (end == -1)
                    end = jsonPointer.length();
                String refSegment = unescapePointerSegment(jsonPointer.substring(start, end));
                start = end + 1;

                pathOut.add(refSegment);
            } while (start < jsonPointer.length());
        }
    }

    /**
     * @param jsonPointer the pointer to split into individual elements
     * @return a JSON path with decoded elements
     */
    public static List<String> split(String jsonPointer) {
        List<String> pathElems = new ArrayList<String>();
        split(pathElems, jsonPointer);
        return pathElems;
    }

    /**
     * @param pointerOut resulting JSON pointer
     * @param path       a JSON path with decoded elements
     * @throws java.io.IOException on output errors
     */
    public static void implode(Appendable pointerOut, List<String> path) throws IOException {
        for (String pathElem : path) {
            pointerOut.append('/');
            pathElem = escapePointerSegment(pathElem);
            pointerOut.append(pathElem);
        }
    }

    /**
     * @param pointerOut resulting JSON pointer
     * @param path       a JSON path with decoded elements
     */
    public static void implode(StringBuilder pointerOut, List<String> path) {
        try {
            implode((Appendable) pointerOut, path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param path a JSON path with decoded elements
     * @return JSON pointer
     */
    public static String implode(List<String> path) {
        StringBuilder pointer = new StringBuilder();
        implode(pointer, path);
        return pointer.toString();
    }

    public static String create(String... segments) {
        StringBuilder pointer = new StringBuilder();
        for (String segment : segments) {
            pointer.append("/");
            pointer.append(escapePointerSegment(segment));
        }
        return pointer.toString();
    }

    private static JsonNode resolvePointerSegment(JsonNode node, String pointerSegment) {
        if (node.isArray())
            try {
                int index = Integer.parseInt(pointerSegment, 10);
                return node.get(index);
            } catch (NumberFormatException ignored) {
                return null;
            }
        return node.get(pointerSegment);
    }

    public static String unescapePointerSegment(String pointerSegment) {
        pointerSegment = replaceAll(pointerSegment, "~1", "/");
        pointerSegment = replaceAll(pointerSegment, "~0", "~");
        return pointerSegment;
    }

    public static String escapePointerSegment(String pointerSegment) {
        pointerSegment = replaceAll(pointerSegment, "~", "~0");
        pointerSegment = replaceAll(pointerSegment, "/", "~1");
        return pointerSegment;
    }

    /**
     * @return a new string with replaced substrings or the specified string, if the substring was not found
     */
    private static String replaceAll(String string, String substring, CharSequence replacement) {
        StringBuilder newString = null;

        int start;
        int end = 0;
        while (true) {
            start = string.indexOf(substring, end);
            if (start >= 0) {
                if (newString == null)
                    newString = new StringBuilder();
                newString.append(string, end, start);
                newString.append(replacement);
                end = start + substring.length();
            } else
                break;
        }
        if (newString != null && end < string.length())
            newString.append(string, end, string.length());

        return newString != null ? newString.toString() : string;
    }

    private JsonPointerUtils() {
    }
}
