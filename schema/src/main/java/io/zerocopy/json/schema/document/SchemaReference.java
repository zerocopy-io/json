package io.zerocopy.json.schema.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import io.zerocopy.json.core.ObjectUtils;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.SchemaUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Describes a schema within its document, where references (JSON pointers) can be resolved
 */
public class SchemaReference {
    public static final boolean STRICT_NODE_VALIDATION = true;
    private SchemaDocument document;
    /**
     * A pointer relative to {@link #document}
     */
    private JsonPointer schemaPointer;
    private JsonNode node;

    public SchemaReference(final SchemaDocument document, JsonPointer schemaPointer, final JsonNode node) {
        if (document == null || document.getRoot() == null || schemaPointer == null)
            throw new IllegalArgumentException();

        this.document = document;
        this.schemaPointer = schemaPointer;
        this.node = node;

        if (this.node == null)
            resolve();
        else
            validate();
    }

    public SchemaReference(SchemaDocument document, JsonPointer schemaPointer) {
        this(document, schemaPointer, null);
    }

    public SchemaReference(SchemaDocument document, String schemaPointer) {
        this(document, JsonPointer.compile(schemaPointer), null);
    }

    public SchemaReference(SchemaDocument document) {
        this(document, JsonPointer.compile(""), null);
    }

    public static Collection<SchemaReference> fromDocuments(List<SchemaDocument> schemas) {
        //for compatibility with older Java versions on older platforms
        List<SchemaReference> schemaReferences = new ArrayList<SchemaReference>(schemas.size());
        for (SchemaDocument schema : schemas)
            schemaReferences.add(new SchemaReference(schema));
        return schemaReferences;
    }

    public SchemaDocument getDocument() {
        validate();
        return document;
    }

    public void setDocument(SchemaDocument document) {
        this.document = document;
        resolve();
    }

    public JsonNode getRoot() {
        validate();
        return this.document != null ? this.document.getRoot() : null;
    }

    public JsonPointer getSchemaPointer() {
        validate();
        return schemaPointer;
    }

    public void setSchemaPointer(JsonPointer schemaPointer) {
        this.schemaPointer = schemaPointer;
        resolve();
    }

    @JsonIgnore
    public JsonNode getSchema() {
        validate();
        return node;
    }

    public static SchemaReference create(SchemaDocument document, JsonPointer pointer, JsonNode node) {
        return new SchemaReference(document, pointer, node);
    }

    public static SchemaReference create(SchemaDocument document, String pointer, JsonNode node) {
        return create(document, JsonPointer.compile(pointer), node);
    }

    public static SchemaReference create(JsonSchemaV7 schema, String pointer, JsonNode node) {
        //TODO document reference
        return new SchemaReference(new SchemaDocument(null, node));
    }

    @Deprecated //TODO temporary helper method
    public static SchemaReference findChild(SchemaDocument document, JsonNode jsonNode) {
        JsonPointer resolvedNode = SchemaUtils.findNode(document.getRoot(), jsonNode);
        return create(document, resolvedNode, jsonNode);
    }

    public SchemaReference createRelative(JsonPointer pointer, JsonNode node) {
        if (this.schemaPointer != null) {
            if (pointer != null)
                pointer = this.schemaPointer.append(pointer);
            else
                pointer = this.schemaPointer;
        }
        if (pointer == null)
            pointer = JsonPointer.empty();
        return new SchemaReference(this.document, pointer, node);
    }

    public SchemaReference createRelative(String pointer, JsonNode node) {
        return createRelative(JsonPointer.compile(pointer), node);
    }

    public SchemaReference resolveRelative(JsonPointer pointer) {
        return createRelative(pointer, this.node.at(pointer));
    }

    public SchemaReference resolveRelative(String pointer) {
        return resolveRelative(JsonPointer.compile(pointer));
    }

    @Deprecated //TODO temporary helper method
    public SchemaReference findChild(JsonNode jsonNode) {
        JsonPointer resolvedNode = SchemaUtils.findNode(this.node, jsonNode);
        return createRelative(resolvedNode, jsonNode);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchemaReference that = (SchemaReference) o;
        return document.getRoot() == that.document.getRoot() &&
                schemaPointer.equals(that.schemaPointer) &&
                node == that.node;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(document, schemaPointer, node);
    }

    private void resolve() {
        JsonNode root = this.document != null ? this.document.getRoot() : null;

        if (this.schemaPointer != null) {
            this.node = null;
            if (root != null) {
                this.node = root.at(this.schemaPointer);
                if (this.node != null && this.node.isMissingNode())
                    this.node = null;
            }
        } else
            this.node = root;

        validate();
    }

    private void validate() {
        validate(this.document != null ? this.document.getRoot() : null, this.schemaPointer, this.node);
    }

    private static void validate(JsonNode root, JsonPointer schemaPointer, JsonNode node) {
        if (STRICT_NODE_VALIDATION) {
            JsonNode resolvedNode = root.at(schemaPointer);
            if (resolvedNode.isMissingNode())
                resolvedNode = null;

            JsonPointer expectedPointer = SchemaUtils.findNode(root, node);

            if (node == null || node != resolvedNode) {
                StringBuilder errorMessage = new StringBuilder();

                if (resolvedNode != null)
                    errorMessage.append("wrong node at ");
                else
                    errorMessage.append("missing node at ");
                errorMessage.append('"').append(schemaPointer).append('"');

                if (expectedPointer != null)
                    errorMessage.append(", found node at ")
                            .append('"').append(expectedPointer).append('"');
                else
                    errorMessage.append(", nod does not exist");

                throw new IllegalArgumentException(errorMessage.toString());
            }
        }
    }
}
