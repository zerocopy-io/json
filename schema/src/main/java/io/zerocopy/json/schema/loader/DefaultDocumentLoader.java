package io.zerocopy.json.schema.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

/**
 * Loads known default schemas and hyper-schemas from prepackaged resources instead of from http://json-schema.org/
 */
public class DefaultDocumentLoader implements DocumentLoader {
    @Override
    public InputStream openURL(URL url) throws IOException {
        if (url.toString().matches("^https?://json-schema.org/+.*?#?$")) {
            String resourcePath = URI.create("/io/zerocopy/json/schema/" + url.getPath()).normalize().toASCIIString();
            return getClass().getResourceAsStream(resourcePath);
        }
        return null;
    }
}