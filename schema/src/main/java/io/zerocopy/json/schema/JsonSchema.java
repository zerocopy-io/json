package io.zerocopy.json.schema;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JsonSchema {

    public enum Version {
        DRAFT_4("http://json-schema.org/draft-04/schema#"),
        DRAFT_6("http://json-schema.org/draft-06/schema#"),
        DRAFT_7("http://json-schema.org/draft-07/schema#"),
        DRAFT_2019_09("https://json-schema.org/draft/2019-09/schema");

        private final URI uri;

        Version(String uri) {
            this.uri = URI.create(uri);
        }

        public URI getUri() {
            return uri;
        }

        public static Version forUri(URI uri) {
            return URI_MAP.get(uri);
        }

        public static final Version LATEST = values()[values().length - 1];

        public static final Map<URI, Version> URI_MAP;

        static {
            Map<URI, Version> map = new HashMap<URI, Version>();

            for (Version version : Version.values())
                if (map.put(version.getUri(), version) != null)
                    throw new IllegalStateException("duplicate schema URI");

            URI_MAP = Collections.unmodifiableMap(map);
        }
    }

    @JsonProperty("$schema")
    public URI schema;
    @JsonProperty("$id")
    public String id;
    @JsonProperty("$ref")
    public String ref;
    @JsonProperty("$anchor")
    public String anchor;
    @JsonAnySetter
    public ObjectNode nonSchemaProperties;
}


