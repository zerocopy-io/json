package io.zerocopy.json.schema.context;

import java.net.URI;
import java.util.LinkedList;

public class SchemaResolutionSubContext {
    public LinkedList<SchemaResolutionNode> nodes = new LinkedList<SchemaResolutionNode>();

    public URI getUpperId() {
        return createId(nodes);
    }

    public URI getFullId() {
        if (nodes.isEmpty() || (nodes.getLast().id == null && nodes.getLast().anchor == null))
            return null;

        return createId(nodes);
    }

    private static URI createId(Iterable<SchemaResolutionNode> nodes) {
        URI id = null;
        for (SchemaResolutionNode node : nodes) {
            String subId = "";

            if (node.id != null) {
                subId += node.id;
            }
            if (node.anchor != null) {
                subId += "#";
                subId += node.anchor;
            }

            if (!subId.isEmpty()) {
                if (id == null)
                    id = URI.create(subId);
                else
                    id = id.resolve(subId);
            }
        }
        return id != null ? id.normalize() : null;
    }
}
