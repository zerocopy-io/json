package io.zerocopy.json.schema.context;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.JsonSchemaDeserializer;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.loader.DocumentLoader;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class SchemaResolutionContext {
    private JsonSchema.Version defaultSchemaVersion = JsonSchema.Version.LATEST;
    private DocumentLoader documentLoader;

    private LinkedList<SchemaResolutionSubContext> subContexts = new LinkedList<SchemaResolutionSubContext>();
    private final Map<URI, SchemaDocument> documentCache = new ConcurrentHashMap<URI, SchemaDocument>();
    private final ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;

    public JsonSchema.Version getDefaultSchemaVersion() {
        return defaultSchemaVersion;
    }

    public void setDefaultSchemaVersion(JsonSchema.Version defaultSchemaVersion) {
        this.defaultSchemaVersion = defaultSchemaVersion;
    }

    public DocumentLoader getDocumentLoader() {
        return documentLoader;
    }

    public void setDocumentLoader(DocumentLoader documentLoader) {
        this.documentLoader = documentLoader;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public JsonSchemaV7 pushSchemaNode(URI rootUri, SchemaReference schemaReference, boolean requireSchema) {
        String id = null;
        String anchor = null;
        URI schemaVersion = null;

        JsonSchemaV7 schema = requireSchema || JsonSchemaDeserializer.canDeserialize(schemaReference.getSchema()) ? mapSchema(schemaReference) : null;

        if (schema != null) {
            id = schema.id;
            anchor = schema.anchor;
            schemaVersion = schema.schema;
        }

        if (rootUri != null && schemaReference.getRoot() == schemaReference.getSchema()) {
            if (id != null) {
                if (!URI.create(id).equals(rootUri))
                    throw new IllegalArgumentException();
            } else
                id = rootUri.normalize().toASCIIString();
        }

        boolean isSchemaRoot = id != null && URI.create(id).getRawAuthority() != null;
        SchemaResolutionSubContext schemaResolutionSubContext;
        if (isSchemaRoot || subContexts.isEmpty()) {
            schemaResolutionSubContext = new SchemaResolutionSubContext();
            subContexts.addLast(schemaResolutionSubContext);
        } else
            schemaResolutionSubContext = subContexts.getLast();

        SchemaResolutionNode schemaResolutionNode = new SchemaResolutionNode();

        schemaResolutionNode.rootNode = schemaReference.getRoot();
        schemaResolutionNode.path = schemaReference.getSchemaPointer();
        schemaResolutionNode.node = schemaReference.getSchema();
        schemaResolutionNode.id = id;
        schemaResolutionNode.anchor = anchor;
        schemaResolutionNode.schemaVersion = schemaVersion;

        schemaResolutionSubContext.nodes.addLast(schemaResolutionNode);

        return schema;
    }

    public void popSchemaNode(JsonNode rootNode, String path, JsonNode node) {
        SchemaResolutionSubContext schemaResolutionSubContext = subContexts.getLast();
        if (schemaResolutionSubContext.nodes.getLast().node == node) {
            schemaResolutionSubContext.nodes.removeLast();
            if (schemaResolutionSubContext.nodes.isEmpty())
                subContexts.removeLast();
        } else
            throw new IllegalStateException();
    }

    public void popSchemaNode(JsonNode rootNode, List<String> path, JsonNode node) {
        StringBuilder pointer = new StringBuilder();
        JsonPointerUtils.implode(pointer, path);
        popSchemaNode(rootNode, pointer.toString(), node);
    }

    public void popSchemaNode(JsonNode rootNode, JsonPointer jsonPointer, JsonNode node) {
        popSchemaNode(rootNode, jsonPointer.toString(), node);
    }

    public void popSchemaNode(SchemaReference schemaReference) {
        popSchemaNode(schemaReference.getRoot(), schemaReference.getSchemaPointer(), schemaReference.getSchema());
    }

    public JsonSchemaV7 pushSchemaReference(SchemaReference schemaReference) {
        return pushSchemaNode(null, schemaReference, true);
    }

    public void popSchemaReference(SchemaReference schemaReference) {
        popSchemaNode(schemaReference);
    }

    public URI getUpperId() {
        if (!subContexts.isEmpty())
            return subContexts.getLast().getUpperId();
        else
            return null;
    }

    public URI getFullId() {
        if (!subContexts.isEmpty())
            return subContexts.getLast().getFullId();
        else
            return null;
    }

    public void clear() {
        subContexts.clear();
    }

    public JsonSchemaV7 mapSchema(SchemaReference schemaReference) {
        return JsonSchemaDeserializer.mapSchema(getDefaultSchemaVersion(), schemaReference);
    }

    private SchemaResolutionContext createResolutionContext() {
        final SchemaResolutionContext resolutionContext = new SchemaResolutionContext();
        resolutionContext.setDefaultSchemaVersion(this.getDefaultSchemaVersion());
        return resolutionContext;
    }

    public static URI getUri(Iterable<JsonNode> nodes, boolean current) {
        URI id = null;
        boolean isCurrent = false;

        for (JsonNode childNode : nodes) {
            JsonNode idNode = childNode.get("$id");
            JsonNode anchorNode = childNode.get("$anchor");

            isCurrent = false;

            if (idNode == null)
                idNode = childNode.get("id");
            if (anchorNode == null)
                anchorNode = childNode.get("anchor");

            if (idNode != null && idNode.isTextual()) {
                isCurrent = true;
                URI idUri = URI.create(idNode.asText());
                if (id == null)
                    id = idUri;
                else
                    id = id.resolve(idUri);
            }
            if (anchorNode != null && anchorNode.isTextual()) {
                isCurrent = true;
                URI anchorUri = URI.create("#" + anchorNode.asText());
                if (id == null)
                    id = anchorUri;
                else
                    id = id.resolve(anchorUri);
            }
        }
        return (!current || isCurrent) ? id : null;
    }

    private static boolean isNullOrEmpty(String str) {
        return str == null || str.equals("");
    }

    public SchemaReference resolveSchema(final SchemaReference schemaReference, String ref) throws IOException {
        SchemaReference result;

        final JsonNode rootNode = schemaReference.getRoot();
        final JsonNode schemaNode = schemaReference.getSchema();

        SchemaDocument referencedDocument;

        final URI refUri = URI.create(ref);

        final AtomicReference<URI> currentUri = new AtomicReference<URI>(null);
        final List<SchemaReference> embeddedReferences = new ArrayList<SchemaReference>();

        SchemaUtils.visitAllNodes(rootNode, new SchemaUtils.NodeVisitor() {
            final LinkedList<JsonNode> nodes = new LinkedList<JsonNode>();

            @Override
            public void push(LinkedList<String> path, JsonNode node) {
                nodes.addLast(node);

                URI parentId = getUri(nodes, false);
                URI id = getUri(nodes, true);

                if (currentUri.get() == null) {
                    if (schemaNode == node) {
                        currentUri.set(parentId);
                    }
                }

                if (refUri.equals(id)) {
                    embeddedReferences.add(new SchemaReference(schemaReference.getDocument(), JsonPointer.compile(JsonPointerUtils.implode(path)), node));
                }
            }

            @Override
            public void pop(LinkedList<String> path, JsonNode node) {
                nodes.removeLast();
            }
        });

        if (embeddedReferences.size() > 1)
            throw new IOException("duplicate entries for ref: " + ref);

        if (!embeddedReferences.isEmpty()) {
            result = embeddedReferences.get(0);
            return result;
        }

        if (isNullOrEmpty(refUri.getAuthority()) && isNullOrEmpty(refUri.getPath())) {
            result = SchemaUtils.resolveRelativeRefPath(schemaReference.getDocument(), schemaNode, ref);
            if (result != null)
                return result;
        }


        final URI destUri;

        if (currentUri.get() != null)
            destUri = currentUri.get().resolve(refUri);
        else
            destUri = refUri;

        currentUri.set(null);
        embeddedReferences.clear();

        SchemaUtils.visitAllNodes(rootNode, new SchemaUtils.NodeVisitor() {
            final LinkedList<JsonNode> nodes = new LinkedList<JsonNode>();

            @Override
            public void push(LinkedList<String> path, JsonNode node) {
                nodes.addLast(node);

                URI parentId = getUri(nodes, false);
                URI id = getUri(nodes, true);

                if (currentUri.get() == null) {
                    if (schemaNode == node) {
                        currentUri.set(parentId);
                    }
                }

                if (destUri.equals(id)) {
                    embeddedReferences.add(new SchemaReference(schemaReference.getDocument(), JsonPointer.compile(JsonPointerUtils.implode(path)), node));
                }
            }

            @Override
            public void pop(LinkedList<String> path, JsonNode node) {
                nodes.removeLast();
            }
        });

        if (embeddedReferences.size() > 1)
            throw new IOException("duplicate entries for ref: " + ref);

        if (!embeddedReferences.isEmpty()) {
            result = embeddedReferences.get(0);
            return result;
        }

        if (destUri.getScheme() != null) {
            //reference to external document
            if (this.getDocumentLoader() != null) {
                referencedDocument = this.documentCache.get(destUri);
                if (referencedDocument == null) {
                    InputStream inputStream = this.getDocumentLoader().openURL(destUri.toURL());
                    if (inputStream != null)
                        referencedDocument = new SchemaDocument(destUri, this.getObjectMapper().readTree(inputStream));
                }

                if (referencedDocument == null)
                    throw new IOException("Failed to load schema: " + destUri);
            } else
                throw new IOException("Failed to load schema, no schema loader installed: " + destUri);
        } else {
            //local reference
            referencedDocument = schemaReference.getDocument();
        }

        if (destUri.getFragment() != null) {
            result = SchemaUtils.resolveRelativeRefPath(referencedDocument, referencedDocument.getRoot(), "#" + destUri.getFragment());
        } else {
            result = new SchemaReference(referencedDocument);
        }

        return result;
    }

    public JsonPointer pointer() {
        SchemaResolutionNode entry = this.subContexts.getLast().nodes.getLast();
        return !entry.path.toString().isEmpty() ? entry.path : JsonPointer.compile("/");
    }
}
