package io.zerocopy.json.schema;

import io.zerocopy.json.schema.document.SchemaReference;

public class SchemaException extends RuntimeException {
    private final SchemaReference location;
    private final SchemaProblem error;

    public SchemaException(SchemaReference location, SchemaProblem error, String message, Throwable cause) {
        super(message, cause);
        this.location = location;
        this.error = error;
    }

    public SchemaException(SchemaReference location, SchemaProblem error, String message) {
        this(location, error, message, null);
    }

    public SchemaReference getLocation() {
        return location;
    }

    public SchemaProblem getError() {
        return error;
    }
}
