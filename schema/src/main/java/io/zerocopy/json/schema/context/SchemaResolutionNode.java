package io.zerocopy.json.schema.context;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;

import java.net.URI;

public class SchemaResolutionNode {
    public JsonNode rootNode;
    public JsonPointer path;
    public JsonNode node;
    public String id;
    public String anchor;
    public URI schemaVersion;
}
