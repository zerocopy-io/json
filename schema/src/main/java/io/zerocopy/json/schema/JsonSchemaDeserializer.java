package io.zerocopy.json.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class JsonSchemaDeserializer {
    /**
     * Maps all supported schema versions to a draft-7 class
     *
     * @param defaultSchemaVersion the default schema version to use, in case the schema does not contain an according property
     * @param schemaReference      the schema to map
     * @return draft-07 mapping
     * @throws SchemaException for unsupported schema versions or invalid schemas
     */
    public static JsonSchemaV7 mapSchema(JsonSchema.Version defaultSchemaVersion, SchemaReference schemaReference) {
        JsonSchemaV7 schema = new JsonSchemaV7();

        URI schemaUri = schemaReference.getDocument().getRootUri();
//        if (schemaUri != null) {
//            if (schemaReference.getSchemaPointer() != null) {
//                StringBuilder newFragment = new StringBuilder();
//                if (schemaUri.getFragment() != null)
//                    newFragment.append(schemaUri.getFragment());
//                newFragment.append(schemaReference.getSchemaPointer().toString());
//                schemaUri = schemaUri.resolve("#" + newFragment);
//            }
//        }

        schema.document = new SchemaDocument(schemaUri, schemaReference.getSchema());

        JsonNode schemaNode = schema.document.getRoot();

        JsonSchema.Version schemaVersion;

        if (schemaNode == null)
            throw new NullPointerException();
        if (!(schemaNode.isObject() || schemaNode.isBoolean() || schemaNode.isNull()))
            throw new IllegalArgumentException("invalid node type: " + schemaNode.getNodeType());

        if (schemaNode.isObject()) {
            ObjectNode node = JsonNodeFactory.instance.objectNode();
            node.setAll((ObjectNode) schemaNode);

            {
                String schemaVersionStr = string(node.remove("$schema"));

                if (schemaVersionStr != null) {
                    URI schemaVersionUri = URI.create(schemaVersionStr).normalize();
                    schemaVersion = JsonSchema.Version.forUri(schemaVersionUri);
                    if (schemaVersion == null)
                        throw new SchemaException(null, SchemaProblem.UNSUPPORTED_SCHEMA_VERSION, "Unsupported schema: " + schemaVersionUri);
                } else
                    schemaVersion = defaultSchemaVersion;

                {
                    if (schemaVersion == null) {
                        JsonNode idNode = node.remove("id");
                        if (idNode != null) {
                            schema.id = string(idNode);
                            if (schema.id != null)
                                schemaVersion = JsonSchema.Version.DRAFT_4;
                            else
                                schema.id = string(node.remove("$id"));
                        }
                    } else if (schemaVersion == JsonSchema.Version.DRAFT_4)
                        schema.id = string(node.remove("id"));
                    else if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_6.ordinal())
                        schema.id = string(node.remove("$id"));
                }

                if (schemaVersion == null)
                    throw new SchemaException(null, SchemaProblem.UNSUPPORTED_SCHEMA_VERSION, "The schema version could not be determined");
            }

            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_2019_09.ordinal())
                schema.anchor = string(node.remove("$anchor"));

            schema.ref = string(node.remove("$ref"));

            {
                JsonNode typeNode = node.remove("type");
                if (typeNode != null) {
                    if (typeNode.isTextual())
                        schema.type = new JsonSchemaV7.Type[]{JsonSchemaV7.Type.forValue(string(typeNode))};
                    else if (typeNode.isArray()) {
                        schema.type = new JsonSchemaV7.Type[typeNode.size()];
                        int i = 0;
                        for (JsonNode typeElement : typeNode)
                            schema.type[i++] = JsonSchemaV7.Type.forValue(string(typeElement));
                    } else
                        throw new IllegalArgumentException();
                }
            }

            schema.title = string(node.remove("title"));
            schema.description = string(node.remove("description"));
            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_7.ordinal())
                schema.comment = string(node.remove("comment"));
            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_6.ordinal())
                schema.examples = array(node.remove("examples"));
            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_6.ordinal())
                schema._const = node.remove("const");
            schema._default = node.remove("default");
            schema._enum = array(node.remove("enum"));

            schema.allOf = array(node.remove("allOf"));
            schema.anyOf = array(node.remove("anyOf"));
            schema.oneOf = array(node.remove("oneOf"));
            schema.not = node.remove("not");

            schema.maxProperties = integer(node.remove("maxProperties"));
            schema.minProperties = integer(node.remove("minProperties"));
            schema.properties = (ObjectNode) node.remove("properties");
            schema.required = array(node.remove("required"));
            schema.patternProperties = getPatternProperties(node.remove("patternProperties"));
            schema.additionalProperties = node.remove("additionalProperties");
            schema.dependencies = node.remove("dependencies");
            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_6.ordinal()) {
                schema.propertyNames = node.remove("propertyNames");
            }

            schema.items = node.remove("items");
            schema.additionalItems = node.remove("additionalItems");
            schema.minItems = integer(node.remove("minItems"));
            schema.maxItems = integer(node.remove("maxItems"));
            schema.uniqueItems = bool(node.remove("uniqueItems"));
            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_6.ordinal())
                schema.contains = node.remove("contains");

            schema.format = JsonSchemaV7.Format.forValue(string(node.remove("format")));
            schema.pattern = string(node.remove("pattern"));
            schema.minLength = integer(node.remove("minLength"));
            schema.maxLength = integer(node.remove("maxLength"));
            schema.contentMediaType = string(node.remove("contentMediaType"));
            schema.contentEncoding = string(node.remove("contentEncoding"));

            schema.minimum = dec(node.remove("minimum"));
            schema.maximum = dec(node.remove("maximum"));

            JsonNode exclusiveMinimumNode = node.remove("exclusiveMinimum");
            JsonNode exclusiveMaximumNode = node.remove("exclusiveMaximum");

            if (schemaVersion == null) {
                if (exclusiveMinimumNode != null && exclusiveMinimumNode.isBoolean()
                        || exclusiveMaximumNode != null && exclusiveMaximumNode.isBoolean())
                    schemaVersion = JsonSchema.Version.DRAFT_4;
            }

            if (schemaVersion == JsonSchema.Version.DRAFT_4) {
                if (bool(exclusiveMinimumNode) == Boolean.TRUE) {
                    schemaVersion = JsonSchema.Version.DRAFT_4;
                    schema.exclusiveMinimum = schema.minimum;
                    schema.minimum = null;
                }
                if (bool(exclusiveMaximumNode) == Boolean.TRUE) {
                    schemaVersion = JsonSchema.Version.DRAFT_4;
                    schema.exclusiveMaximum = schema.maximum;
                    schema.maximum = null;
                }
            } else {
                schema.exclusiveMinimum = dec(exclusiveMinimumNode);
                schema.exclusiveMaximum = dec(exclusiveMaximumNode);
            }
            schema.multipleOf = dec(node.remove("multipleOf"));

            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_7.ordinal()) {
                schema._if = node.remove("if");
                schema._then = node.remove("then");
                schema._else = node.remove("else");
            }

            if (schemaVersion == JsonSchema.Version.DRAFT_4)
                schema.numberWithZeroFractionAsInteger = false;

            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_7.ordinal()) {
                schema.readOnly = bool(node.remove("readOnly"));
                schema.writeOnly = bool(node.remove("writeOnly"));
            }

            if (schemaVersion.ordinal() >= JsonSchema.Version.DRAFT_2019_09.ordinal()) {
                schema.dependentRequired = node.remove("dependentRequired");
                schema.dependentSchemas = node.remove("dependentSchemas");
                schema.minContains = integer(node.remove("minContains"));
                schema.maxContains = integer(node.remove("maxContains"));
                schema.unevaluatedItems = node.remove("unevaluatedItems");
                schema.unevaluatedProperties = node.remove("unevaluatedProperties");
            }

            schema.definitions = node.remove("definitions");

            if (node.size() > 0)
                schema.nonSchemaProperties = node;

            schema.schema = schemaVersion.getUri();

        } else if (schemaNode.isBoolean()) {
            schema.booleanValidity = schemaNode.asBoolean();
        } else if (schemaNode.isNull()) {
            schema.booleanValidity = true;
        } else
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "unsupported node type: " + schemaNode.getNodeType());
        return schema;
    }

    private static Map<String, JsonNode> getPatternProperties(JsonNode node) {
        if (node == null)
            return null;
        if (!node.isObject())
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "not an object");

        Map<String, JsonNode> map = new TreeMap<String, JsonNode>();
        for (Iterator<Map.Entry<String, JsonNode>> it = node.fields(); it.hasNext(); ) {
            Map.Entry<String, JsonNode> propertyEntry = it.next();
            String pattern = propertyEntry.getKey();
            map.put(pattern, propertyEntry.getValue());
        }
        return map;
    }

    private static String string(JsonNode node) {
        if (node == null)
            return null;
        if (!node.isTextual())
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "not a string");
        return node.asText();
    }

    private static Boolean bool(JsonNode node) {
        if (node == null)
            return null;
        if (!node.isBoolean())
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "not a boolean");
        return node.asBoolean();
    }

    private static Integer integer(JsonNode node) {
        if (node == null)
            return null;
        if (!node.isInt())
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "not an integer");
        return node.asInt();
    }

    private static BigDecimal dec(JsonNode node) {
        if (node == null)
            return null;
        if (!node.isNumber())
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "not a number");
        return node.decimalValue();
    }

    private static ArrayNode array(JsonNode node) {
        if (node == null)
            return null;
        if (!node.isArray())
            throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "not an array");
        return (ArrayNode) node;
    }

    public static boolean canDeserialize(JsonNode node) {
        return node != null && (node.isObject() || node.isBoolean());
    }
}
