package io.zerocopy.json.schema.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public interface DocumentLoader {
    /**
     * @param url the url to open
     * @return an input stream or null, if not found
     * @throws IOException for errors other than 'not found'
     */
    InputStream openURL(URL url) throws IOException;
}