package io.zerocopy.json.schema;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.core.format.DateTimeConverter;
import io.zerocopy.json.core.format.RFC3339Converter;
import io.zerocopy.json.schema.document.SchemaDocument;

import java.math.BigDecimal;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * http://json-schema.org/latest/json-schema-validation.html
 */
public class JsonSchemaV7 extends JsonSchema {
    public static final URI URI = Version.DRAFT_7.getUri();

    //TODO add lenient mode allowing zero-prefixed numbers
    private static final String IPV4 = "(?:(25[0-5]|2[0-4][0-9]|[1]?[0-9][0-9]?)(\\.(25[0-5]|2[0-4][0-9]|[1]?[0-9][0-9]?)){3})";
    private static final String IPV6 = "(?:[a-fA-F0-9]{0,4}(:[a-fA-F0-9]{0,4}){0,7})";
    private static final String HOSTNAME = "(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,62}(?:\\.[a-zA-Z0-9-]{1,63})+\\.?)";

    private static final String URI_SCHEME = "(?:[^\\s,:/?#\\\\{}]+)";

    private static final String URI_PATH_TOKEN = "(?:[^?#\\s\\\\]+)";
    private static final String URI_QUERY_TOKEN = "(?:[^#\\s\\\\]+)";
    private static final String URI_FRAGMENT_TOKEN = "(?:[^\\s\\\\]+)";

    // ATTENTION: leave \\} escaped (might otherwise cause a syntax error on Android)
    private static final String URI_TEMPLATE_PATH_TOKEN = "(?:\\{[^{}]*\\}|[^?#{}\\s\\\\])";
    private static final String URI_TEMPLATE_QUERY_TOKEN = "(?:\\{[^{}]*\\}|[^#{}\\s\\\\])";
    private static final String URI_TEMPLATE_FRAGMENT_TOKEN = "(?:\\{[^{}]*\\}|[^{}\\s\\\\])";

    private static final String URI_AUTHORITY = "(?:(?:[^\\s@]*@)?(" + IPV4 + "|\\[" + IPV6 + "(?:%25[^]*])?]|" + HOSTNAME + ")(?::[0-9]+)?)";

    private static final String JSON_POINTER_PATH_ELEM = "(?:/+(?:[^/~]|(?:~[01]))*)";

    public enum Rule {
        REF("ref"),
        TYPE("type"),
        MAX_PROPERTIES("maxProperties"),
        MIN_PROPERTIES("minProperties"),
        DEPENDENCY("dependency"),
        PATTERN_PROPERTIES("patternProperties"),
        PROPERTY_NAMES("propertyNames"),
        REQUIRED("required"),
        ADDITIONAL_PROPERTIES("additionalProperties"),
        MIN_ITEMS("minItems"),
        MAX_ITEMS("maxItems"),
        ITEMS("items"),
        NOT("not"),
        UNIQUE_ITEMS("uniqueItems"),
        CONTENT_ENCODING("contentEncoding"),
        CONTENT_MEDIA_TYPE("contentMediaType"),
        FORMAT("format"),
        PATTERN("pattern"),
        MIN_LENGTH("minLength"),
        MAX_LENGTH("maxLength"),
        MINIMUM("minimum"),
        EXCLUSIVE_MINIMUM("exclusiveMinimum"),
        MAXIMUM("maximum"),
        EXCLUSIVE_MAXIMUM("exclusiveMaximum"),
        MULTIPLE_OF("multipleOf"),
        CONST("const"),
        ENUM("enum"),
        ANY_OF("anyOf"),
        ONE_OF("oneOf"),
        ALL_OF("allOf"),

        BOOLEAN_SCHEMA(""),

        CONTAINS("contains"),
        MIN_CONTAINS("minContains"),
        MAX_CONTAINS("maxContains"),
        ;

        @JsonValue
        public final String value;
        public final String pointer;

        Rule(String pointer) {
            this.value = name().toLowerCase();
            this.pointer = pointer;
        }
    }

    public enum Type {
        OBJECT("object", JsonNodeType.OBJECT),
        ARRAY("array", JsonNodeType.ARRAY),
        STRING("string", JsonNodeType.STRING),
        NUMBER("number", JsonNodeType.NUMBER),
        INTEGER("integer", JsonNodeType.NUMBER, NUMBER),
        BOOLEAN("boolean", JsonNodeType.BOOLEAN),
        NULL("null", JsonNodeType.NULL);

        @JsonValue
        public final String value;
        public final JsonNodeType nodeType;
        public final Type superType;

        Type(String value, JsonNodeType nodeType, Type superType) {
            this.value = value;
            this.nodeType = nodeType;
            this.superType = superType;
        }

        Type(String value, JsonNodeType nodeType) {
            this(value, nodeType, null);
        }

        private static final Map<String, Type> valueMapping = new HashMap<String, Type>();

        static {
            for (Type type : values())
                valueMapping.put(type.value, type);
        }

        public static Type forValue(String value) {
            if (value == null)
                return null;
            Type type = valueMapping.get(value);
            if (type != null)
                return type;
            throw new IllegalArgumentException();
        }
    }

    public interface FormatMatcher {
        boolean matches(String value);
    }

    public enum Format {//TODO all formats
        DATE_TIME("date-time", new RFC3339Converter(RFC3339Converter.Format.DATE_TIME)),
        DATE("date", new RFC3339Converter(RFC3339Converter.Format.DATE)),
        TIME("time", new RFC3339Converter(RFC3339Converter.Format.TIME)),
        DURATION("duration", new DurationMatcher()),

        UUID("uuid", new UUIDMatcher()),

        URI("uri", Pattern.compile(
                "^" +
                        URI_SCHEME + ":" +
                        "(?:" +
                        "(//" + URI_AUTHORITY + "(/" + URI_PATH_TOKEN + "*))?" +
                        "|(?<!//)(" + URI_PATH_TOKEN + "*)?" +
                        ")" +
                        "(\\?" + URI_QUERY_TOKEN + "*)?" +
                        "(#" + URI_FRAGMENT_TOKEN + "*)?" +
                        "$")),
        URI_REFERENCE("uri-reference", Pattern.compile(
                "^(?:" +
                        URI_SCHEME + ":" +
                        "(?:" +
                        "(?:" +
                        "(//" + URI_AUTHORITY + "(/" + URI_PATH_TOKEN + "*))?" +
                        ")" +
                        "|(?<!//)(" + URI_PATH_TOKEN + "*)?" +
                        ")|(" + URI_PATH_TOKEN + "*)?" +
                        ")" +
                        "(\\?" + URI_QUERY_TOKEN + "*)?" +
                        "(#" + URI_FRAGMENT_TOKEN + "*)?" +
                        "$")),
        URI_TEMPLATE("uri-template", Pattern.compile(
                "^(?:" +
                        URI_SCHEME + ":" +
                        "(?:" +
                        "(?:" +
                        "(//" + URI_AUTHORITY + "(/" + URI_TEMPLATE_PATH_TOKEN + "*))?" +
                        ")" +
                        "|(?<!//)(" + URI_TEMPLATE_PATH_TOKEN + "*)?" +
                        ")|(" + URI_TEMPLATE_PATH_TOKEN + "*)?" +
                        ")" +
                        "(\\?" + URI_TEMPLATE_QUERY_TOKEN + "*)?" +
                        "(#" + URI_TEMPLATE_FRAGMENT_TOKEN + "*)?" +
                        "$")),

        IRI("iri"),
        IRI_REFERENCE("iri-reference"),

        EMAIL("email", new EMailAddressMatcher()),
        IDN_EMAIL("idn-email", Pattern.compile("^[^@]+@[^@]+$")),
        HOSTNAME("hostname", Pattern.compile("^" + JsonSchemaV7.HOSTNAME + "$")),
        IDN_HOSTNAME("idn-hostname"),
        IPV4("ipv4", Pattern.compile("^" + JsonSchemaV7.IPV4 + "$")),
        IPV6("ipv6", new IPv6Matcher()),

        REGEX("regex", Pattern.compile("((?<!\\\\[SZ]).)+")),

        JSON_POINTER("json-pointer", Pattern.compile("^" + JSON_POINTER_PATH_ELEM + "*$")),

        RELATIVE_JSON_POINTER("relative-json-pointer", Pattern.compile("^(0|[1-9][0-9]*)(/" + "(?:(?:[^/~]|(~[01]))+)" + "*)*#?$"));

        @JsonValue
        public final String value;
        public final DateTimeConverter dateTimeConverter;
        public final FormatMatcher matcher;

        Format(String value) {
            this.value = value;
            this.dateTimeConverter = null;
            this.matcher = null;
        }

        Format(String value, DateTimeConverter dateTimeConverter) {
            this.value = value;
            this.dateTimeConverter = dateTimeConverter;
            this.matcher = null;
        }

        Format(String value, final FormatMatcher matcher) {
            this.value = value;
            this.dateTimeConverter = null;
            this.matcher = matcher;
        }

        Format(String value, final Pattern pattern) {
            this.value = value;
            this.dateTimeConverter = null;
            this.matcher = new FormatMatcher() {
                @Override
                public boolean matches(String value) {
                    return pattern.matcher(value).matches();
                }
            };
        }

        private static final Map<String, Format> valueMapping = new HashMap<String, Format>();

        static {
            for (Format format : values())
                valueMapping.put(format.value, format);
        }

        public static Format forValue(String value) {
            if (value == null)
                return null;
            Format format = valueMapping.get(value);
            if (format != null)
                return format;
            throw new IllegalArgumentException();
        }

        private static class IPv6Matcher implements FormatMatcher {
            private static boolean isHexDigit(char c) {
                return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f');
            }

            @Override
            public boolean matches(String string) {
                final char[] data = string.toCharArray();

                int digit = 0;
                int numeralStartIndex = -1;
                int numeralEndIndex = -1;
                int numerals = 0;
                int v4numerals = 0;
                int contiguousSeparators = 0;
                int shortZeroes = 0;
                boolean inV4 = false;

                for (int i = 0; i <= data.length; ++i) {
                    final char c = i < data.length ? data[i] : '\0';

                    if (isHexDigit(c)) {
                        if (contiguousSeparators == 1 && numerals == 0)
                            return false;

                        ++digit;

                        if (digit == 1)
                            numeralStartIndex = i;

                        contiguousSeparators = 0;
                    } else if (c == ':') {
                        ++contiguousSeparators;

                        if (inV4)
                            return false;
                        if (digit > 4)
                            return false;

                        if (digit > 0)
                            ++numerals;

                        if (contiguousSeparators == 1) {
                            if (i == data.length - 1)
                                return false;

                            if (i > 0 && !(1 <= digit && digit <= 4))
                                return false;
                            digit = 0;
                        } else if (contiguousSeparators == 2) {
                            if (shortZeroes > 0)
                                return false;
                            else
                                ++shortZeroes;
                        } else
                            return false;

                    } else if (c == '.') {
                        inV4 = true;
                        ++v4numerals;
                        numeralEndIndex = i;

                        if (digit > 0 && digit <= 3) {
                            digit = 0;
                        } else
                            return false;

                        int r = Integer.parseInt(String.valueOf(data, numeralStartIndex, numeralEndIndex - numeralStartIndex));

                        if (r > 255)
                            return false;
                    } else if (c == '\0') {
                        if (contiguousSeparators == 2)
                            break;

                        if (digit == 0)
                            return false;

                        numeralEndIndex = i;

                        if (inV4) {
                            if (digit > 3)
                                return false;
                            ++v4numerals;
                        } else {
                            if (digit > 4)
                                return false;
                            ++numerals;
                        }

                    } else {
                        if (digit > 0)
                            numeralEndIndex = i;
                        return false;
                    }
                }

                int addrBytes = numerals * 2 + v4numerals;

                return (shortZeroes == 0 && addrBytes == 16) || (shortZeroes == 1 && addrBytes < 16);
            }
        }

        private static class EMailAddressMatcher implements FormatMatcher {
            Pattern pattern = Pattern.compile("^(?!\\.)[a-zA-Z0-9.!#$%&'*/=?^_`{|}~+-]*(?<!\\.)@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}?$");

            @Override
            public boolean matches(String value) {
                return !value.contains("..")
                        && pattern.matcher(value).matches()
                        ;
            }
        }

        private static class DurationMatcher implements FormatMatcher {
            Pattern pattern = Pattern.compile(
                    "^P" +
                            "(?:(\\d+)Y)?" +
                            "(?:(\\d+)M)?" +
                            "(?:(\\d+)D)?" +
                            "(?:(\\d+)W)?" +
                            "(T)?" +
                            "(?:(\\d+)H)?" +
                            "(?:(\\d+)M)?" +
                            "(?:(\\d+)S)?" +
                            "$"
            );

            @Override
            public boolean matches(String value) {
                Matcher matcher = pattern.matcher(value);
                if (!matcher.matches())
                    return false;

                String Y = matcher.group(1);
                String M = matcher.group(2);
                String D = matcher.group(3);
                String W = matcher.group(4);

                String T = matcher.group(5);

                String h = matcher.group(6);
                String m = matcher.group(7);
                String s = matcher.group(8);

                if (!(Y != null || M != null || D != null || W != null || h != null || m != null || s != null))
                    return false;

                if (W != null && (Y != null || M != null || D != null || T != null || h != null || m != null || s != null))
                    return false;

                if ((T != null) != (h != null || m != null || s != null))
                    return false;

                return true;
            }
        }

        private static class UUIDMatcher implements FormatMatcher {
            @Override
            public boolean matches(String value) {
                if (value != null) {
                    String[] components = value.split("-");

                    if (components.length != 5)
                        return false;
                    if (components[0].length() != 8)
                        return false;
                    if (components[1].length() != 4)
                        return false;
                    if (components[2].length() != 4)
                        return false;
                    if (components[3].length() != 4)
                        return false;
                    if (components[4].length() != 12)
                        return false;

                    for (String component : components) {
                        for (int i = 0; i < component.length(); ++i) {
                            char c = component.charAt(i);
                            boolean isHexChar
                                    = (c >= '0' && c <= '9')
                                    || (c >= 'a' && c <= 'f')
                                    || (c >= 'A' && c <= 'F');

                            if (!isHexChar)
                                return false;
                        }
                    }

                    return true;
                }
                return false;
            }
        }
    }

    @JsonIgnore
    public SchemaDocument document;

    @JsonIgnore
    public boolean numberWithZeroFractionAsInteger = true;

    @JsonIgnore
    public Boolean booleanValidity;

    // type
    @JsonFormat(with = {JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, JsonFormat.Feature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED})
    public Type[] type;
//    public Discriminator discriminator;

    // schema
    public String title;
    public String description;
    public String comment;
    public ArrayNode examples;

    // common
    @JsonProperty("const")
    public JsonNode _const;
    @JsonProperty("default")
    public JsonNode _default;
    @JsonProperty("enum")
    public ArrayNode _enum;

    public ArrayNode allOf;
    public ArrayNode anyOf;
    public ArrayNode oneOf;
    public JsonNode not;

    // 6.5. Validation Keywords for Objects
    public Integer maxProperties;
    public Integer minProperties;
    public ObjectNode properties;
    public ArrayNode required;
    public Map<String, JsonNode> patternProperties;
    public JsonNode additionalProperties;
    public JsonNode dependencies;
    public JsonNode propertyNames;

    // array
    public JsonNode items;
    public JsonNode additionalItems;
    public Integer minItems;
    public Integer maxItems;
    public Boolean uniqueItems;
    public JsonNode contains;

    // string
    public Format format;
    public String pattern;
    public Integer minLength;
    public Integer maxLength;
    public String contentMediaType;
    public String contentEncoding;

    // number
    public BigDecimal minimum;
    public BigDecimal maximum;

    public BigDecimal exclusiveMinimum;
    public BigDecimal exclusiveMaximum;

    public BigDecimal multipleOf;

    // conditional
    @JsonProperty("if")
    public JsonNode _if;
    @JsonProperty("then")
    public JsonNode _then;
    @JsonProperty("else")
    public JsonNode _else;

    // protocol
    public Boolean readOnly;
    public Boolean writeOnly;

    public JsonNode definitions;


    //draft-2019-09
    @JsonProperty("dependentRequired")
    public JsonNode dependentRequired;

    @JsonProperty("dependentSchemas")
    public JsonNode dependentSchemas;

    @JsonProperty("minContains")
    public Integer minContains;

    @JsonProperty("maxContains")
    public Integer maxContains;

    @JsonProperty("unevaluatedItems")
    public JsonNode unevaluatedItems;

    @JsonProperty("unevaluatedProperties")
    public JsonNode unevaluatedProperties;
}
