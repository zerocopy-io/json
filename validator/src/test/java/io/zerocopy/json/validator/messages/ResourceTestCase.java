package io.zerocopy.json.validator.messages;

import org.junit.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static junit.framework.TestCase.assertNotNull;

public class ResourceTestCase {
    @Test
    public void loadResourceBundle() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(MessageFactory.DEFAULT_RESOURCE_BUNDLE_BASE_NAME, Locale.US);
        assertNotNull(resourceBundle);
    }
}
