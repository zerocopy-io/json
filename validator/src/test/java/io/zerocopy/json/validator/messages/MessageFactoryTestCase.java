package io.zerocopy.json.validator.messages;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ResourceList;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.validator.SchemaViolation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class MessageFactoryTestCase {

    private static Collection<URL> listTestResources(final String... relativePaths) {
        ResourceList resourceList = new ClassGraph()
                .whitelistPaths(relativePaths)
                .scan()
                .getResourcesWithExtension("json");
        return new HashSet<URL>(resourceList.getURLs());
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> parameters() throws Exception {
        List<Object[]> argumentsList = new ArrayList<Object[]>();

        Map<String, JsonSchema.Version> schemaVersionMapping = new LinkedHashMap<String, JsonSchema.Version>();
        schemaVersionMapping.put("io/zerocopy/json/validator/messages", JsonSchema.Version.DRAFT_7);

        for (Map.Entry<String, JsonSchema.Version> schemaVersionEntry : schemaVersionMapping.entrySet()) {
            String testResourcePath = schemaVersionEntry.getKey();
            JsonSchema.Version defaultSchemaVersion = schemaVersionEntry.getValue();

            for (final URL resourceUrl : listTestResources(testResourcePath)) {

                JsonNode testJson = objectMapper.readTree(resourceUrl);
                for (JsonNode testCase : testJson) {
                    JsonNode testCaseDescription = testCase.get("description");
                    JsonNode expectedDescriptors = testCase.get("expectedDescriptors");
                    JsonNode violations = testCase.get("violations");

                    List<JsonNode> schemas = new ArrayList<JsonNode>();
                    for (JsonNode schema : violations) {
                        schemas.add(schema);
                    }

                    String[] testTrace = {
                            testCaseDescription != null ? testCaseDescription.asText() : null,
                            resourceUrl.getPath()
                    };

                    Object[] args = {
                            Arrays.toString(testTrace),
                            testTrace,
                            defaultSchemaVersion,
                            schemas,
                            expectedDescriptors,

                    };
                    argumentsList.add(args);
                }
            }
        }
        return argumentsList;
    }

    private final String testName;
    private final String[] testTrace;
    private final JsonSchema.Version defaultSchemaVersion;
    private final List<JsonNode> violations;
    private final JsonNode expectedDescriptors;

    private static final ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;

    static {
        objectMapper.setDefaultPropertyInclusion(JsonInclude.Include.ALWAYS);
        objectMapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
    }

    public MessageFactoryTestCase(String testName, String[] testTrace, JsonSchema.Version defaultSchemaVersion, List<JsonNode> violations, JsonNode expectedDescriptors) {
        this.testName = testName;
        this.testTrace = testTrace;
        this.defaultSchemaVersion = defaultSchemaVersion;
        this.violations = violations;
        this.expectedDescriptors = expectedDescriptors;
    }

    @Test
    public void messageCreation() throws JsonProcessingException {
        MessageFactory messageFactory = new MessageFactory(Locale.US);

        ArrayList<SchemaViolation> violations = new ArrayList<SchemaViolation>();
        for (JsonNode violation : this.violations)
            violations.add(objectMapper.treeToValue(violation, SchemaViolation.class));

        Map<String, Set<MessageFactory.ErrorDescriptor>> expectedDescriptors = new HashMap<String, Set<MessageFactory.ErrorDescriptor>>();

        for (Iterator<Map.Entry<String, JsonNode>> propertyIt = this.expectedDescriptors.fields(); propertyIt.hasNext(); ) {
            Map.Entry<String, JsonNode> property = propertyIt.next();

            Set<MessageFactory.ErrorDescriptor> descriptorSet = expectedDescriptors.get(property.getKey());
            if (descriptorSet == null) {
                descriptorSet = new HashSet<MessageFactory.ErrorDescriptor>();
                expectedDescriptors.put(property.getKey(), descriptorSet);
            }

            for (JsonNode descriptorNode : property.getValue()) {
                descriptorSet.add(objectMapper.treeToValue(descriptorNode, MessageFactory.ErrorDescriptor.class));
            }
        }

        Map<String, Set<MessageFactory.ErrorDescriptor>> actualDescriptors = new HashMap<String, Set<MessageFactory.ErrorDescriptor>>();

        MessageFactory.coalesceErrors(actualDescriptors, violations);

        for (Map.Entry<String, Set<MessageFactory.ErrorDescriptor>> entry : actualDescriptors.entrySet()) {
            System.out.println(entry.getKey() + ":");
            for (MessageFactory.ErrorDescriptor errorDescriptor : entry.getValue()) {
                System.out.println("    " + messageFactory.createMessage(errorDescriptor));
            }
        }

        assertEquals(objectMapper.writeValueAsString(expectedDescriptors), objectMapper.writeValueAsString(actualDescriptors));
    }
}
