package io.zerocopy.json.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ResourceList;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;
import io.zerocopy.json.validator.messages.MessageFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ValidatorCallbackTestCase {
    private static final ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;
    private static final Validator validator = new Validator();

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> parameters() throws Exception {
        List<Object[]> argumentsList = new ArrayList<Object[]>();

        Map<String, JsonSchema.Version> schemaVersionMapping = new LinkedHashMap<String, JsonSchema.Version>();
        schemaVersionMapping.put("callback-tests", JsonSchema.Version.DRAFT_7);

        for (Map.Entry<String, JsonSchema.Version> schemaVersionEntry : schemaVersionMapping.entrySet()) {
            String testResourcePath = schemaVersionEntry.getKey();
            JsonSchema.Version defaultSchemaVersion = schemaVersionEntry.getValue();

            for (final URL resourceUrl : listTestResources(testResourcePath)) {

                JsonNode testJson = objectMapper.readTree(resourceUrl);
                for (JsonNode testCase : testJson) {
                    JsonNode testCaseDescription = testCase.get("description");
                    JsonNode schema = testCase.get("schema");
                    JsonNode tests = testCase.get("tests");
                    for (JsonNode test : tests) {
                        JsonNode testDescription = test.get("description");
                        String[] testTrace = {
                                testDescription != null ? testDescription.asText() : null,
                                testCaseDescription != null ? testCaseDescription.asText() : null,
                                resourceUrl.getPath()
                        };

                        ArrayNode requiredFieldsArray = (ArrayNode) test.get("requiredFields");

                        Set<String> requiredFields = new HashSet<String>();

                        for (JsonNode jsonNode : requiredFieldsArray) {
                            requiredFields.add(jsonNode.asText());
                        }

                        Object[] args = {
                                Arrays.toString(testTrace),
                                defaultSchemaVersion,
                                testTrace,
                                new SchemaReference(new SchemaDocument(null, schema)),
                                test.get("data"),
                                test.get("valid").asBoolean(),
                                requiredFields
                                ,

                        };
                        argumentsList.add(args);
                    }
                }
            }
        }
        return argumentsList;
    }

    private final String[] testTrace;
    private final JsonSchema.Version defaultSchemaVersion;
    private final SchemaReference schemaReference;
    private final JsonNode value;
    private final boolean expectedResult;
    private final Set<String> expectedRequiredFields;

    public ValidatorCallbackTestCase(String testName, JsonSchema.Version defaultSchemaVersion, String[] testTrace, SchemaReference schemaReference, JsonNode value, boolean expectedResult, Set<String> expectedRequiredFields) {
        this.testTrace = testTrace;
        this.defaultSchemaVersion = defaultSchemaVersion;
        this.schemaReference = schemaReference;
        this.value = value;
        this.expectedResult = expectedResult;
        this.expectedRequiredFields = expectedRequiredFields;

        validator.setDefaultSchemaVersion(defaultSchemaVersion);
    }

    @Test
    public void test() throws Exception {
        MessageFactory messageFactory = new MessageFactory(Locale.US);

        List<SchemaViolation> schemaViolations = new ArrayList<SchemaViolation>();

        Exception exception = null;
        boolean actualResult = true;
        final Set<String> actualRequiredFields = new HashSet<String>();
        try {
            try {
                validator.validate(new ValidatorCallback() {
                    @Override
                    public void onPathPushed(List<String> path, int flags) {
                        if ((flags & FLAG_REQUIRED) != 0)
                            actualRequiredFields.add(JsonPointerUtils.implode(path));
                    }

                    @Override
                    public void onViolation(SchemaViolation violation) {

                    }
                }, schemaReference, value);
            } catch (Exception e) {
                exception = e;
            }

            actualResult = schemaViolations.isEmpty() && exception == null;

            if (expectedResult != actualResult) {
                System.err.println("schema: " + schemaReference.getSchema());
                System.err.println("value: " + value);
                System.err.println();
                for (SchemaViolation schemaViolation : schemaViolations)
                    System.err.println(schemaViolation);
                System.err.println();
                for (String element : testTrace)
                    System.err.println("at " + element);
                System.err.println();
                System.err.flush();
            }

            for (SchemaViolation schemaViolation : schemaViolations) {
                messageFactory.createMessage(schemaViolation); //test for complete message argument mapping
            }
        } finally {
            if (expectedResult != actualResult && exception != null)
                throw exception;
            assertEquals(expectedResult, actualResult);


            assertEquals(expectedRequiredFields, actualRequiredFields);
        }
    }

    private static Collection<URL> listTestResources(final String... relativePaths) {
        ResourceList resourceList = new ClassGraph()
                .whitelistPaths(relativePaths)
                .scan()
                .getResourcesWithExtension("json");
        return new HashSet<URL>(resourceList.getURLs());
    }
}
