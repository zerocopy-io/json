package io.zerocopy.json.validator.impl;

import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.loader.DefaultDocumentLoader;
import io.zerocopy.json.validator.ValidatorCallbackTestCase;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class OfflineTestDocumentLoader extends DefaultDocumentLoader {
    public static final String RESOURCE_BASE_PATH = "/org/json-schema/remotes/";

    @Override
    public InputStream openURL(URL url) throws IOException {
        InputStream inputStream = super.openURL(url);
        if (inputStream != null)
            return inputStream;
        if (url.toString().matches("^http://localhost:1234/+.*$")) {
            String resourcePath = URI.create(RESOURCE_BASE_PATH + url.getPath()).normalize().toASCIIString();
            InputStream resourceStream = getClass().getResourceAsStream(resourcePath);
            if (resourceStream == null)
                throw new IOException("Failed to open schema resource for " + url + ": " + resourcePath);
            return resourceStream;
        } else if (url.getProtocol().equals("file")) {
            String file = url.getFile();
            return new FileInputStream(file);
        } else
            throw new AssertionError("online schema loading is not supported during tests: " + url);
    }

    public static URI resourceUrlToRemoteUri(URL resource) throws IOException, URISyntaxException {
        URI resourceDir = URI.create(ValidatorCallbackTestCase.class.getResource(RESOURCE_BASE_PATH).toURI().normalize().toASCIIString() + "/" + RESOURCE_BASE_PATH).normalize();
        if (resource.getPath().startsWith(resourceDir.getPath())) {
            URI uri = URI.create("http://localhost:1234/" + resource.getPath().substring(resourceDir.getPath().length())).normalize();

            if (SchemaReference.STRICT_NODE_VALIDATION) {
                try {
                    if (new OfflineTestDocumentLoader().openURL(uri.toURL()) == null)
                        throw new AssertionError();
                } catch (Exception e) {
                    throw new AssertionError(e);
                }
            }

            return uri;
        }
        return resource.toURI();
    }
}
