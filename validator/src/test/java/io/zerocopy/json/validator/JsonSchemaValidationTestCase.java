package io.zerocopy.json.validator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.validator.messages.R;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class JsonSchemaValidationTestCase {
    public static final Comparator<SchemaViolation> SCHEMA_VIOLATION_COMPARATOR = new Comparator<SchemaViolation>() {
        @Override
        public int compare(SchemaViolation a, SchemaViolation b) {
            int d = performComparison(a, b);
            boolean equals = a.equals(b);
            boolean hashEquals = a.hashCode() == b.hashCode();

            if ((d == 0) != equals)
                throw new AssertionError();
            if ((d == 0) != hashEquals)
                throw new AssertionError();
            return d;
        }

        private int performComparison(SchemaViolation a, SchemaViolation b) {
            int d;
            d = nullSafeCompare(a.getProperty(), b.getProperty());
            if (d != 0) return d;
            d = nullSafeCompare(a.getRule(), b.getRule());
            if (d != 0) return d;
            d = nullSafeCompare(a.getSeverity(), b.getSeverity());
            if (d != 0) return d;
            d = nullSafeCompare(a.getMessageKey(), b.getMessageKey());
            if (d != 0) return d;
            d = nullSafeCompare(a.getNonLocalizedMessage(), b.getNonLocalizedMessage());
            if (d != 0) return d;
            d = a.toString().compareTo(b.toString());
            if (d != 0) return d;
            return d;
        }

        private <T extends Comparable<T>> int nullSafeCompare(T a, T b) {
            if (a == null || b == null)
                if (a == b)
                    return 0;
                else
                    return a == null ? -1 : 1;
            return a.compareTo(b);
        }

        private <T extends Comparable<T>> int nullSafeCompare(T[] a, T[] b) {
            if (a == null || b == null)
                if (a == b)
                    return 0;
                else
                    return a == null ? -1 : 1;

            int d;

            d = b.length - a.length;
            if (d != 0) return d;

            for (int i = 0; i < a.length; i++) {
                d = nullSafeCompare(a[i], b[i]);
                if (d != 0)
                    break;
            }
            return d;
        }
    };

    private ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;


    @Test
    public void testValidations() throws IOException {
        ObjectNode schemas = JsonNodeFactory.instance.objectNode();
        {
            ObjectNode schema = JsonNodeFactory.instance.objectNode();
            ArrayNode required = JsonNodeFactory.instance.arrayNode();
            required.add("key");
            schema.set("required", required);
            schemas.set("TestEntity", objectMapper.valueToTree(schema));
        }

        TreeSet<SchemaViolation> expectedSchemaViolations = new TreeSet<SchemaViolation>(SCHEMA_VIOLATION_COMPARATOR);
        expectedSchemaViolations.add(new SchemaViolation(Collections.singletonList("key"), "/", JsonSchemaV7.Rule.REQUIRED, R.string.must_be_specified)
                .putMessageArg("property", "key"));

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonSerializableSchema(id = "#/TestEntity")
        class TestEntity {
            public String key;
            public int value;
        }
        TestEntity obj = new TestEntity();

        testValidation(schemas, expectedSchemaViolations, obj);
    }

    @Test(expected = NullPointerException.class)
    public void testValidationWithNullDocument() throws IOException {
        Validator validator = new Validator();
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
        validator.validate(violations, null, JsonNodeFactory.instance.objectNode());
        fail("must not be reached");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationWithNullDocumentRootNode() throws IOException {
        Validator validator = new Validator();
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
        validator.validate(violations, new SchemaReference(null, null, JsonNodeFactory.instance.objectNode()), JsonNodeFactory.instance.objectNode());
        fail("must not be reached");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationWithNullDocumentSchemaNode() throws IOException {
        Validator validator = new Validator();
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
        validator.validate(violations, new SchemaReference(new SchemaDocument(null, JsonNodeFactory.instance.objectNode()), null, (JsonNode) null), JsonNodeFactory.instance.objectNode());
        fail("must not be reached");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationWithNullDocumentNodes() throws IOException {
        Validator validator = new Validator();
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
        validator.validate(violations, new SchemaReference(null, null, (JsonNode) null), JsonNodeFactory.instance.objectNode());
        fail("must not be reached");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationWithInvalidDOMNodeType() throws IOException {
        Validator validator = new Validator();
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();

        JsonNode binaryNode = JsonNodeFactory.instance.binaryNode(new byte[42]);
        validator.validate(violations, new SchemaReference(new SchemaDocument(null, binaryNode)), JsonNodeFactory.instance.objectNode());
        fail("must not be reached");
    }

    private void testValidation(final JsonNode schemas, Set<SchemaViolation> expectedSchemaViolations, Object obj) throws IOException {
        Validator validator = new Validator();

        assertNull(validator.getDocumentLoader());
        assertEquals(JsonSchema.Version.LATEST, validator.getDefaultSchemaVersion());

        String schemaRef = null;
        {
            JsonSerializableSchema jsonSerializableSchema = obj.getClass().getAnnotation(JsonSerializableSchema.class);
            if (jsonSerializableSchema != null)
                schemaRef = jsonSerializableSchema.id();
        }
        JsonNode value = objectMapper.valueToTree(obj);
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();

        JsonNode schema = SchemaUtils.resolveRelativeRef(schemas, schemas, schemaRef);
        validator.validate(violations, new SchemaReference(new SchemaDocument(null, schema)), value);

        {
            TreeSet<SchemaViolation> violationSet = new TreeSet<SchemaViolation>(SCHEMA_VIOLATION_COMPARATOR);
            violationSet.addAll(violations);
            assertEquals(expectedSchemaViolations.toString(), violationSet.toString());
            assertEquals(expectedSchemaViolations, violationSet);
        }
    }
}
