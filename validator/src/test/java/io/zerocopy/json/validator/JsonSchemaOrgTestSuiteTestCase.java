package io.zerocopy.json.validator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.Resource;
import io.github.classgraph.ResourceList;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.schema.document.SchemaDocument;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.validator.impl.OfflineTestDocumentLoader;
import io.zerocopy.json.validator.messages.MessageFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class JsonSchemaOrgTestSuiteTestCase {

    private static final ObjectMapper objectMapper = SchemaUtils.OBJECT_MAPPER;
    private static final Validator validator = new Validator();
    private static final Comparator<URL> URL_COMPARATOR = new Comparator<URL>() {
        @Override
        public int compare(URL url, URL otherUrl) {
            return url.toString().compareTo(otherUrl.toString());
        }
    };

    static {
        validator.setDocumentLoader(new OfflineTestDocumentLoader());
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> parameters() throws Exception {
        List<Object[]> argumentsList = new ArrayList<Object[]>();

        String testResourceBaseDir = "org/json-schema";

        Map<String, JsonSchema.Version> schemaVersionMapping = new LinkedHashMap<String, JsonSchema.Version>();
        schemaVersionMapping.put(testResourceBaseDir + "/tests/draft4", JsonSchema.Version.DRAFT_4);
        schemaVersionMapping.put(testResourceBaseDir + "/tests/draft6", JsonSchema.Version.DRAFT_6);
        schemaVersionMapping.put(testResourceBaseDir + "/tests/draft7", JsonSchema.Version.DRAFT_7);
        //schemaVersionMapping.put(testResourceBaseDir + "/tests/draft2019-09", JsonSchema.Version.DRAFT_2019_09);
        schemaVersionMapping.put(testResourceBaseDir + "/extra-tests/draft7", JsonSchema.Version.DRAFT_7);

        Set<String> excludedTests = new HashSet<String>();
        excludedTests.add(testResourceBaseDir + "/tests/draft4/optional/float-overflow.json");

        excludedTests.add(testResourceBaseDir + "/tests/draft7/optional/format/idn-hostname.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft7/optional/format/iri.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft7/optional/format/iri-reference.json");

        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/optional/format/idn-hostname.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/optional/format/iri.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/optional/format/iri-reference.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/optional/format/iri-reference.json");

        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/id.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/ref.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/refRemote.json");
        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/defs.json");

        excludedTests.add(testResourceBaseDir + "/tests/draft2019-09/unevaluatedProperties.json");

        for (Map.Entry<String, JsonSchema.Version> schemaVersionEntry : schemaVersionMapping.entrySet()) {
            String testResourcePath = schemaVersionEntry.getKey();
            JsonSchema.Version defaultSchemaVersion = schemaVersionEntry.getValue();

            long testCount = 0;
            for (final Resource resource : listTestResources(excludedTests, testResourcePath)) {

                JsonNode testJson = objectMapper.readTree(resource.getURL());
                for (JsonNode testCase : testJson) {
                    JsonNode testCaseDescription = testCase.get("description");
                    JsonNode schema = testCase.get("schema");
                    JsonNode tests = testCase.get("tests");
                    for (JsonNode test : tests) {
                        JsonNode testDescription = test.get("description");
                        String[] testTrace = {
                                resource.getPath(),
                                testDescription != null ? testDescription.asText() : null,
                                testCaseDescription != null ? testCaseDescription.asText() : null
                        };

                        JsonNode validNode = test.get("valid");
                        ArrayNode expectedSchemaViolationsNode = (ArrayNode) test.get("expectedSchemaViolations");

                        if ((validNode == null) == (expectedSchemaViolationsNode == null))
                            throw new IllegalArgumentException("exactly one of \"valid\" or \"expectedSchemaViolations\" must be specified");

                        boolean valid;
                        List<SchemaViolation> expectedSchemaViolations = null;

                        if (expectedSchemaViolationsNode != null) {
                            if (expectedSchemaViolationsNode.size() > 0) {
                                expectedSchemaViolations = new ArrayList<SchemaViolation>(expectedSchemaViolationsNode.size());

                                for (JsonNode expectedSchemaViolationNode : expectedSchemaViolationsNode) {
                                    SchemaViolation schemaViolation = objectMapper.treeToValue(expectedSchemaViolationNode, SchemaViolation.class);
                                    expectedSchemaViolations.add(schemaViolation);
                                }
                            } else
                                expectedSchemaViolations = Collections.emptyList();

                            valid = expectedSchemaViolations.isEmpty();
                        } else {
                            valid = validNode.asBoolean();
                        }

                        if (expectedSchemaViolations != null) {
                            if (valid != expectedSchemaViolations.isEmpty())
                                throw new IllegalArgumentException("contradiction between \"valid\" and \"expectedSchemaViolations\"");
                        }


                        Object[] args = {
                                Arrays.toString(testTrace),
                                defaultSchemaVersion,
                                testTrace,
                                new SchemaReference(new SchemaDocument(null, schema)),
                                test.get("data"),
                                valid,
                                expectedSchemaViolations,
                        };
                        argumentsList.add(args);
                    }
                }
                ++testCount;
            }

            if (testCount == 0)
                throw new IllegalArgumentException("empty test resource dir: " + testResourcePath);
        }

        if (argumentsList.isEmpty())
            throw new IllegalArgumentException("test argument list is empty");

        return argumentsList;
    }

    private final String[] testTrace;
    private final SchemaReference schemaReference;
    private final JsonNode value;
    private final boolean expectedValid;
    private final List<SchemaViolation> expectedSchemaViolations;

    public JsonSchemaOrgTestSuiteTestCase(String testName, JsonSchema.Version defaultSchemaVersion, String[] testTrace, SchemaReference schemaReference, JsonNode value, boolean expectedValid, List<SchemaViolation> expectedSchemaViolations) {
        this.testTrace = testTrace;
        this.schemaReference = schemaReference;
        this.value = value;
        this.expectedValid = expectedValid;
        this.expectedSchemaViolations = expectedSchemaViolations;

        validator.setDefaultSchemaVersion(defaultSchemaVersion);
    }

    @Test
    public void suiteTest() throws Exception {
        MessageFactory messageFactory = new MessageFactory(Locale.US);

        List<SchemaViolation> schemaViolations = new ArrayList<SchemaViolation>();

        Exception exception = null;
        boolean actualValid = true;

        String expectedSchemaViolationsStr = null;
        String actualSchemaViolationsStr = null;

        try {
            try {
                validator.validate(schemaViolations, schemaReference, value);
            } catch (Exception e) {
                exception = e;
            }

            actualValid = schemaViolations.isEmpty() && exception == null;

            if (expectedSchemaViolations != null) {
                TreeSet<SchemaViolation> schemaViolationSet = new TreeSet<SchemaViolation>(SCHEMA_VIOLATION_COMPARATOR);
                schemaViolationSet.addAll(schemaViolations);
                TreeSet<SchemaViolation> expectedSchemaViolationSet = new TreeSet<SchemaViolation>(SCHEMA_VIOLATION_COMPARATOR);
                expectedSchemaViolationSet.addAll(expectedSchemaViolations);

                actualSchemaViolationsStr = toString(schemaViolationSet);
                expectedSchemaViolationsStr = toString(expectedSchemaViolationSet);
            }

            for (SchemaViolation schemaViolation : schemaViolations) {
                messageFactory.createMessage(schemaViolation); //test for complete message argument mapping
            }
        } finally {
            if (expectedValid != actualValid && exception != null)
                throw exception;

            String errorMessage;
            {
                StringWriter errorMessageBuffer = new StringWriter();
                PrintWriter errorMessageWriter = new PrintWriter(errorMessageBuffer);

                errorMessageWriter.println("schema: " + schemaReference.getSchema());
                errorMessageWriter.println("value: " + value);
                errorMessageWriter.println();
                for (SchemaViolation schemaViolation : schemaViolations)
                    errorMessageWriter.println(schemaViolation);
                errorMessageWriter.println();
                for (String element : testTrace)
                    errorMessageWriter.println("at " + element);
                errorMessageWriter.println();
                errorMessageWriter.flush();

                errorMessage = errorMessageBuffer.toString();
            }

            assertEquals(errorMessage, expectedSchemaViolationsStr, actualSchemaViolationsStr);
            assertEquals(errorMessage, expectedValid, actualValid);
        }
    }

    private static ResourceList listTestResources(final Set<String> excludedTests, final String... relativePaths) {
        return new ClassGraph()
                .whitelistPaths(relativePaths)
                .scan()
                .getResourcesWithExtension("json")
                .filter(new ResourceList.ResourceFilter() {
                    @Override
                    public boolean accept(Resource resource) {
                        for (String excludedTestCase : excludedTests)
                            if (resource.getPathRelativeToClasspathElement().equals(excludedTestCase))
                                return false;
                        return true;
                    }
                });
    }

    private static TreeSet<URL> createUrlSet(ResourceList resourceList) {
        TreeSet<URL> urls = new TreeSet<URL>(URL_COMPARATOR);
        urls.addAll(resourceList.getURLs());
        return urls;
    }

    private static String toString(TreeSet<SchemaViolation> schemaViolations) {
        StringBuilder result = new StringBuilder();

        for (SchemaViolation schemaViolation : schemaViolations) {
            result.append(toString(schemaViolation));
            result.append("\n");
        }

        return result.toString();
    }

    private static String toString(SchemaViolation schemaViolation) {
        try {
            return SCHEMA_VIOLATION_WRITER.writeValueAsString(schemaViolation);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Comparator<SchemaViolation> SCHEMA_VIOLATION_COMPARATOR = new Comparator<SchemaViolation>() {
        @Override
        public int compare(SchemaViolation a, SchemaViolation b) {
            return JsonSchemaOrgTestSuiteTestCase.toString(a).compareTo(JsonSchemaOrgTestSuiteTestCase.toString(b));
        }
    };

    private static final ObjectWriter SCHEMA_VIOLATION_WRITER = SchemaUtils.OBJECT_MAPPER.writerFor(SchemaViolation.class);

}
