package io.zerocopy.json.validator.messages;

import io.zerocopy.json.core.values.Value;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.SchemaUtils;
import io.zerocopy.json.validator.SchemaViolation;

import java.math.BigDecimal;
import java.util.*;

public class MessageFactory {

    public static final String DEFAULT_RESOURCE_BUNDLE_BASE_NAME = MessageFactory.class.getPackage().getName().replace(".", "/") + "/validation_errors";

    private ResourceBundle resourceBundle;

    /**
     * Constructs a message factory with the specified resource bundle
     *
     * @param resourceBundle the resource bundle of any locale
     */
    public MessageFactory(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    /**
     * Constructs a message factory with the built-in default resource bundle for the specified locale
     *
     * @param locale the message locale
     */
    public MessageFactory(Locale locale) {
        this(ResourceBundle.getBundle(DEFAULT_RESOURCE_BUNDLE_BASE_NAME, locale));
    }

    public CharSequence createMessage(ErrorDescriptor errorDescriptor) {
        return createMessage(this.resourceBundle, errorDescriptor);
    }

    public CharSequence createMessage(SchemaViolation schemaViolation) {
        return createMessage(this.resourceBundle, schemaViolation);
    }

    public static CharSequence createMessage(ResourceBundle resourceBundle, String nonLocalizedMessage, String messageId, Map<String, Object> messageArgs) {
        if (messageArgs == null)
            messageArgs = Collections.emptyMap();

        if (messageId != null) {
            String resourceValue = resourceBundle.getString(messageId);

            StringBuilder localizedMessage = new StringBuilder();

            boolean escaping = false;
            int variableStart = -1;
            int variableEnd = -1;

            for (int i = 0; i < resourceValue.length(); ++i) {
                char c = resourceValue.charAt(i);
                switch (c) {
                    case '\\':
                        escaping = !escaping;
                        if (!escaping)
                            localizedMessage.append(c);
                        break;
                    case '{':
                        if (escaping) {
                            localizedMessage.append(c);
                        } else {
                            variableStart = i;
                            variableEnd = variableStart;
                        }
                        break;
                    case '}':
                        if (variableStart >= 0 && !escaping) {
                            variableEnd = i + 1;
                            String variableName = resourceValue.substring(variableStart + 1, variableEnd - 1);
                            Object variableValue = messageArgs.get(variableName);
                            if (variableValue == null && !messageArgs.containsKey(variableName))
                                throw new IllegalArgumentException("missing variable: \"" + variableName + "\" for resource key \"" + messageId + "\"");
                            appendMessageArg(localizedMessage, variableValue);
                            variableStart = -1;
                            variableEnd = variableStart;
                            continue;
                        }
                        break;
                    default:
                        if (escaping)
                            throw new IllegalArgumentException("unsupported escape sequence @" + i);
                        if (variableStart >= 0)
                            continue;
                        localizedMessage.append(c);
                        break;
                }
            }

            return localizedMessage;
        } else {
            return nonLocalizedMessage;
        }
    }

    private static void appendMessageArg(StringBuilder localizedMessage, Object variableValue) {
        if (variableValue != null) {
            if (variableValue.getClass().isArray()) {
                localizedMessage.append(Arrays.deepToString((Object[]) variableValue));
            } else
                localizedMessage.append(variableValue.toString());
        } else
            localizedMessage.append("null");
    }

    public static CharSequence createMessage(ResourceBundle resourceBundle, ErrorDescriptor errorDescriptor) {
        CharSequence localizedMessage = createMessage(resourceBundle, errorDescriptor.nonLocalizedMessage, errorDescriptor.messageId, errorDescriptor.messageArgs);
        if (localizedMessage != null)
            return localizedMessage;
        else
            return errorDescriptor.getErrorClass().name();
    }

    public static CharSequence createMessage(ResourceBundle resourceBundle, SchemaViolation schemaViolation) {
        CharSequence localizedMessage = createMessage(resourceBundle, schemaViolation.getNonLocalizedMessage(), schemaViolation.getMessageKey(), schemaViolation.getMessageArgs());
        if (localizedMessage != null)
            return localizedMessage;
        else if (schemaViolation.getRule() != null)
            return "Violation of rule \"" + schemaViolation.getRule() + "\"";
        else
            return ErrorClass.UNKNOWN_PROBLEM.name();
    }

    public enum ErrorClass {
        SCHEMA_VIOLATION,
        MISSING,
        NOT_ALLOWED,
        INVALID_VALUE,
        OUT_OF_RANGE,
        UNKNOWN_PROBLEM
    }

    public static class ErrorDescriptor {
        private ErrorClass errorClass;
        private String nonLocalizedMessage;
        private String messageId;
        private TreeMap<String, Object> messageArgs;

        public ErrorDescriptor() {
        }

        public ErrorDescriptor(ErrorClass errorClass, String messageId, Map<String, Object> messageArgs) {
            this.errorClass = errorClass;
            this.messageId = messageId;
            this.messageArgs = messageArgs != null ? new TreeMap<String, Object>(messageArgs) : null;
        }

        public ErrorDescriptor(ErrorClass errorClass, String messageId) {
            this(errorClass, messageId, null);
        }

        public String getNonLocalizedMessage() {
            return nonLocalizedMessage;
        }

        public void setNonLocalizedMessage(String nonLocalizedMessage) {
            this.nonLocalizedMessage = nonLocalizedMessage;
        }

        public ErrorClass getErrorClass() {
            return errorClass;
        }

        public void setErrorClass(ErrorClass errorClass) {
            this.errorClass = errorClass;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public Map<String, Object> getMessageArgs() {
            return messageArgs;
        }

        public void setMessageArgs(TreeMap<String, Object> messageArgs) {
            this.messageArgs = messageArgs;
        }

        public ErrorDescriptor nonLocalizedMessage(String nonLocalizedMessage) {
            this.nonLocalizedMessage = nonLocalizedMessage;
            return this;
        }

        public <T> ErrorDescriptor addArg(String key, T value) {
            if (this.messageArgs == null)
                this.messageArgs = new TreeMap<String, Object>();
            else if (this.messageArgs.containsKey(key))
                throw new IllegalArgumentException("duplicate key: " + key);
            this.messageArgs.put(key, SchemaUtils.OBJECT_MAPPER.valueToTree(value));
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ErrorDescriptor that = (ErrorDescriptor) o;
            return errorClass == that.errorClass &&
                    Value.equals(nonLocalizedMessage, that.nonLocalizedMessage) &&
                    Value.equals(messageId, that.messageId) &&
                    Value.equals(messageArgs, that.messageArgs);
        }

        @Override
        public int hashCode() {
            return Value.hash(errorClass, nonLocalizedMessage, messageId, messageArgs);
        }

        @Override
        public String toString() {
            return "ErrorDescriptor{" +
                    "errorClass=" + errorClass +
                    ", nonLocalizedMessage='" + nonLocalizedMessage + '\'' +
                    ", messageId='" + messageId + '\'' +
                    ", messageArgs=" + messageArgs +
                    '}';
        }
    }

    private static class ErrorInfo {
        public boolean missing;
        public Set<JsonSchemaV7.Type> types;
        public BigDecimal minimum;
        public BigDecimal maximum;
        public BigDecimal exclusiveMinimum;
        public BigDecimal exclusiveMaximum;
        public BigDecimal multipleOf;
        public Set<Object> _const;

        public List<SchemaViolation> errors = new ArrayList<SchemaViolation>();
        public List<SchemaViolation> uncoalescedErrors = new ArrayList<SchemaViolation>();
    }

    public static void coalesceErrors(Map<String, Set<MessageFactory.ErrorDescriptor>> errorDescriptorsOut, Iterable<SchemaViolation> schemaViolations) {
        Map<String, ErrorInfo> errorDescriptors = new HashMap<String, ErrorInfo>();

        MessageFactory fallbackMessageFactory = new MessageFactory(Locale.US);

        boolean retainNullType = true;//determines how to handle conflicting type definitions, where only one contains "null"

        for (SchemaViolation schemaViolation : schemaViolations) {
            ErrorInfo errorInfo = errorDescriptors.get(schemaViolation.getProperty());
            if (errorInfo == null) {
                errorInfo = new ErrorInfo();
                errorDescriptors.put(schemaViolation.getProperty(), errorInfo);
            }

            if (schemaViolation.getRule() != null)
                switch (schemaViolation.getRule()) {
                    case REQUIRED:
                        errorInfo.missing = true;
                        break;
                    case TYPE:
                        Set<JsonSchemaV7.Type> types = EnumSet.noneOf(JsonSchemaV7.Type.class);
                        Object type = schemaViolation.getMessageArgs().get("type");
                        if (type instanceof Collection)
                            for (String typeElem : ((Collection<String>) type)) {
                                types.add(JsonSchemaV7.Type.forValue(typeElem));
                            }
                        else
                            types.add(JsonSchemaV7.Type.forValue((String) type));
                        if (errorInfo.types == null)
                            errorInfo.types = types;
                        else {
                            if (retainNullType)
                                if (errorInfo.types.contains(JsonSchemaV7.Type.NULL))
                                    types.add(JsonSchemaV7.Type.NULL);
                                else if (types.contains(JsonSchemaV7.Type.NULL))
                                    errorInfo.types.add(JsonSchemaV7.Type.NULL);
                            errorInfo.types.retainAll(types);
                        }
                        break;
                    case MINIMUM:
                        errorInfo.minimum = Value.max(errorInfo.minimum, Value.scalarToBigInteger(schemaViolation.getMessageArgs().get("minimum")));
                        break;
                    case MAXIMUM:
                        errorInfo.maximum = Value.min(errorInfo.maximum, Value.scalarToBigInteger(schemaViolation.getMessageArgs().get("maximum")));
                        break;
                    case EXCLUSIVE_MINIMUM:
                        errorInfo.exclusiveMinimum = Value.max(errorInfo.exclusiveMinimum, Value.scalarToBigInteger(schemaViolation.getMessageArgs().get("exclusiveMinimum")));
                        break;
                    case EXCLUSIVE_MAXIMUM:
                        errorInfo.exclusiveMaximum = Value.min(errorInfo.exclusiveMaximum, Value.scalarToBigInteger(schemaViolation.getMessageArgs().get("exclusiveMaximum")));
                        break;
                    case MULTIPLE_OF:
                        errorInfo.multipleOf = Value.lcm(errorInfo.multipleOf, Value.scalarToBigInteger(schemaViolation.getMessageArgs().get("multipleOf")));
                        break;
                    case CONST:
                        if (errorInfo._const == null)
                            errorInfo._const = new HashSet<Object>();
                        errorInfo._const.add(schemaViolation.getMessageArgs().get("const"));
                        break;
                    default:
                        errorInfo.uncoalescedErrors.add(schemaViolation);
                        break;
                }
            else
                errorInfo.uncoalescedErrors.add(schemaViolation);

            errorInfo.errors.add(schemaViolation);
        }

        for (Map.Entry<String, ErrorInfo> entry : errorDescriptors.entrySet()) {
            String property = entry.getKey();
            ErrorInfo errorInfo = entry.getValue();

            Set<MessageFactory.ErrorDescriptor> descriptorSet = errorDescriptorsOut.get(property);
            if (descriptorSet == null) {
                descriptorSet = new HashSet<MessageFactory.ErrorDescriptor>();
                errorDescriptorsOut.put(property, descriptorSet);
            }

            ErrorDescriptor errorDescriptor = null;

            if (errorInfo.missing) {
                errorDescriptor = new ErrorDescriptor(ErrorClass.MISSING, R.string.must_be_specified)
                        .addArg("property", property);
            }
            if (errorInfo.types != null) {
                if (errorInfo.types.size() == 1) {
                    errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.must_be_of_type)
                            .addArg("property", property)
                            .addArg("type", errorInfo.types.iterator().next())
                    ;
                } else if (errorInfo.types.size() > 1) {
                    errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.must_be_of_one_of_the_following_types)
                            .addArg("property", property)
                            .addArg("type", errorInfo.types.toArray())
                    ;
                } else {
                    if (errorInfo.missing) {
                        errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.required_but_cannot_be_specified_due_to_conflicting_rules)
                                .addArg("property", property)
                                .addArg("type", errorInfo.types.toArray())
                        ;
                    } else
                        errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.must_not_be_specified)
                                .addArg("property", property)
                                .addArg("type", errorInfo.types.toArray())
                                ;
                }
            }

            if (errorInfo.minimum != null || errorInfo.exclusiveMinimum != null || errorInfo.maximum != null || errorInfo.exclusiveMaximum != null) {

                BigDecimal minimum = Value.max(errorInfo.exclusiveMinimum, errorInfo.minimum);
                boolean minimumExclusive = minimum != null && minimum == errorInfo.exclusiveMinimum;
                BigDecimal maximum = Value.min(errorInfo.exclusiveMaximum, errorInfo.maximum);
                boolean maximumExclusive = maximum != null && maximum == errorInfo.exclusiveMaximum;

                String messageId = null;
                if (minimum != null && maximum != null) {
                    if (!minimumExclusive && !maximumExclusive)
                        messageId = R.string.number_must_be_gteq_lteq;
                    else if (minimumExclusive && !maximumExclusive)
                        messageId = R.string.number_must_be_gt_lteq;
                    else if (!minimumExclusive && maximumExclusive)
                        messageId = R.string.number_must_be_gteq_lt;
                    else
                        messageId = R.string.number_must_be_gt_lt;
                } else if (minimum != null) {
                    if (!minimumExclusive)
                        messageId = R.string.number_must_be_gteq;
                    else
                        messageId = R.string.number_must_be_gt;
                } else if (maximum != null) {
                    if (!maximumExclusive)
                        messageId = R.string.number_must_be_lteq;
                    else
                        messageId = R.string.number_must_be_lt;
                }

                errorDescriptor = new ErrorDescriptor(ErrorClass.OUT_OF_RANGE, messageId)
                        .addArg("property", property)
                ;

                if (minimum != null)
                    errorDescriptor.addArg(minimumExclusive ? "exclusiveMinimum" : "minimum", minimum);
                if (maximum != null)
                    errorDescriptor.addArg(maximumExclusive ? "exclusiveMaximum" : "maximum", maximum);
            }
            if (errorInfo.multipleOf != null) {
                errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.value_must_be_an_integer_multiple_of)
                        .addArg("property", property)
                        .addArg("multipleOf", errorInfo.multipleOf);
            }
            if (errorInfo._const != null) {
                if (errorInfo._const.size() == 1) {
                    errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.must_be_eq_to)
                            .addArg("property", property)
                            .addArg("const", errorInfo._const.iterator().next())
                    ;
                } else if (errorInfo._const.size() >= 2) {
                    errorDescriptor = new ErrorDescriptor(ErrorClass.INVALID_VALUE, R.string.must_be_eq_one_of)
                            .addArg("property", property)
                            .addArg("enum", errorInfo._const.toArray())
                    ;
                } else
                    throw new IllegalArgumentException("empty constant set");
            }

            if (errorDescriptor != null)
                descriptorSet.add(errorDescriptor);

            for (SchemaViolation uncoalescedError : errorInfo.uncoalescedErrors) {
                if (uncoalescedError.getMessageKey() != null)
                    descriptorSet.add(new ErrorDescriptor(ErrorClass.SCHEMA_VIOLATION, uncoalescedError.getMessageKey(), uncoalescedError.getMessageArgs()));
                else {
                    descriptorSet.add(new ErrorDescriptor(ErrorClass.SCHEMA_VIOLATION, null).nonLocalizedMessage(fallbackMessageFactory.createMessage(uncoalescedError).toString()));
                }
            }
        }
    }

}
