package io.zerocopy.json.validator;

import com.fasterxml.jackson.core.JsonPointer;
import io.zerocopy.json.core.values.Value;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SchemaViolation {

    public enum Severity {
        ERROR,
        WARNING,
        INFO
    }

    private String property;
    private JsonSchemaV7.Rule rule;
    private Severity severity = Severity.ERROR;
    private String messageKey;
    private Map<String, Object> messageArgs;
    private String nonLocalizedMessage;

    private String schemaRef;
    private String rulePointer;

    public SchemaViolation() {
    }

    /**
     * @param property            the violating property
     * @param schemaRef           the involved schema element
     * @param rule                the violated rule
     * @param severity            the severity of this violation
     * @param messageKey          a resource key
     * @param nonLocalizedMessage non-localized default message
     */
    public SchemaViolation(List<String> property, String schemaRef, JsonSchemaV7.Rule rule, Severity severity, String messageKey, String nonLocalizedMessage) {
        this();
        this.property = JsonPointerUtils.implode(property);
        this.rule = rule;
        this.severity = severity;
        this.messageKey = messageKey;
        this.nonLocalizedMessage = nonLocalizedMessage;

        this.schemaRef = schemaRef;
        this.rulePointer = rule.pointer;
    }

    /**
     * @param property   the violating property
     * @param schemaRef  the involved schema element
     * @param rule       the violated rule
     * @param messageKey a resource key
     */
    public SchemaViolation(List<String> property, String schemaRef, JsonSchemaV7.Rule rule, String messageKey) {
        this(property, schemaRef, rule, Severity.ERROR, messageKey, null);
    }

    public SchemaViolation(List<String> property, JsonPointer pointer, JsonSchemaV7.Rule rule, String messageKey) {
        this(property, pointer.toString(), rule, Severity.ERROR, messageKey, null);
    }

    public SchemaViolation putRawMessageArg(String key, Object value) {
        if (this.messageArgs == null)
            this.messageArgs = new TreeMap<String, Object>();
        this.messageArgs.put(key, value);
        return this;
    }

    public SchemaViolation putMessageArg(String key, String value) {
        return putRawMessageArg(key, value);
    }

    public SchemaViolation putMessageArg(String key, Number value) {
        return putRawMessageArg(key, value);
    }

    public SchemaViolation putMessageArg(String key, Boolean value) {
        return putRawMessageArg(key, value);
    }

    public SchemaViolation putMessageArg(String key, JsonSchemaV7.Type[] value) {
        String[] strings = new String[value.length];
        for (int i = 0; i < value.length; i++) {
            JsonSchemaV7.Type type = value[i];
            strings[i] = type.value;
        }
        return putRawMessageArg(key, strings);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public JsonSchemaV7.Rule getRule() {
        return rule;
    }

    public void setRule(JsonSchemaV7.Rule rule) {
        this.rule = rule;
    }

    public Map<String, Object> getMessageArgs() {
        return messageArgs;
    }

    public void setMessageArgs(Map<String, Object> messageArgs) {
        this.messageArgs = messageArgs;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getNonLocalizedMessage() {
        return nonLocalizedMessage;
    }

    public void setNonLocalizedMessage(String nonLocalizedMessage) {
        this.nonLocalizedMessage = nonLocalizedMessage;
    }

    public String getSchemaRef() {
        return schemaRef;
    }

    public void setSchemaRef(String schemaRef) {
        this.schemaRef = schemaRef;
    }

    public String getRulePointer() {
        return rulePointer;
    }

    public void setRulePointer(String rulePointer) {
        this.rulePointer = rulePointer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchemaViolation that = (SchemaViolation) o;
        return Value.equals(property, that.property) &&
                rule == that.rule &&
                severity == that.severity &&
                Value.equals(messageKey, that.messageKey) &&
                Value.equals(messageArgs, that.messageArgs) &&
                Value.equals(nonLocalizedMessage, that.nonLocalizedMessage) &&
                Value.equals(schemaRef, that.schemaRef) &&
                Value.equals(rulePointer, that.rulePointer);
    }

    @Override
    public int hashCode() {
        return Value.hash(property, rule, severity, messageKey, messageArgs, nonLocalizedMessage, schemaRef, rulePointer);
    }
}
