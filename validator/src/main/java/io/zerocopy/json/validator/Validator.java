package io.zerocopy.json.validator;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.zerocopy.json.core.encoding.Base64Coder;
import io.zerocopy.json.schema.JsonSchema;
import io.zerocopy.json.schema.JsonSchemaV7;
import io.zerocopy.json.schema.SchemaException;
import io.zerocopy.json.schema.SchemaProblem;
import io.zerocopy.json.schema.context.SchemaResolutionContext;
import io.zerocopy.json.schema.document.SchemaReference;
import io.zerocopy.json.schema.loader.DocumentLoader;
import io.zerocopy.json.schema.pointer.JsonPointerUtils;
import io.zerocopy.json.validator.messages.R;

import javax.script.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Note: This class is <i>not</i> thread safe.
 */
public class Validator {

    private boolean write = false;
    private boolean addIndividualErrorsFromAllOf = true;

    private LinkedList<String> path = new LinkedList<String>();
    private SchemaResolutionContext schemaContext = new SchemaResolutionContext();
    private boolean required;

    public JsonSchema.Version getDefaultSchemaVersion() {
        return schemaContext.getDefaultSchemaVersion();
    }

    public void setDefaultSchemaVersion(JsonSchema.Version defaultSchemaVersion) {
        this.schemaContext.setDefaultSchemaVersion(defaultSchemaVersion);
    }

    public DocumentLoader getDocumentLoader() {
        return schemaContext.getDocumentLoader();
    }

    public void setDocumentLoader(DocumentLoader documentLoader) {
        this.schemaContext.setDocumentLoader(documentLoader);
    }

    public JsonNode validate(List<SchemaViolation> violationsOut, SchemaReference schemaReference, JsonNode value) throws IOException {
        if (schemaReference == null)
            throw new NullPointerException("schemaReference is null");
        if (schemaReference.getRoot() == null)
            throw new IllegalArgumentException("schemaReference.root is null");
        if (schemaReference.getSchema() == null)
            throw new IllegalArgumentException("schemaReference.schema is null");
        if (write)
            value = value.deepCopy();
        path.clear();
        schemaContext.clear();
        return performValidation(violationsOut, schemaReference, value);
    }

    public JsonNode validate(ValidatorCallback validatorCallback, SchemaReference schemaReference, JsonNode value) throws IOException {
        if (schemaReference == null)
            throw new NullPointerException("schemaReference is null");
        if (schemaReference.getRoot() == null)
            throw new IllegalArgumentException("schemaReference.root is null");
        if (schemaReference.getSchema() == null)
            throw new IllegalArgumentException("schemaReference.schema is null");
        if (write)
            value = value.deepCopy();
        path.clear();
        schemaContext.clear();
        return performValidation(validatorCallback, null, null, schemaReference, value);
    }

    private JsonNode performValidation(final List<SchemaViolation> violationsOut, final SchemaReference schemaReference, JsonNode value) throws IOException {
        return performValidation(violationsOut, new HashSet<Integer>(), new HashSet<String>(), schemaReference, value);
    }

    private JsonNode performValidation(final List<SchemaViolation> violationsOut, Set<Integer> evaluatedItemsOut, Set<String> evaluatedPropertiesOut, final SchemaReference schemaReference, JsonNode value) throws IOException {
        if (write)
            value = value.deepCopy();
        return performValidation(new ValidatorCallback() {
            @Override
            public void onPathPushed(List<String> path, int flags) {
            }

            @Override
            public void onViolation(SchemaViolation violation) {
                violationsOut.add(violation);
            }
        }, evaluatedItemsOut, evaluatedPropertiesOut, schemaReference, value);
    }

    private JsonNode validateObject(ValidatorCallback validatorCallback, Set<Integer> evaluatedItemsOut, Set<String> evaluatedPropertiesOut, final SchemaReference schemaReference, JsonSchemaV7 schema, JsonNode value) throws IOException {
        Set<String> evaluatedKeys = new HashSet<String>();
        Boolean evaluatedKeysAllExcept = false;

        ObjectNode objectNode = (ObjectNode) value;

        boolean addedImplicitly = false;

        if (schema.maxProperties != null && !(value.size() <= schema.maxProperties))
            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MAX_PROPERTIES, R.string.must_have_at_most_n_properties)
                    .putMessageArg("maxProperties", schema.maxProperties));

        if (schema.minProperties != null && !(value.size() >= schema.minProperties))
            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MIN_PROPERTIES, R.string.must_have_at_least_n_properties)
                    .putMessageArg("minProperties", schema.minProperties));

        Set<String> disallowedProperties = null;
        Set<String> remainingRequiredProperties = null;

        if (schema.required != null) {
            remainingRequiredProperties = new LinkedHashSet<String>();
            for (JsonNode required : schema.required)
                remainingRequiredProperties.add(required.asText());
        }


        boolean additionalPropertiesAllowed = true;
        SchemaReference additionalPropertySchema = null;
        if (schema.additionalProperties != null)
            if (schema.additionalProperties.isObject())
                additionalPropertySchema = schemaReference.createRelative("/additionalProperties", schema.additionalProperties);
            else if (schema.additionalProperties.isBoolean()) {
                additionalPropertiesAllowed = schema.additionalProperties.asBoolean();
                additionalPropertySchema = schemaReference.createRelative("/additionalProperties", schema.additionalProperties);
            } else
                throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "additionalProperties must be a boolean or a schema, got " + schema.additionalProperties.getClass());

        for (Iterator<Map.Entry<String, JsonNode>> fieldIterator = objectNode.fields(); fieldIterator.hasNext(); ) {
            Map.Entry<String, JsonNode> field = fieldIterator.next();

            String propertyName = field.getKey();
            JsonNode propertyValue = field.getValue();

            if (schema.dependencies != null) {
                JsonNode dependency = schema.dependencies.get(propertyName);
                if (dependency != null) {
                    if (dependency.isArray()) {
                        for (JsonNode dependencyProperty : dependency) {
                            if (dependencyProperty.isTextual()) {
                                String dependencyPropertyName = dependencyProperty.asText();
                                if (!value.has(dependencyPropertyName))
                                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.DEPENDENCY, R.string.depends_on_property)
                                            .putMessageArg("property", propertyName)
                                            .putMessageArg("dependencyProperty", dependencyPropertyName)
                                    );
                            } else
                                throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "dependency array elements must be strings");
                        }
                    } else if (dependency.isObject() || dependency.isBoolean()) {
                        value = performValidation(validatorCallback, evaluatedItemsOut, evaluatedPropertiesOut, schemaReference.createRelative(JsonPointerUtils.create("dependencies", propertyName), dependency), value);
                    } else {
                        throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "dependencies must contain arrays with property name or schemas (objects)");
                    }
                }
            }

            if (schema.dependentRequired != null) {
                JsonNode dependency = schema.dependentRequired.get(propertyName);
                if (dependency != null) {
                    if (dependency.isArray()) {
                        for (JsonNode dependencyProperty : dependency) {
                            if (dependencyProperty.isTextual()) {
                                String dependencyPropertyName = dependencyProperty.asText();
                                if (!value.has(dependencyPropertyName))
                                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.DEPENDENCY, R.string.depends_on_property)
                                            .putMessageArg("property", propertyName)
                                            .putMessageArg("dependencyProperty", dependencyPropertyName)
                                    );
                            } else
                                throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "dependency array elements must be strings");
                        }
                    } else {
                        throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "dependentRequired must contain array properties with property names");
                    }
                }
            }

            if (schema.dependentSchemas != null) {
                JsonNode dependentSchema = schema.dependentSchemas.get(propertyName);
                if (dependentSchema != null) {
                    if (dependentSchema.isObject() || dependentSchema.isBoolean()) {
                        value = performValidation(validatorCallback, evaluatedItemsOut, evaluatedPropertiesOut, schemaReference.createRelative("/dependentSchema", dependentSchema), value);
                    } else {
                        throw new SchemaException(null, SchemaProblem.TYPE_ERROR, "dependentSchemas must contain properties with schemas");
                    }
                }
            }

            path.addLast(propertyName);
            try {

                if (schema.patternProperties != null && schema.patternProperties.size() != 0) {
                    int foundRules = 0;
                    int matchedRules = 0;
                    for (Map.Entry<String, JsonNode> patternPropertyEntry : schema.patternProperties.entrySet()) {
                        String propertyNamePattern = patternPropertyEntry.getKey();
                        JsonNode propertySchema = patternPropertyEntry.getValue();
                        if (matchesPattern(propertyName, propertyNamePattern)) {
                            ++foundRules;
                            List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
                            propertyValue = performValidation(violations, evaluatedItemsOut, evaluatedPropertiesOut, schemaReference.createRelative(JsonPointerUtils.create("patternProperties", patternPropertyEntry.getKey()), propertySchema), propertyValue);
                            evaluatedKeys.add(propertyName);
                            if (violations.isEmpty())
                                ++matchedRules;
                        }
                    }
                    if (foundRules > 0)
                        addedImplicitly = true;
                    if (matchedRules != foundRules)
                        validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.PATTERN_PROPERTIES, R.string.must_match_all_of_the_schemas_with_a_matching_pattern)
                                /*.putMessageArg("patternProperties", schema.patternProperties)*/);
                }

                if (schema.propertyNames != null) {
//                            if (schema.propertyNames.getNodeType() != JsonNodeType.STRING)
//                                throw new SchemaException("propertyNames must be a string schema");

                    List<SchemaViolation> propertyNameViolations = new ArrayList<SchemaViolation>();
                    // TODO how is "default" handled here? Since property are always a string, a default value can be ignored.
                    performValidation(propertyNameViolations, schemaReference.createRelative("/propertyNames", schema.propertyNames), JsonNodeFactory.instance.textNode(propertyName));
                    evaluatedKeys.add(propertyName);
                    if (!propertyNameViolations.isEmpty())
                        validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.PROPERTY_NAMES, R.string.name_must_match_all_of_the_schemas_with_a_matching_pattern)
                                /*.putMessageArg("propertyNames", schema.propertyNames)*/);
                }

                SchemaReference propertySchema = null;
                if (schema.properties != null) {
                    JsonNode propertySchemaNode = schema.properties.get(propertyName);
                    if (propertySchemaNode != null)
                        propertySchema = schemaReference.createRelative(JsonPointerUtils.create("properties", propertyName), propertySchemaNode);
                }
                if (propertySchema == null && !addedImplicitly)
                    propertySchema = additionalPropertySchema;

                required = remainingRequiredProperties != null && remainingRequiredProperties.remove(propertyName);

                validatorCallback.onPathPushed(path, required ? ValidatorCallback.FLAG_REQUIRED : 0);

                if (propertySchema != null) {
                    propertyValue = performValidation(validatorCallback, evaluatedItemsOut, evaluatedPropertiesOut, propertySchema, propertyValue);
                    evaluatedKeys.add(propertyName);
                } else if (!(additionalPropertiesAllowed || required || addedImplicitly)) {
                    if (disallowedProperties == null)
                        disallowedProperties = new LinkedHashSet<String>();
                    disallowedProperties.add(propertyName);
                }

                if (write)
                    field.setValue(propertyValue);
                required = false;
            } finally {
                path.removeLast();
            }
        }

        if (remainingRequiredProperties != null)
            for (String propertyName : remainingRequiredProperties) {
                path.addLast(propertyName);

                validatorCallback.onPathPushed(path, ValidatorCallback.FLAG_REQUIRED);

                validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.REQUIRED, R.string.must_be_specified)
                        .putMessageArg("property", propertyName));
                path.removeLast();
            }

        if (disallowedProperties != null)
            for (String propertyName : disallowedProperties) {
                path.addLast(propertyName);
                validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.ADDITIONAL_PROPERTIES, R.string.must_not_be_specified)
                        .putMessageArg("property", propertyName));
                path.removeLast();
            }

        if (evaluatedPropertiesOut != null) {
            if (evaluatedKeysAllExcept) {
                Iterator<String> keyIt = objectNode.fieldNames();
                while (keyIt.hasNext()) {
                    String k = keyIt.next();
                    if (!evaluatedKeys.contains(k))
                        evaluatedPropertiesOut.add(k);
                }
            } else {
                evaluatedPropertiesOut.addAll(evaluatedKeys);
            }
        }

        return value;
    }

    private JsonNode validateArray(ValidatorCallback validatorCallback, Set<Integer> evaluatedItemsOut, Set<String> evaluatedPropertiesOut, final SchemaReference schemaReference, JsonSchemaV7 schema, JsonNode value) throws IOException {
        Set<Integer> evaluatedIndices = new HashSet<Integer>();
        Boolean evaluatedIndicesAllExcept = false;

        if (schema.minItems != null && !(value.size() >= schema.minItems))
            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MIN_ITEMS, R.string.array_min_items)
                    .putMessageArg("minItems", schema.minItems));

        if (schema.maxItems != null && !(value.size() <= schema.maxItems))
            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MAX_ITEMS, R.string.array_max_items)
                    .putMessageArg("maxItems", schema.maxItems));

        if (schema.items != null && schema.items.getNodeType() != JsonNodeType.NULL) {
            evaluatedIndicesAllExcept = true;
            if (schema.items.isObject()) {
                JsonNode itemSchema = schema.items;
                for (int i = 0; i < value.size(); ++i) {
                    JsonNode itemValue = value.get(i);

                    //recurse
                    path.addLast(Integer.toString(i));
                    itemValue = performValidation(validatorCallback, evaluatedItemsOut, evaluatedPropertiesOut, schemaReference.createRelative("/items", itemSchema), itemValue);
                    if (write)
                        ((ArrayNode) value).set(i, itemValue);
                    path.removeLast();
                }
            } else if (schema.items.isArray()) {
                for (int i = 0; i < value.size(); ++i) {
                    SchemaReference itemSchema;
                    if (i < schema.items.size())
                        itemSchema = schemaReference.createRelative("/items/" + i, schema.items.get(i));
                    else if (schema.additionalItems != null)
                        itemSchema = schemaReference.createRelative("/additionalItems", schema.additionalItems);
                    else
                        itemSchema = null;

                    if (itemSchema != null) {
                        JsonNode itemValue = value.get(i);

                        //recurse
                        path.addLast(Integer.toString(i));
                        performValidation(validatorCallback, evaluatedItemsOut, evaluatedPropertiesOut, itemSchema, itemValue);
                        if (write)
                            ((ArrayNode) value).set(i, itemValue);
                        path.removeLast();
                    } else {
                        evaluatedIndices.add(i);
                    }
                }
            } else if (schema.items.isBoolean()) {
                if (!schema.items.asBoolean() && (value.size() > 0))
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.ITEMS, R.string.array_must_be_empty)
                            /*.putMessageArg("items", schema.items)*/);
            }
        }

        if (schema.contains != null) {
            evaluatedIndicesAllExcept = true;
            boolean haveMinOrMaxContains = schema.minContains != null || schema.maxContains != null;
            int matchingItems = 0;
            List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
            for (JsonNode arrayItem : value) {
                //TODO should a default value be assigned here? If so, to any item or only to the matching one?
                performValidation(violations, schemaReference.createRelative("/contains", schema.contains), arrayItem);
                if (violations.isEmpty()) {
                    ++matchingItems;
                    if (!haveMinOrMaxContains)//for performance
                        break;
                }
                violations.clear();
            }

            //TODO fix error attributes

            if (haveMinOrMaxContains) {
                if (schema.minContains != null && matchingItems < schema.minContains)
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MIN_CONTAINS, R.string.must_not_match_the_specified_schema));

                if (schema.maxContains != null && matchingItems > schema.maxContains)
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MAX_CONTAINS, R.string.must_not_match_the_specified_schema));

                if (schema.minContains == null && matchingItems == 0)
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.CONTAINS, R.string.must_not_match_the_specified_schema));
            } else {
                if (matchingItems == 0)
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.CONTAINS, R.string.must_not_match_the_specified_schema));
            }
        }

        if (schema.uniqueItems != null && schema.uniqueItems) {
            Set<JsonNode> uniqueValues = new HashSet<JsonNode>();
            boolean containsUniqueItems = true;
            for (JsonNode arrayItem : value) {
                if (!uniqueValues.add(arrayItem)) {
                    containsUniqueItems = false;
                    break;
                }
            }
            if (!containsUniqueItems)
                validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.UNIQUE_ITEMS, R.string.array_must_contain_unique_items));
        }

        if (evaluatedItemsOut != null) {
            if (evaluatedIndicesAllExcept) {
                for (int i = 0; i < value.size(); ++i)
                    if (!evaluatedIndices.contains(i))
                        evaluatedItemsOut.add(i);
            } else {
                evaluatedItemsOut.addAll(evaluatedIndices);
            }
        }

        return value;
    }

    private JsonNode performValidation(ValidatorCallback validatorCallback, Set<Integer> evaluatedItemsOut, Set<String> evaluatedPropertiesOut, SchemaReference schemaReference, JsonNode value) throws IOException {
        validatorCallback.onPathPushed(Collections.unmodifiableList(path), 0);

        Set<Integer> evaluatedItemsLocal = new HashSet<Integer>();
        Set<String> evaluatedPropertiesLocal = new HashSet<String>();

        JsonSchemaV7 schema = schemaContext.pushSchemaNode(null, schemaReference, true);
        try {
            boolean continueValidation = true;

            String ref = schema.ref;
            if (ref != null) {
                SchemaReference referencedSchema;

                try {
                    referencedSchema = schemaContext.resolveSchema(schemaReference, ref);
                } catch (SchemaException e) {
                    if (e.getError() == SchemaProblem.RECURSION)
                        referencedSchema = e.getLocation();
                    else
                        throw e;
                }

                if (referencedSchema != null) {
                    if (referencedSchema.getSchema() == schemaReference.getSchema())
                        throw new SchemaException(null, SchemaProblem.IMMEDIATE_RECURSION, "schema references itself");
                    else {

                        if (referencedSchema.getRoot() != schemaReference.getRoot()) {
                            value = performValidation(validatorCallback, evaluatedItemsLocal, evaluatedPropertiesLocal, referencedSchema, value);
                        } else {
                            List<String> refPath = JsonPointerUtils.split(referencedSchema.getSchemaPointer().toString());
                            List<String> nodesToPush = !refPath.isEmpty() ? refPath.subList(0, refPath.size() - 1) : Collections.<String>emptyList();

                            Deque<SchemaReference> pushedNodes = new ArrayDeque<SchemaReference>(nodesToPush.size());
                            {
                                List<String> nodePath = new ArrayList<String>();
                                JsonNode nodeInPath = schemaReference.getRoot();
                                for (String refPathElem : nodesToPush) {
                                    nodeInPath = nodeInPath.at("/" + refPathElem);
                                    nodePath.add(refPathElem);
                                    SchemaReference refSchemaReference = SchemaReference.create(schemaReference.getDocument(), JsonPointerUtils.implode(nodePath), nodeInPath);
                                    schemaContext.pushSchemaNode(null, refSchemaReference, false);
                                    pushedNodes.push(refSchemaReference);
                                }
                            }
                            try {
                                value = performValidation(validatorCallback, evaluatedItemsLocal, evaluatedPropertiesLocal, referencedSchema, value);
                            } finally {
                                while (!pushedNodes.isEmpty())
                                    schemaContext.popSchemaNode(pushedNodes.pop());
                            }
                        }
                    }
                    continueValidation = false;
                } else {
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.REF, R.string.unresolved_ref)
                            .putMessageArg("ref", schema.ref));
                    continueValidation = false;
                }
            }

            if (value == null)
                value = schema._default;

            JsonSchemaV7.Type valueType;

            switch (value.getNodeType()) {
                case ARRAY:
                    valueType = JsonSchemaV7.Type.ARRAY;
                    break;
                case BOOLEAN:
                    valueType = JsonSchemaV7.Type.BOOLEAN;
                    break;
                case NUMBER:
                    if (value.isIntegralNumber())
                        valueType = JsonSchemaV7.Type.INTEGER;
                    else if (value.isFloatingPointNumber()) {
                        valueType = JsonSchemaV7.Type.NUMBER;
                        if (schema.numberWithZeroFractionAsInteger) {
                            BigDecimal bigDecimal = value.decimalValue();
                            BigDecimal fraction = bigDecimal.remainder(BigDecimal.ONE);
                            if (fraction.compareTo(BigDecimal.ZERO) == 0)
                                valueType = JsonSchemaV7.Type.INTEGER;
                        }
                    } else
                        throw new IllegalStateException("unable to determine number type");
                    break;
                case OBJECT:
                    valueType = JsonSchemaV7.Type.OBJECT;
                    break;
                case STRING:
                    valueType = JsonSchemaV7.Type.STRING;
                    break;
                case NULL:
                    valueType = JsonSchemaV7.Type.NULL;
                    break;
                case MISSING:
                case BINARY:
                case POJO:
                default:
                    throw new IllegalArgumentException("unimplemented node type: " + value.getNodeType());
            }

            if (continueValidation) {

                if (schema.type != null && !matchesAnyTypeOf(valueType, schema.type)) {
                    if (schema.type.length == 1)
                        validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.TYPE, R.string.must_be_of_type)
                                .putMessageArg("type", schema.type));
                    else
                        validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.TYPE, R.string.must_be_of_one_of_the_following_types)
                                .putMessageArg("type", schema.type));
                    //TODO handle zero length type array
                    return value;
                }

                switch (valueType) {
                    case OBJECT:
                        validateObject(validatorCallback, evaluatedItemsLocal, evaluatedPropertiesLocal, schemaReference, schema, value);
                        break;
                    case ARRAY:
                        validateArray(validatorCallback, evaluatedItemsLocal, evaluatedPropertiesLocal, schemaReference, schema, value);
                        break;
                    case STRING:
                        String string = value.asText();

                        if (schema.contentEncoding != null) {
                            if (schema.contentEncoding.equals("base64")) {
                                try {
                                    string = Base64Coder.decodeString(string);
                                } catch (IllegalArgumentException e) {
                                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.CONTENT_ENCODING, R.string.must_be_a_base64_string)
                                            .putMessageArg("contentEncoding", schema.contentEncoding));
                                }
                            } else
                                throw new SchemaException(null, SchemaProblem.UNSUPPORTED_ENCODING, "unsupported contentEncoding: " + schema.contentEncoding);
                        }

                        if (schema.contentMediaType != null) {
                            if (schema.contentMediaType.equals("application/json")) {
                                try {
                                    schemaContext.getObjectMapper().readTree(string);
                                } catch (JsonParseException e) {
                                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.CONTENT_MEDIA_TYPE, R.string.must_be_a_json_document)
                                            .putMessageArg("contentMediaType", schema.contentMediaType));
                                }
                            } else
                                throw new SchemaException(null, SchemaProblem.UNSUPPORTED_MEDIATYPE, "unsupported contentMediaType: " + schema.contentMediaType);
                        }

                        if (schema.format != null) {
                            if (schema.format.dateTimeConverter != null) {
                                Date date = null;
                                try {
                                    date = schema.format.dateTimeConverter.parse(string);
                                } catch (Exception ignored) {
                                }
                                if (date == null)
                                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.FORMAT, R.string.must_match_the_format)
                                            .putMessageArg("format", schema.format.value)
                                            .putMessageArg("format.specification", "RFC3339")
                                    );
                            }

                            //using the native regex implementation for internal non-schema patterns
                            if (schema.format.matcher != null && !schema.format.matcher.matches(string)) {
                                validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.FORMAT, R.string.must_match_the_format)
                                        .putMessageArg("format", schema.format.value)
                                        .putMessageArg("format.pattern", schema.format.name())
                                );
                            }

                            switch (schema.format) {
                                case IDN_HOSTNAME:
                                case IRI:
                                case IRI_REFERENCE:
                                    throw new SchemaException(null, SchemaProblem.UNSUPPORTED_FORMAT, "unimplemented format: " + schema.format.value);
                                case REGEX:
                                    try {
                                        Pattern.compile(string, 0);
                                    } catch (PatternSyntaxException e) {
                                        validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.FORMAT, R.string.must_be_a_valid_regular_expression));
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (!matchesPattern(string, schema.pattern))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.PATTERN, R.string.string_must_match)
                                    .putMessageArg("pattern", schema.pattern));

                        if (schema.minLength != null && !(string.codePointCount(0, string.length()) >= schema.minLength))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MIN_LENGTH, R.string.string_min_length)
                                    .putMessageArg("minLength", schema.minLength));

                        if (schema.maxLength != null && !(string.codePointCount(0, string.length()) <= schema.maxLength))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MAX_LENGTH, R.string.string_max_length)
                                    .putMessageArg("maxLength", schema.maxLength));
                        break;
                    case NUMBER:
                    case INTEGER: {
                        BigDecimal number = value.decimalValue();

                        if (schema.minimum != null && !(number.compareTo(schema.minimum) >= 0))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MINIMUM, R.string.number_must_be_gteq)
                                    .putMessageArg("minimum", schema.minimum));

                        if (schema.exclusiveMinimum != null && !(number.compareTo(schema.exclusiveMinimum) > 0))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.EXCLUSIVE_MINIMUM, R.string.number_must_be_gt)
                                    .putMessageArg("exclusiveMinimum", schema.exclusiveMinimum));

                        if (schema.maximum != null && !(number.compareTo(schema.maximum) <= 0))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MAXIMUM, R.string.number_must_be_lteq)
                                    .putMessageArg("maximum", schema.maximum));

                        if (schema.exclusiveMaximum != null && !(number.compareTo(schema.exclusiveMaximum) < 0))
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.EXCLUSIVE_MAXIMUM, R.string.number_must_be_lt)
                                    .putMessageArg("exclusiveMaximum", schema.exclusiveMaximum));

                        if (schema.multipleOf != null && number.remainder(schema.multipleOf).compareTo(BigDecimal.ZERO) != 0)
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.MULTIPLE_OF, R.string.value_must_be_an_integer_multiple_of)
                                    .putMessageArg("multipleOf", schema.multipleOf));
                    }
                    break;
                    case BOOLEAN:
                    case NULL:
                        break;
                    default:
                        throw new IOException("unimplemented type: " + valueType);
                }

                if (schema.booleanValidity != null && schema.booleanValidity != (value != null && !value.isMissingNode()))
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.BOOLEAN_SCHEMA, R.string.must_not_be_specified)
                            .putMessageArg("booleanValidity", schema.booleanValidity));

                if (schema._const != null && !enumNodeEquals(value, schema._const))
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.CONST, R.string.must_be_eq_to)
                            .putRawMessageArg("const", createMessageArg(schema._const)));

                if (schema._enum != null && !enumContains(schema._enum, value))
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.ENUM, R.string.must_be_eq_one_of)
                            .putRawMessageArg("enum", createMessageArg(schema._enum)));

                if (schema.allOf != null) {
                    List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
                    AtomicReference<JsonNode> valueRef = new AtomicReference<JsonNode>(value);
                    if (!matchAll(violations, evaluatedItemsLocal, evaluatedPropertiesLocal, schemaReference, valueRef, schema.allOf)) {
                        if (addIndividualErrorsFromAllOf) {
                            for (SchemaViolation violation : violations)
                                validatorCallback.onViolation(violation);
                        } else
                            validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.ALL_OF, R.string.must_match_all_of_the_specified_schemas));
                    }
                    if (write)
                        value = valueRef.get();
                }

                //TODO should a default value be applied here?
                if (schema.anyOf != null && countMatches(schemaReference, evaluatedItemsLocal, evaluatedPropertiesLocal, value, schema.anyOf, false) == 0) {
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.ANY_OF, R.string.must_match_any_of_the_specified_schemas));
                }

                //TODO should a default value be applied here?
                if (schema.oneOf != null && countMatches(schemaReference, evaluatedItemsLocal, evaluatedPropertiesLocal, value, schema.oneOf, false) != 1)
                    validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.ONE_OF, R.string.must_match_exactly_one_of_the_specified_schemas));

                if (schema.not != null) {
                    List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
                    //TODO should a default value be applied here?
                    performValidation(violations, schemaReference.createRelative("/not", schema.not), value);
                    if (violations.isEmpty())
                        validatorCallback.onViolation(new SchemaViolation(path, schemaContext.pointer(), JsonSchemaV7.Rule.NOT, R.string.must_not_match_the_specified_schema));
                }

                if (schema._if != null && (schema._then != null || schema._else != null)) {
                    //TODO should the value of the "if" schema really be applied here? This is left to the schema designer for now.
                    List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
                    {
                        Set<Integer> evaluatedItems = new HashSet<Integer>();
                        Set<String> evaluatedProperties = new HashSet<String>();
                        value = performValidation(violations, evaluatedItems, evaluatedProperties, schemaReference.createRelative("/if", schema._if), value);
                        if (violations.isEmpty()) {
                            if (evaluatedItemsLocal != null)
                                evaluatedItemsLocal.addAll(evaluatedItems);
                            if (evaluatedPropertiesLocal != null)
                                evaluatedPropertiesLocal.addAll(evaluatedProperties);
                        }
                    }
                    if (violations.isEmpty()) {
                        if (schema._then != null)
                            value = performValidation(validatorCallback, evaluatedItemsLocal, evaluatedPropertiesLocal, schemaReference.createRelative("/then", schema._then), value);
                    } else {
                        if (schema._else != null)
                            value = performValidation(validatorCallback, evaluatedItemsLocal, evaluatedPropertiesLocal, schemaReference.createRelative("/else", schema._else), value);
                    }
                }
            }

            if (valueType == JsonSchemaV7.Type.OBJECT) {
                if (schema.unevaluatedProperties != null) {
                    Iterator<String> keyIt = value.fieldNames();
                    while (keyIt.hasNext()) {
                        String k = keyIt.next();
                        if (!evaluatedPropertiesLocal.contains(k)) {
                            List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
                            Set<Integer> evaluatedItems = new HashSet<Integer>();
                            Set<String> evaluatedProperties = new HashSet<String>();

                            performValidation(violations, evaluatedItems, evaluatedProperties, schemaReference.createRelative("/unevaluatedProperties", schema.unevaluatedProperties), value.get(k));

                            if (violations.isEmpty()) {
                                if (evaluatedItemsLocal != null)
                                    evaluatedItemsLocal.addAll(evaluatedItems);
                                if (evaluatedPropertiesLocal != null)
                                    evaluatedPropertiesLocal.addAll(evaluatedProperties);

                                if (evaluatedPropertiesLocal != null)
                                    evaluatedPropertiesLocal.add(k);
                            } else {
                                for (SchemaViolation violation : violations)
                                    validatorCallback.onViolation(violation);
                            }
                        }
                    }
                }
            }

            if (valueType == JsonSchemaV7.Type.ARRAY) {
                if (schema.unevaluatedItems != null) {
                    for (int i = 0; i < value.size(); ++i)
                        if (!evaluatedItemsLocal.contains(i)) {
                            List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
                            Set<Integer> evaluatedItems = new HashSet<Integer>();
                            Set<String> evaluatedProperties = new HashSet<String>();

                            performValidation(violations, evaluatedItems, evaluatedProperties, schemaReference.createRelative("/unevaluatedItems", schema.unevaluatedItems), value.get(i));

                            if (violations.isEmpty()) {
                                if (evaluatedItemsLocal != null)
                                    evaluatedItemsLocal.addAll(evaluatedItems);
                                if (evaluatedPropertiesLocal != null)
                                    evaluatedPropertiesLocal.addAll(evaluatedProperties);

                                if (evaluatedItemsLocal != null)
                                    evaluatedItemsLocal.add(i);
                            } else {
                                for (SchemaViolation violation : violations)
                                    validatorCallback.onViolation(violation);
                            }
                        }
                }
            }
        } finally {
            if (evaluatedItemsOut != null)
                evaluatedItemsOut.addAll(evaluatedItemsLocal);
            if (evaluatedPropertiesOut != null)
                evaluatedPropertiesOut.addAll(evaluatedPropertiesLocal);

            schemaContext.popSchemaNode(schemaReference.getRoot(), (String) null, schemaReference.getSchema());
        }

        return value;
    }

    private ScriptEngine engine;
    private ScriptContext scriptContext;
    private CompiledScript compiledScript;
    private Object regexMatch;

    private static final boolean USE_JAVASCRIPT_REGEXP = true;

    {
        if (USE_JAVASCRIPT_REGEXP)
            try {
                engine = new ScriptEngineManager().getEngineByName("nashorn");
                if (engine != null) {
                    scriptContext = new SimpleScriptContext();
                    compiledScript = ((Compilable) engine).compile(
                            "function (string, pattern) { return RegExp(pattern, 'g').test(string); }"
                    );
                    regexMatch = compiledScript.eval(scriptContext);
                }
            } catch (ScriptException e) {
                throw new RuntimeException(e);
            }
    }

    private boolean matchesPattern(final String string, String pattern) throws IOException {
        //TODO ECMA 262 regular expression
        if (pattern == null)
            return true;

        try {
            final Object result;

            if (USE_JAVASCRIPT_REGEXP) {
                StringBuilder escapedPattern = new StringBuilder();

                boolean mbChar = false;

                for (char c : pattern.toCharArray()) {
                    if (c <= 127) {
                        if (mbChar) {
                            mbChar = false;
                            escapedPattern.append(")");
                        }
                        escapedPattern.append(c);
                    } else {
                        if (!mbChar) {
                            mbChar = true;
                            escapedPattern.append("(?:");
                        }
                        escapedPattern.append(c);
                        //escapedPattern.append("\\u{").append(Long.toHexString(c)).append("}");
                    }
                }

                if (mbChar) {
                    mbChar = false;
                    escapedPattern.append(")");
                }

                pattern = escapedPattern.toString();
            } else {
//                pattern = pattern
//                        .replace("\\s", "[ ]")
//                        .replace("\\S", "[^ ]")
//                ;
            }

            if (regexMatch != null)
                result = ((Invocable) engine).invokeMethod(regexMatch, "call", regexMatch, string, pattern);
            else
                result = Pattern.compile(pattern, 0).matcher(string).find();

            return result.equals(Boolean.TRUE);
        } catch (ScriptException e) {
            throw new IOException(e);
        } catch (NoSuchMethodException e) {
            throw new IOException(e);
        }
    }

    private static boolean matchesAnyTypeOf(JsonSchemaV7.Type type, JsonSchemaV7.Type[] allowedTypes) {
        do {
            for (JsonSchemaV7.Type y : allowedTypes)
                if (y.equals(type))
                    return true;
            type = type.superType;
        } while (type != null);
        return false;
    }

    private static <T> boolean nodeEquals(JsonNode a, JsonNode b) {
        return a.getNodeType() == b.getNodeType() && a.equals(b);
    }

    private static <T> boolean enumNodeEquals(JsonNode a, JsonNode b) {
        if (a.isNumber() && b.isNumber() && a.numberType() != b.numberType())
            return a.decimalValue().compareTo(b.decimalValue()) == 0;
        else
            return a.equals(b);
    }

    private static <T> boolean enumContains(ArrayNode enumeration, JsonNode x) {
        for (JsonNode y : enumeration)
            if (enumNodeEquals(y, x))
                return true;
        return false;
    }

    private int countMatches(final SchemaReference schemaReference, Set<Integer> evaluatedItemsOut, Set<String> evaluatedPropertiesOut, JsonNode value, ArrayNode schemas, boolean stopOnFirstMatch) throws IOException {
        int matchCount = 0;
        List<SchemaViolation> violations = new ArrayList<SchemaViolation>();
        for (JsonNode jsonSchema : schemas) {
            Set<Integer> evaluatedItems = new HashSet<Integer>();
            Set<String> evaluatedProperties = new HashSet<String>();
            performValidation(violations, evaluatedItems, evaluatedProperties, schemaReference.findChild(jsonSchema), value);
            if (violations.isEmpty()) {
                ++matchCount;
                if (evaluatedItemsOut != null)
                    evaluatedItemsOut.addAll(evaluatedItems);
                if (evaluatedPropertiesOut != null)
                    evaluatedPropertiesOut.addAll(evaluatedProperties);
                if (stopOnFirstMatch)
                    break;
            } else
                violations.clear();
        }
        return matchCount;
    }

    private boolean matchAll(List<SchemaViolation> violationsOut, Set<Integer> evaluatedItemsOut, Set<String> evaluatedPropertiesOut, final SchemaReference schemaReference, AtomicReference<JsonNode> valueRef, ArrayNode schemas) throws IOException {
        JsonNode value = valueRef.get();
        for (JsonNode jsonSchema : schemas) {
            value = performValidation(violationsOut, evaluatedItemsOut, evaluatedPropertiesOut, schemaReference.findChild(jsonSchema), value);
        }
        if (write)
            valueRef.set(value);
        return violationsOut.isEmpty();
    }

    private static Object createMessageArg(JsonNode value) {
        switch (value.getNodeType()) {
            case BOOLEAN:
                return value.asBoolean();
            case NULL:
                return null;
            case NUMBER:
                return value.decimalValue();
            case STRING:
                return value.asText();
            case ARRAY:
            case BINARY:
            case MISSING:
            case OBJECT:
            case POJO:
            default:
                return value.toString();
        }
    }
}
