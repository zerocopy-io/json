package io.zerocopy.json.validator;

import java.util.List;

public interface ValidatorCallback {
    /**
     * Indicates, that a property is required (may depend on the presence of properties in the validated document)
     */
    int FLAG_REQUIRED = 0x1;

    /**
     * @param path  the current data path
     * @param flags a combination of {@link ValidatorCallback}#FLAG_*
     */
    void onPathPushed(List<String> path, int flags);

    void onViolation(SchemaViolation violation);
}
